package com.E.B.SHORASHIM;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Androwsa on 8/21/17.
 */

public class User_toUpload implements Serializable{

    private String id="";
    private String fullname="";
    private String phone="";
    private String dateofbirth="";
    private String description="";
    private String activated="";
    private String email="";

    public String getYoutubeid() {
        return youtubeid;
    }

    public void setYoutubeid(String youtubeid) {
        this.youtubeid = youtubeid;
    }

    private String youtubeid="";

    private String photolink="";

    public String getPhotolink() {
        return photolink;
    }

    public void setPhotolink(String photolink) {
        this.photolink = photolink;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    private String gender="";


    private String dateofdeath="";
    private String Did="";

    private String [] parent;
    private ArrayList<String> children;
    private ArrayList<String> spouse;
    private ArrayList<String> sibling;

    private String lat,Long;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLong() {
        return Long;
    }

    public void setLong(String aLong) {
        Long = aLong;
    }

    public String getActivated() {
        return activated;
    }

    public void setActivated(String activated) {
        this.activated = activated;
    }

    public User_toUpload(String id, String fullname, String phone, String dateofbirth, String description, String activated, String email, String dateofdeath, String did, String[] parent, ArrayList<String> children, ArrayList<String> spouse, ArrayList<String> sibling) {
        this.id = id;
        this.fullname = fullname;
        this.phone = phone;
        this.dateofbirth = dateofbirth;
        this.description = description;
        this.activated = activated;
        this.email = email;
        this.dateofdeath = dateofdeath;
        Did = did;
        this.parent = parent;
        this.children = children;
        this.spouse = spouse;
        this.sibling = sibling;
    }

    public User_toUpload(String id, String fullname, String phone, String dateofbirth, String description, String activated, String email) {

        this.id = id;
        this.fullname = fullname;
        this.phone = phone;
        this.dateofbirth = dateofbirth;
        this.description = description;
        this.activated = activated;
        this.email = email;
    }

    public User_toUpload(String id, String fullname, String phone, String dateofbirth, String description, String activated, String email, String dateofdeath, String did) {

        this.id = id;
        this.fullname = fullname;
        this.phone = phone;
        this.dateofbirth = dateofbirth;
        this.description = description;
        this.activated = activated;
        this.email = email;
        this.dateofdeath = dateofdeath;
        Did = did;

        if(this.dateofdeath==null)
            this.dateofdeath = "";
    }

    public User_toUpload(String id, String fullname, String phone, String dateofbirth, String description, String activated) {

        this.id = id;
        this.fullname = fullname;
        this.phone = phone;
        this.dateofbirth = dateofbirth;
        this.description = description;
        this.activated = activated;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDateofbirth() {
        return dateofbirth;
    }

    public void setDateofbirth(String dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateofdeath() {
        return dateofdeath;
    }

    public void setDateofdeath(String dateofdeath) {
        this.dateofdeath = dateofdeath;
    }

    public String getDid() {
        return Did;
    }

    public void setDid(String did) {
        Did = did;
    }

    public String[] getParent() {
        return parent;
    }

    public ArrayList<String> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<String> children) {
        this.children = children;
    }

    public ArrayList<String> getSpouse() {
        return spouse;
    }

    public void setSpouse(ArrayList<String> spouse) {
        this.spouse = spouse;
    }

    public ArrayList<String> getSibling() {
        return sibling;
    }

    public void setSibling(ArrayList<String> sibling) {
        this.sibling = sibling;
    }

    public void setParent(String[] parent) {
        this.parent = parent;
    }



    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", fullname='" + fullname + '\'' +
                ", phone='" + phone + '\'' +
                ", dateofbirth='" + dateofbirth + '\'' +
                ", description='" + description + '\'' +
                ", dateofdeath='" + dateofdeath + '\'' +
                ", Did='" + Did + '\'' +
                ", parent=" + Arrays.toString(parent) +
                ", children=" + children +
                ", spouse=" + spouse +
                ", sibling=" + sibling +
                '}';
    }
}
