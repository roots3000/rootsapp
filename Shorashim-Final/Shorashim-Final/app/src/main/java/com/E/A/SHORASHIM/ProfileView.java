package com.E.B.SHORASHIM;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.icu.util.Calendar;
import android.icu.util.HebrewCalendar;
import android.icu.util.TimeZone;
import android.location.Location;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.InputType;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.E.A.SHORASHIM.R;
import com.E.B.SHORASHIM.AlaramManagerr.AlarmReceiver;
import com.E.B.SHORASHIM.JewishCalendar.CalendarDate;
import com.E.B.SHORASHIM.JewishCalendar.CalendarImpl;
import com.E.B.SHORASHIM.JewishCalendar.jewishcal;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Locale;
import java.util.Random;


import de.hdodenhof.circleimageview.CircleImageView;

import static android.R.attr.delay;

public class ProfileView extends  YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener{

    LinearLayout profilelayout;

    User person;

    String Phone;
    Spinner kedomet;


    User father,mother;

    EditText fullname,lastname, phone, day, month, year, about,youtubeidvid;

    EditText fDday, fDmonth, fDyear;
    String FDday, FDmonth, FDyear;

    CircleImageView profilepicture;


    Uri thepicture;

    CheckBox isDeceased,withOutPhone;

    DatabaseReference dataBaseUser;

    boolean ifpicturepicked = false;
    private StorageReference mStorage;


    LinearLayout isdeceasedlayout;

    ProgressDialog progressbar;

    byte[] biti;

    FirebaseHandler fbh;

    String currentuser, who, Gender,MainUser,currentspouse;
    private String VIDEOID = "";

    countryCodes codes = new countryCodes();


    //---------------------OPENING NAVIGATION-------------------------------//
    public void OpenNav(String latitude, String longtitude) {
        String uri = "geo: " + latitude + " , " + longtitude;
        startActivity(new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse(uri)));
    }
    //---------------------OPENING NAVIGATION-------------------------------//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_view);
        setPopUp();

        dataBaseUser = FirebaseDatabase.getInstance().getReference("userprofile");

        profilelayout = (LinearLayout) findViewById(R.id.PVlayout);

        progressbar = new ProgressDialog(this);

        mStorage = FirebaseStorage.getInstance().getReference();

        //all the getIntent....... gets data ive sent from the treeLayOut to know why the profile view was called


        //getSerializableExtra it retrieves an Object
        father = (User) getIntent().getSerializableExtra("father1");
        mother = (User) getIntent().getSerializableExtra("mother1");

        person = (User) getIntent().getSerializableExtra("user");

        //hai el split 3la keef m2na ktbt el code lal youtube url
        try {
            VIDEOID= person.getYoutubeid().split("/")[3];
        }
        catch (Exception e){}

        currentuser = getIntent().getExtras().getString("currentUser");
        who = getIntent().getExtras().getString("who");

        MainUser = getIntent().getExtras().getString("mainuser");

        if(who.equals("children")){
            currentspouse = getIntent().getExtras().getString("currentspouse");
        }

        try {
            Gender = getIntent().getExtras().getString("currentGender");
        } catch (Exception e) {
        }

        biti = getIntent().getExtras().getByteArray("phototo");

        fbh = new FirebaseHandler(null,ProfileView.this);
        if (person != null) {
            buildView();
        } else {
            buildViewForNull();
        }


    }


    //This activity build view for viewing existing user
    private void buildView() {
        if (currentuser.equals(MainUser)){
            if ((person.getId().equals(MainUser) || person.getActivated().equals("inactive"))&& checkinternetconnectivity()) {
                Button Edit = new Button(this);
                Edit.setText(getResources().getString(R.string.ste_edit));
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(250, 150);
                lp.setMargins(0, 15, 0, 0);
                lp.gravity = Gravity.RIGHT;
                Edit.setLayoutParams(lp);


                //An Edit button to enable editing for the profile we chose IFFFF the above is accepted-----------Start

                Edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!checkinternetconnectivity()) {
                            Toast t = Toast.makeText(ProfileView.this, R.string.str_error_nointernetconnection, Toast.LENGTH_SHORT);
                            t.setGravity(Gravity.CENTER, 0, 0);
                            t.show();
                        }
                        else{
                            profilelayout.removeAllViews();

                            FDday = "";
                            FDmonth = "";
                            FDyear = "";

                            DisplayMetrics dm = new DisplayMetrics();
                            getWindowManager().getDefaultDisplay().getMetrics(dm);

                            int width = dm.widthPixels;

                            double used = width * 0.5;

                            RadioGroup rdg = new RadioGroup(ProfileView.this);
                            final RadioButton male = new RadioButton(ProfileView.this);
                            RadioButton female = new RadioButton(ProfileView.this);
                            male.setTextColor(Color.parseColor("#000000"));
                            female.setTextColor(Color.parseColor("#000000"));
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            lp.setMargins(0, 14, 0, 0);
                            rdg.setLayoutParams(lp);
                            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            lp.setMargins((int) used / 2, 14, (int) used / 2, 0);
                            rdg.addView(male);

                            male.setText(ProfileView.this.getResources().getString(R.string.str_male));
                            male.setLayoutParams(lp);
                            male.setGravity(Gravity.CENTER);
                            female.setText(ProfileView.this.getResources().getString(R.string.str_femaile));
                            female.setLayoutParams(lp);
                            rdg.addView(female);


                            if (person.getGender().equals("male")) {
                                male.setChecked(true);

                            } else {
                                female.setChecked(true);
                            }


                            final ImageButton Vsave = new ImageButton(ProfileView.this);
                            lp = new LinearLayout.LayoutParams(150, 150);
                            lp.setMargins(0, 15, 0, 0);
                            Vsave.setBackgroundResource(R.drawable.checked_1);
                            lp.gravity = Gravity.RIGHT;
                            Vsave.setLayoutParams(lp);
                            Vsave.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    progressbar.setMessage(getResources().getString(R.string.str_error_loading));
                                    String Fullname = fullname.getText().toString().trim();
                                    Fullname = Fullname.replace(" ", "");
                                    String Lastname = lastname.getText().toString().trim();
                                    Phone = phone.getText().toString().trim();
                                    Lastname = Lastname.replace(" ", "");
                                    String Phone = phone.getText().toString().trim();
                                    String About = about.getText().toString().trim();
                                    String Yotube = youtubeidvid.getText().toString().trim();
                                    String Day1 = day.getText().toString().trim();
                                    String Month1 = month.getText().toString().trim();
                                    String Year1 = year.getText().toString().trim();
                                    int Day = Integer.parseInt(day.getText().toString().trim());
                                    int Month = Integer.parseInt(month.getText().toString().trim());
                                    int Year = Integer.parseInt(year.getText().toString().trim());

                                    String gender;
                                    if (male.isChecked()) {
                                        gender = "male";
                                    } else
                                        gender = "female";

                                    if (TextUtils.isEmpty(Fullname)) {
                                        Toast.makeText(ProfileView.this, R.string.str_error_enterfullname, Toast.LENGTH_SHORT).show();
                                        return;
                                    }

                                    if (TextUtils.isEmpty(Lastname)) {
                                        Toast.makeText(ProfileView.this, R.string.str_error_enterlastname, Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    if (TextUtils.isEmpty(gender)) {
                                        Toast.makeText(ProfileView.this, R.string.str_error_chooseGender, Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    if (!isDeceased.isChecked() && !withOutPhone.isChecked()) {
                                        if (TextUtils.isEmpty(Phone)) {
                                            Toast.makeText(ProfileView.this, R.string.str_error_enternumber, Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                        if (Phone.length() == 9) {
                                            Phone = "+" + getplace(kedomet.getSelectedItem().toString()) + Phone;
                                        } else if (Phone.length() == 10) {
                                            Phone = Phone.substring(1);
                                            Phone = "+" + getplace(kedomet.getSelectedItem().toString()) + Phone;
                                        } else {
                                            Toast.makeText(ProfileView.this, R.string.str_error_entervalidphone, Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                    } else {
                                        Phone = "";
                                    }
                                    if (TextUtils.isEmpty(About)) {
                                        About = " ";
                                    }
                                    if (TextUtils.isEmpty(Day1) || TextUtils.isEmpty(Month1) || TextUtils.isEmpty(Year1)) {
                                        Toast.makeText(ProfileView.this, R.string.str_error_enterdateofbirth, Toast.LENGTH_SHORT).show();
                                        return;
                                    }

                                    if (Day > 0 && Day < 32) {

                                    } else {
                                        Toast.makeText(ProfileView.this, getResources().getString(R.string.str_error_entervalidday), Toast.LENGTH_SHORT).show();
                                        return;
                                    }

                                    if (Month > 0 && Month < 13) {

                                    } else {
                                        Toast.makeText(ProfileView.this, R.string.str_error_entervalidmonth, Toast.LENGTH_SHORT).show();
                                        return;
                                    }

                                    if (Year > 0 && Year < 2020) {

                                    } else {
                                        Toast.makeText(ProfileView.this, R.string.str_error_entervalidyear, Toast.LENGTH_SHORT).show();
                                        return;
                                    }

                                    if (isDeceased.isChecked()) {
                                        FDday = fDday.getText().toString().trim();
                                        FDmonth = fDmonth.getText().toString().trim();
                                        FDyear = fDyear.getText().toString().trim();

                                        if (TextUtils.isEmpty(FDday) || TextUtils.isEmpty(FDmonth) || TextUtils.isEmpty(FDyear)) {
                                            Toast.makeText(ProfileView.this, R.string.str_error_enterdateofdeath, Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                    }
                                    //String id, String fullname, String phone, String dateofbirth, String description, String activated, String email, String dateofdeath, String did)
                                    String Ddate;
                                    if (TextUtils.isEmpty(FDday)) {
                                        Ddate = "";
                                    } else {
                                        Ddate = FDday + "/" + FDmonth + "/" + FDyear;
                                    }
                                    final User_toUpload temp = new User_toUpload(person.getId(), Fullname + " " + Lastname, Phone, Day + "/" + Month + "/" + Year, About, person.getActivated(), "", Ddate, "");

                                    StorageReference storageref = mStorage.child("photos").child(person.getId());

                                    temp.setEmail(person.getEmail());

                                    if (isDeceased.isChecked()) {
                                        temp.setLong(person.getLong());
                                        temp.setLat(person.getLat());
                                    } else {
                                        temp.setLong(" ");
                                        temp.setLat(" ");
                                    }

                                    temp.setGender(gender);

                                    if (!TextUtils.isEmpty(Yotube))
                                        try {
                                            String s = Yotube.split("/")[3];
                                            temp.setYoutubeid(Yotube);
                                        } catch (Exception e) {
                                            Toast.makeText(ProfileView.this, getResources().getString(R.string.str_error_invalidyoutubelink), Toast.LENGTH_LONG).show();
                                            return;
                                        }
                                    if (!ifpicturepicked) temp.setPhotolink("no");

                                    progressbar.show();
                                    if (!ifpicturepicked) {
                                        Vsave.setClickable(false);
                                        fbh.EditUser(temp, ProfileView.this, progressbar);
                                    } else {
                                        storageref.putFile(thepicture).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                            @Override
                                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                temp.setPhotolink(taskSnapshot.getDownloadUrl().toString());
                                                Vsave.setClickable(false);
                                                fbh.EditUser(temp, ProfileView.this, progressbar);
                                            }
                                        });
                                    }
                                }
                            });
                            profilelayout.addView(Vsave);

                            lp = new LinearLayout.LayoutParams(420, 420);
                            lp.gravity = Gravity.CENTER;
                            final CircleImageView kokos = new CircleImageView(ProfileView.this);
                            kokos.setLayoutParams(lp);
                            profilepicture = kokos;
                            kokos.setImageResource(R.drawable.male);
                            kokos.setBorderWidth(20);
                            kokos.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    int gallery = 1;

                                    startActivityForResult(pickPhoto, gallery);
                                }
                            });
                            kokos.setBorderColor(Color.parseColor("#FFFFFF"));
                            profilelayout.addView(kokos);

//                    Bitmap bitmaps = BitmapFactory.decodeByteArray(biti, 0, biti.length);
//                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                    bitmaps.compress(Bitmap.CompressFormat.PNG, 100, stream);

                            Glide.with(ProfileView.this)
                                    .load(biti)
                                    .asBitmap()
                                    .into(kokos);

                            male.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
//                            profilepicture.setImageResource(R.drawable.male);
                                }
                            });

                            female.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
//                            profilepicture.setImageResource(R.drawable.female);
                                }
                            });

                            fullname = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
                            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            lp.setMargins(0, 14, 0, 0);
                            fullname.setHint("*" + getResources().getString(R.string.str_fullname));
                            fullname.setLayoutParams(lp);
                            fullname.setHintTextColor(Color.parseColor("#ffffff"));
                            fullname.setText(person.getFullname().split(" ")[0]);
                            profilelayout.addView(fullname);


                            lastname = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
                            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            lp.setMargins(0, 14, 0, 0);
                            lastname.setHint("*" + getResources().getString(R.string.str_lastname));
                            lastname.setLayoutParams(lp);
                            lastname.setHintTextColor(Color.parseColor("#ffffff"));
                            lastname.setText(person.getFullname().split(" ")[1]);
                            profilelayout.addView(lastname);

                            profilelayout.addView(rdg);

                            LinearLayout tempphonelayout = new LinearLayout(ProfileView.this);
                            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                            tempphonelayout.setOrientation(LinearLayout.HORIZONTAL);
                            tempphonelayout.setLayoutParams(lp);

                            kedomet = new Spinner(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
                            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0.2f);
                            kedomet.setLayoutParams(lp);
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(ProfileView.this, android.R.layout.simple_spinner_dropdown_item, codes.countryCodes);
                            kedomet.setAdapter(adapter);
                            tempphonelayout.addView(kedomet);


                            phone = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
                            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0.8f);
                            lp.setMargins(0, 14, 0, 0);
                            phone.setHint("*" + getResources().getString(R.string.str_phone_number));
                            phone.setHintTextColor(Color.parseColor("#ffffff"));
                            phone.setInputType(InputType.TYPE_CLASS_NUMBER);
                            try {
                                phone.setText(person.getPhone().substring(4));
                            } catch (Exception e) {
                            }
                            phone.setLayoutParams(lp);
                            tempphonelayout.addView(phone);

                            profilelayout.addView(tempphonelayout);

                            TextView dateofbirth = new TextView(ProfileView.this);
                            dateofbirth.setText(getResources().getString(R.string.str_birth_date));
                            profilelayout.addView(dateofbirth);

                            LinearLayout ll = new LinearLayout(ProfileView.this);
                            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            lp.setMargins(0, 14, 0, 0);
                            ll.setLayoutParams(lp);

                            day = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
                            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
                            day.setHint("*" + getResources().getString(R.string.str_day));
                            day.setHintTextColor(Color.parseColor("#ffffff"));
                            day.setInputType(InputType.TYPE_CLASS_NUMBER);
                            lp.setMargins(0, 0, 4, 0);
                            day.setText(person.getDateofbirth().split("/")[0]);
                            day.setLayoutParams(lp);
                            ll.addView(day);

                            month = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
                            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
                            month.setHint("*" + getResources().getString(R.string.str_month));
                            month.setInputType(InputType.TYPE_CLASS_NUMBER);
                            month.setHintTextColor(Color.parseColor("#ffffff"));
                            month.setText(person.getDateofbirth().split("/")[1]);
                            lp.setMargins(0, 0, 4, 0);
                            month.setLayoutParams(lp);
                            ll.addView(month);

                            year = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
                            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
                            year.setHint("*" + getResources().getString(R.string.str_year));
                            year.setHintTextColor(Color.parseColor("#ffffff"));
                            year.setText(person.getDateofbirth().split("/")[2]);
                            year.setLayoutParams(lp);
                            year.setInputType(InputType.TYPE_CLASS_NUMBER);
                            ll.addView(year);

                            profilelayout.addView(ll);

                            about = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
                            about.setSingleLine(false);
                            about.setHintTextColor(Color.parseColor("#ffffff"));
                            about.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
                            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            lp.setMargins(0, 14, 0, 0);
                            if (!person.getDescription().equals(" "))
                                about.setText(person.getDescription());
                            about.setHint(getResources().getString(R.string.str_about));
                            about.setLayoutParams(lp);
                            profilelayout.addView(about);

                            youtubeidvid = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
                            youtubeidvid.setSingleLine(false);
                            youtubeidvid.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
                            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            lp.setMargins(0, 14, 0, 0);
                            if (!person.getYoutubeid().equals(""))
                                youtubeidvid.setText(person.getYoutubeid());
                            youtubeidvid.setHint(getResources().getString(R.string.str_keshor));
                            youtubeidvid.setHintTextColor(Color.parseColor("#ffffff"));
                            youtubeidvid.setLayoutParams(lp);
                            profilelayout.addView(youtubeidvid);


                            isDeceased = new CheckBox(ProfileView.this);
                            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            lp.setMargins(0, 14, 0, 0);
                            isDeceased.setText(getResources().getString(R.string.str_ispersondead));
                            isDeceased.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (isDeceased.isChecked()) {
                                        addDeadEditTextsfordead();
                                    } else {
                                        isdeceasedlayout.removeAllViews();
                                        fDday = null;
                                        fDmonth = null;
                                        fDyear = null;
                                    }
                                }
                            });
                            profilelayout.addView(isDeceased);


                            isdeceasedlayout = new LinearLayout(ProfileView.this);
                            isdeceasedlayout.setOrientation(LinearLayout.VERTICAL);
                            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            isdeceasedlayout.setLayoutParams(lp);
                            profilelayout.addView(isdeceasedlayout);

                            withOutPhone = new CheckBox(ProfileView.this);
                            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            lp.setMargins(0, 14, 0, 0);
                            withOutPhone.setText(getResources().getString(R.string.str_withoutphone));
                            profilelayout.addView(withOutPhone);

                            if (!person.getDateofdeath().equals("")) {
                                isDeceased.performClick();
                            }
                            if (person.getDateofdeath().equals("") && person.getPhone().equals("")) {
                                withOutPhone.performClick();
                            }


//                profilelayout.addView(savenewUser);

                        }
                    }
                });
                profilelayout.addView(Edit);
            }
        }

        //An Edit button to enable editing for the profile we chose IFFFF the above is accepted-----------END




        //Sets the Profile view with all the data

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(520,520);
        lp.gravity= Gravity.CENTER;
        final CircleImageView kokos = new CircleImageView(ProfileView.this);
        kokos.setLayoutParams(lp);
        profilepicture =kokos;
        kokos.setImageResource(R.drawable.male);
        kokos.setBorderWidth(20);
        kokos.setBorderColor(Color.parseColor("#FFFFFF"));
        profilelayout.addView(kokos);

        Glide.with(ProfileView.this)
                .load(biti)
                .asBitmap()
                .into(kokos);


        TextView fullname = new TextView(this);
        lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0, 15, 0, 0);
        lp.gravity = Gravity.CENTER;
        fullname.setGravity(Gravity.CENTER);
        fullname.setTextSize(20);
        fullname.setText(person.getFullname());
        fullname.setLayoutParams(lp);
        profilelayout.addView(fullname);

        TextView date = new TextView(this);
        int day = Integer.parseInt(person.getDateofbirth().split("/")[0]);
        int month = Integer.parseInt(person.getDateofbirth().split("/")[1]);
        int year = Integer.parseInt(person.getDateofbirth().split("/")[2]);
        Log.e("buildView: ", day + "/"+month+"/"+year);
        CalendarDate gregoriandate= new CalendarDate(day,month,year);
        CalendarImpl imp = new CalendarImpl();
        CalendarDate jewishCal = gregorian2Jewish(gregoriandate,imp);
        jewishcal jewiishhelper = new jewishcal(this);
        lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0, 15, 0, 0);
        lp.gravity = Gravity.CENTER;
        date.setGravity(Gravity.CENTER);
        date.setTextSize(15);
        date.setText(person.getDateofbirth()+"\n"+jewiishhelper.getJewishdayinHebrew(jewishCal.getDay())+"   "+jewiishhelper.getJewishMonthName(jewishCal.getMonth(),jewishCal.getYear())+"   "+jewiishhelper.getJewishYearinHebrew(jewishCal.getYear()));
        date.setLayoutParams(lp);
        profilelayout.addView(date);


        // sameday BUTTONS sets alarms to notify on birthday/date of death
        Button sameday = new Button(ProfileView.this);
        lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.gravity = Gravity.CENTER;
        sameday.setLayoutParams(lp);
        sameday.setBackgroundResource(R.drawable.drawable_test);
        sameday.setText(R.string.reminder);
        final int finalMonth = month;
        final int finalDay = day;
        final int finalYear = year;
        sameday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random r = new Random();

                // abel mteb3aso eshe hon estsherone

                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
                notificationIntent.addCategory("android.intent.category.DEFAULT");

                notificationIntent.putExtra("fullname",person.getFullname());
                notificationIntent.putExtra("or","dateofbirth");
                notificationIntent.putExtra("date",finalDay+"/"+finalMonth);
                notificationIntent.putExtra("id",MainUser);


                int uniquenum= (((((finalMonth+finalDay+finalYear)*23)/4)+finalDay+finalMonth+finalYear)*29)/3+(person.getFullname().length()*18)+69;
                PendingIntent broadcast = PendingIntent.getBroadcast(ProfileView.this,uniquenum, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.MONTH,finalMonth-1);
                cal.set(Calendar.DAY_OF_MONTH,finalDay);
                cal.set(Calendar.HOUR_OF_DAY,8);
                cal.set(Calendar.MINUTE,0);
                cal.set(Calendar.SECOND,0);
//                    cal.add(Calendar.DAY_OF_YEAR, 362);
//                    cal.add(Calendar.DAY_OF_YEAR, 358);
//                    cal.add(Calendar.DAY_OF_YEAR, 365);
//                    cal.set(Calendar.HOUR_OF_DAY,8);

//                    cal.add(Calendar.SECOND, 60);

                long pika = cal.getTimeInMillis();

                Long pikachu = Calendar.getInstance().getTimeInMillis();

                AlarmManager alarmManager3 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                Intent notificationIntent3 = new Intent("android.media.action.DISPLAY_NOTIFICATION");
                notificationIntent3.addCategory("android.intent.category.DEFAULT");

                notificationIntent3.putExtra("fullname",person.getFullname());
                notificationIntent3.putExtra("or","dateofdeath");
                notificationIntent3.putExtra("date",finalDay+"/"+finalMonth);
                notificationIntent3.putExtra("id",MainUser);

                PendingIntent broadcast3 = PendingIntent.getBroadcast(ProfileView.this,uniquenum+1, notificationIntent3, PendingIntent.FLAG_UPDATE_CURRENT);

                AlarmManager alarmManager7 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                Intent notificationIntent7 = new Intent("android.media.action.DISPLAY_NOTIFICATION");
                notificationIntent7.addCategory("android.intent.category.DEFAULT");

                notificationIntent7.putExtra("fullname",person.getFullname());
                notificationIntent7.putExtra("or","dateofdeath");
                notificationIntent7.putExtra("date",finalDay+"/"+finalMonth);
                notificationIntent7.putExtra("id",MainUser);
//                    notificationIntent.putExtra("notyid",r.nextInt(999999999));

                PendingIntent broadcast7 = PendingIntent.getBroadcast(ProfileView.this,uniquenum+2, notificationIntent7, PendingIntent.FLAG_UPDATE_CURRENT);


                Calendar zoborte = Calendar.getInstance();
                zoborte.add(Calendar.SECOND, 60);
                Calendar zoborte2 = Calendar.getInstance();
                zoborte2.add(Calendar.SECOND, 15);
                Calendar zoborte3 = Calendar.getInstance();
                zoborte3.add(Calendar.SECOND, 30);

                if(pika < pikachu){
                    cal.set(Calendar.YEAR,cal.get(Calendar.YEAR)+1);
                }

                long pikas = cal.getTimeInMillis();

                int day = cal.get(Calendar.DAY_OF_MONTH);
                int month = cal.get(Calendar.MONTH);
                int year = cal.get(Calendar.YEAR);
                Log.e("check","pika   "+day+"/"+month+"/"+year);

                Calendar threedays = Calendar.getInstance();
                threedays.setTimeInMillis(pikas-259200000);
                day = threedays.get(Calendar.DAY_OF_MONTH);
                month = threedays.get(Calendar.MONTH);
                year = threedays.get(Calendar.YEAR);
                Log.e("check","pika   "+day+"/"+month+"/"+year);

                Calendar oneweek = Calendar.getInstance();
                oneweek.setTimeInMillis(pikas-604800000);
                day = oneweek.get(Calendar.DAY_OF_MONTH);
                month = oneweek.get(Calendar.MONTH);
                year = oneweek.get(Calendar.YEAR);
                Log.e("check","pika   "+day+"/"+month+"/"+year);

                alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);
                alarmManager3.setExact(AlarmManager.RTC_WAKEUP, threedays.getTimeInMillis(), broadcast3);
                alarmManager7.setExact(AlarmManager.RTC_WAKEUP, oneweek.getTimeInMillis(), broadcast7);

                Toast.makeText(ProfileView.this,R.string.savednotification,Toast.LENGTH_SHORT).show();

            }
        });
        profilelayout.addView(sameday);


        Button SendSMS = new Button(ProfileView.this);
        lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0,50,0,0);
        SendSMS.setBackgroundResource(R.drawable.placeholder);
        if(person.getPhone().equals("")|| person.getActivated().equals("active")){
            SendSMS.setText(getResources().getString(R.string.str_invite));
            SendSMS.setBackgroundResource(R.drawable.roundedsilver);
            SendSMS.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast toast=Toast.makeText(ProfileView.this,R.string.str_errorcannotinvitebecauseuserisnotconnectedwithaphone,Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            });
        }
        else{
            SendSMS.setBackgroundResource(R.drawable.roundedb2);
            SendSMS.setText(getResources().getString(R.string.str_invite));
            SendSMS.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final AlertDialog alertDialog = new AlertDialog.Builder(ProfileView.this).create();
                    alertDialog.setTitle(R.string.str_InvitationTitle);
                    alertDialog.setMessage(getResources().getString(R.string.str_InvitationAsk));
                    final String finalPhone = person.getPhone();

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.str_OK),

                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + finalPhone));
                                    intent.putExtra("sms_body", getResources().getString(R.string.str_sms));
                                    ProfileView.this.startActivity(intent);
                                    ProfileView.this.finish();
                                }
                            });
                    alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.str_NO), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ProfileView.this.finish();
                            alertDialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }
            });
        }
        SendSMS.setLayoutParams(lp);

        if (person.getDateofdeath().equals("")) {
        } else {
            TextView deathdate = new TextView(this);
            day = Integer.parseInt(person.getDateofdeath().split("/")[0]);
            month = Integer.parseInt(person.getDateofdeath().split("/")[1]);
            year = Integer.parseInt(person.getDateofdeath().split("/")[2]);
            Log.e("buildView: ", day + "/"+month+"/"+year);
            gregoriandate= new CalendarDate(day,month,year);
            imp = new CalendarImpl();
            jewishCal = gregorian2Jewish(gregoriandate,imp);
            jewiishhelper = new jewishcal(this);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 15, 0, 0);
            lp.gravity = Gravity.CENTER;
            deathdate.setGravity(Gravity.CENTER);
            deathdate.setTextSize(20);
            deathdate.setText(getResources().getString(R.string.str_dateofdeath) + "\n" + person.getDateofdeath()+"\n"+jewiishhelper.getJewishdayinHebrew(jewishCal.getDay())+"   "+jewiishhelper.getJewishMonthName(jewishCal.getMonth(),jewishCal.getYear())+"   "+jewiishhelper.getJewishYearinHebrew(jewishCal.getYear()));
            deathdate.setLayoutParams(lp);
            profilelayout.addView(deathdate);

            // sameday BUTTONS sets alarms to notify on birthday/date of death
            Button sameday1 = new Button(ProfileView.this);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.gravity = Gravity.CENTER;
            sameday1.setLayoutParams(lp);
            sameday1.setText(R.string.reminder_D);
            sameday1.setBackgroundResource(R.drawable.drawable_test);
            final int finalMonth1 = month;
            final int finalDay1 = day;
            final int finalYear1 = year;
            sameday1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Random r = new Random();


                    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                    Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
                    notificationIntent.addCategory("android.intent.category.DEFAULT");

                    notificationIntent.putExtra("fullname",person.getFullname());
                    notificationIntent.putExtra("or","dateofdeath");
                    notificationIntent.putExtra("date",finalDay1+"/"+finalMonth1);
                    notificationIntent.putExtra("id",MainUser);

                    int uniquenum= (((((finalMonth1+finalDay1+finalYear1)*23)/4)+finalDay+finalMonth+finalYear)*29)/3;
                    PendingIntent broadcast = PendingIntent.getBroadcast(ProfileView.this,uniquenum, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                    Calendar cal = Calendar.getInstance();
                    cal.set(Calendar.MONTH,finalMonth1-1);
                    cal.set(Calendar.DAY_OF_MONTH,finalDay1);
                    cal.set(Calendar.HOUR_OF_DAY,8);
                    cal.set(Calendar.MINUTE,0);
                    cal.set(Calendar.SECOND,0);
//                    cal.add(Calendar.DAY_OF_YEAR, 362);
//                    cal.add(Calendar.DAY_OF_YEAR, 358);
//                    cal.add(Calendar.DAY_OF_YEAR, 365);
//                    cal.set(Calendar.HOUR_OF_DAY,8);

//                    cal.add(Calendar.SECOND, 60);

                    long pika = cal.getTimeInMillis();

                    Long pikachu = Calendar.getInstance().getTimeInMillis();

                    AlarmManager alarmManager3 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                    Intent notificationIntent3 = new Intent("android.media.action.DISPLAY_NOTIFICATION");
                    notificationIntent3.addCategory("android.intent.category.DEFAULT");

                    notificationIntent3.putExtra("fullname",person.getFullname());
                    notificationIntent3.putExtra("or","dateofdeath");
                    notificationIntent3.putExtra("date",finalDay1+"/"+finalMonth1);
                    notificationIntent3.putExtra("id",MainUser);

                    PendingIntent broadcast3 = PendingIntent.getBroadcast(ProfileView.this,uniquenum+1, notificationIntent3, PendingIntent.FLAG_UPDATE_CURRENT);

                    AlarmManager alarmManager7 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                    Intent notificationIntent7 = new Intent("android.media.action.DISPLAY_NOTIFICATION");
                    notificationIntent7.addCategory("android.intent.category.DEFAULT");

                    notificationIntent7.putExtra("fullname",person.getFullname());
                    notificationIntent7.putExtra("or","dateofdeath");
                    notificationIntent7.putExtra("date",finalDay1+"/"+finalMonth1);
                    notificationIntent7.putExtra("id",MainUser);
//                    notificationIntent.putExtra("notyid",r.nextInt(999999999));

                    PendingIntent broadcast7 = PendingIntent.getBroadcast(ProfileView.this,uniquenum+2, notificationIntent7, PendingIntent.FLAG_UPDATE_CURRENT);


                    Calendar zoborte = Calendar.getInstance();
                    zoborte.add(Calendar.SECOND, 60);
                    Calendar zoborte2 = Calendar.getInstance();
                    zoborte2.add(Calendar.SECOND, 15);
                    Calendar zoborte3 = Calendar.getInstance();
                    zoborte3.add(Calendar.SECOND, 30);

                    if(pika < pikachu){
                        cal.set(Calendar.YEAR,cal.get(Calendar.YEAR)+1);
                    }

                    long pikas = cal.getTimeInMillis();

                    int day = cal.get(Calendar.DAY_OF_MONTH);
                    int month = cal.get(Calendar.MONTH);
                    int year = cal.get(Calendar.YEAR);
                    Log.e("check","pika   "+day+"/"+month+"/"+year);

                    Calendar threedays = Calendar.getInstance();
                    threedays.setTimeInMillis(pikas-259200000);
                    day = threedays.get(Calendar.DAY_OF_MONTH);
                    month = threedays.get(Calendar.MONTH);
                    year = threedays.get(Calendar.YEAR);
                    Log.e("check","pika   "+day+"/"+month+"/"+year);

                    Calendar oneweek = Calendar.getInstance();
                    oneweek.setTimeInMillis(pikas-604800000);
                    day = oneweek.get(Calendar.DAY_OF_MONTH);
                    month = oneweek.get(Calendar.MONTH);
                    year = oneweek.get(Calendar.YEAR);
                    Log.e("check","pika   "+day+"/"+month+"/"+year);

                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);
                    alarmManager3.setExact(AlarmManager.RTC_WAKEUP, threedays.getTimeInMillis(), broadcast3);
                    alarmManager7.setExact(AlarmManager.RTC_WAKEUP, oneweek.getTimeInMillis(), broadcast7);

                    Toast.makeText(ProfileView.this,R.string.savednotification,Toast.LENGTH_SHORT).show();

                }
            });
            profilelayout.addView(sameday1);

            LinearLayout ll = new LinearLayout(this);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 30, 0, 0);
            ll.setOrientation(LinearLayout.HORIZONTAL);
            ll.setLayoutParams(lp);

            TextView temp= new TextView(ProfileView.this);
            temp.setText(getResources().getString(R.string.str_savelocationforgrave));
            temp.setTextSize(20);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0,200,0,0);
            temp.setGravity(Gravity.CENTER);
            temp.setLayoutParams(lp);
            profilelayout.addView(temp);

            ImageButton getlocation = new ImageButton(ProfileView.this);
            lp = new LinearLayout.LayoutParams(220, 220);
            lp.gravity=Gravity.CENTER;
            getlocation.setBackgroundResource(R.drawable.placeholder);
            getlocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //erios is an temp User_toUpload sex blbne wz3tr
                        Intent i = new Intent(ProfileView.this,LocationSaver.class);
                        User_toUpload erios = new User_toUpload(person.getId(),person.getFullname(),person.getPhone(),person.getDateofbirth(),person.getDescription(),
                                person.getActivated(),person.getEmail(),person.getDateofdeath(),person.getDid());
                        erios.setGender(person.getGender());
                        erios.setPhotolink("no");
                        erios.setYoutubeid(person.getYoutubeid());
                        erios.setLat(person.getLat());
                        erios.setLong(person.getLong());

                        i.putExtra("user",(Serializable)erios);
                        i.putExtra("Main",MainUser);
                        i.putExtra("Current",currentuser);
                        ProfileView.this.startActivityForResult(i,0);


                }
            });
            getlocation.setLayoutParams(lp);
            profilelayout.addView(getlocation);


        }

        profilelayout.addView(SendSMS);

        TextView te2or = new TextView(this);
        lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0, 15, 0, 0);
        lp.gravity = Gravity.CENTER;
        te2or.setGravity(Gravity.CENTER);
        te2or.setTextSize(20);
        te2or.setText(person.getDescription());
        te2or.setLayoutParams(lp);
        profilelayout.addView(te2or);

        if(!VIDEOID.equals("")) {
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            YouTubePlayerView youTubePlayerView = new YouTubePlayerView(ProfileView.this);
            youTubePlayerView.setLayoutParams(lp);
            youTubePlayerView.initialize("kokos", ProfileView.this);

            profilelayout.addView(youTubePlayerView);
        }

    }


    //This activity build a view for adding a new user
    private void buildViewForNull() {
        progressbar.setMessage(getResources().getString(R.string.str_error_loading));

        // Sets a Save User LayOut for siblings and children--------------Start
        if (who.equals("sibling") || who.equals("children")) {
            profilelayout.removeAllViews();

            FDday = "";
            FDmonth = "";
            FDyear = "";

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
            lp.gravity= Gravity.CENTER;
            final CircleImageView kokos = new CircleImageView(this);
            kokos.setLayoutParams(lp);
            profilepicture =kokos;
            kokos.setImageResource(R.drawable.male);
            kokos.setBorderWidth(20);
            kokos.setBorderColor(Color.parseColor("#FFFFFF"));
            profilelayout.addView(kokos);

//            <de.hdodenhof.circleimageview.CircleImageView
//            xmlns:app="http://schemas.android.com/apk/res-auto"
//            android:id="@+id/profile_image"
//            android:layout_width="120dp"
//            android:layout_gravity="center"
//            android:layout_height="120dp"
//            android:layout_centerHorizontal="true"
//            android:layout_centerVertical="true"
//            android:src="@drawable/male"
//            app:border_color="#fffff"
//            app:border_width="1dp" />


            fullname = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 14, 0, 0);
            fullname.setHint("*"+getResources().getString(R.string.str_fullname));
            fullname.setHintTextColor(Color.parseColor("#ffffff"));
            fullname.setLayoutParams(lp);
            profilelayout.addView(fullname);

            lastname = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 14, 0, 0);
            lastname.setHint("*"+getResources().getString(R.string.str_lastname));
            lastname.setHintTextColor(Color.parseColor("#ffffff"));
            lastname.setLayoutParams(lp);
            profilelayout.addView(lastname);

            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);

            int width = dm.widthPixels;

            double used = width*0.5;

            RadioGroup rdg = new RadioGroup(ProfileView.this);
            final RadioButton male = new RadioButton(ProfileView.this);
            RadioButton female = new RadioButton(ProfileView.this);
            male.setTextColor(Color.parseColor("#000000"));
            female.setTextColor(Color.parseColor("#000000"));
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 14, 0, 0);
            rdg.setLayoutParams(lp);
            rdg.addView(male);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins((int)used/2, 14, (int)used/2, 0);
            male.setText(R.string.str_male);
            female.setText(R.string.str_femaile);
            male.setLayoutParams(lp);
            female.setLayoutParams(lp);
            rdg.addView(female);
            male.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!ifpicturepicked)
                        kokos.setImageResource(R.drawable.male);

                }
            });
            female.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!ifpicturepicked)
                        kokos.setImageResource(R.drawable.female);
                }
            });
            profilelayout.addView(rdg);

            LinearLayout tempphonelayout = new LinearLayout(ProfileView.this);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            tempphonelayout.setOrientation(LinearLayout.HORIZONTAL);
            tempphonelayout.setLayoutParams(lp);

            kedomet = new Spinner(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,0.2f);
            kedomet.setLayoutParams(lp);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(ProfileView.this, android.R.layout.simple_spinner_dropdown_item, codes.countryCodes);
            kedomet.setAdapter(adapter);
            tempphonelayout.addView(kedomet);


            phone = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,0.8f);
            lp.setMargins(0, 14, 0, 0);
            phone.setHint("*"+getResources().getString(R.string.str_phone_number));
            phone.setHintTextColor(Color.parseColor("#ffffff"));
            phone.setInputType(InputType.TYPE_CLASS_NUMBER);
            phone.setLayoutParams(lp);
            tempphonelayout.addView(phone);

            profilelayout.addView(tempphonelayout);

            TextView dateofbirth = new TextView(this);
            dateofbirth.setText(getResources().getString(R.string.str_birth_date)+":");
            profilelayout.addView(dateofbirth);

            LinearLayout ll = new LinearLayout(this);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 14, 0, 0);
            ll.setLayoutParams(lp);

            day = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
            day.setHint("*"+getResources().getString(R.string.str_day));
            day.setHintTextColor(Color.parseColor("#ffffff"));
            day.setInputType(InputType.TYPE_CLASS_NUMBER);
            lp.setMargins(0, 0, 4, 0);
            day.setLayoutParams(lp);
            ll.addView(day);

            month = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
            month.setHint("*"+getResources().getString(R.string.str_month));
            month.setHintTextColor(Color.parseColor("#ffffff"));
            month.setInputType(InputType.TYPE_CLASS_NUMBER);
            lp.setMargins(0, 0, 4, 0);
            month.setLayoutParams(lp);
            ll.addView(month);

            year = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
            year.setHint("*"+getResources().getString(R.string.str_year));
            year.setHintTextColor(Color.parseColor("#ffffff"));
            year.setLayoutParams(lp);
            year.setInputType(InputType.TYPE_CLASS_NUMBER);
            ll.addView(year);

            profilelayout.addView(ll);

            about = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
            about.setSingleLine(false);
            about.setHintTextColor(Color.parseColor("#ffffff"));
            about.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 14, 0, 0);
            about.setHint(getResources().getString(R.string.str_about));
            about.setLayoutParams(lp);
            profilelayout.addView(about);

            youtubeidvid = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
            youtubeidvid.setSingleLine(false);
            youtubeidvid.setHintTextColor(Color.parseColor("#ffffff"));
            youtubeidvid.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 14, 0, 0);
            youtubeidvid.setHint(getResources().getString(R.string.str_keshor));
            youtubeidvid.setLayoutParams(lp);
            profilelayout.addView(youtubeidvid);

            isDeceased = new CheckBox(this);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 14, 0, 0);
            isDeceased.setText(R.string.str_ispersondead);
            isDeceased.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isDeceased.isChecked()) {
                        addDeadEditTexts();
                    } else {
                        isdeceasedlayout.removeAllViews();
                        fDday = null;
                        fDmonth = null;
                        fDyear = null;
                    }
                }
            });
            profilelayout.addView(isDeceased);

            isdeceasedlayout = new LinearLayout(this);
            isdeceasedlayout.setOrientation(LinearLayout.VERTICAL);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            isdeceasedlayout.setLayoutParams(lp);
            profilelayout.addView(isdeceasedlayout);

            withOutPhone = new CheckBox(ProfileView.this);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 14, 0, 0);
            withOutPhone.setText(getResources().getString(R.string.str_withoutphone));
            profilelayout.addView(withOutPhone);

            final Button savenewUser = new Button(this);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 130, 0, 0);
            savenewUser.setText(R.string.str_save);
            savenewUser.setBackgroundResource(R.drawable.drawable_test);
            savenewUser.setLayoutParams(lp);
            savenewUser.setTextColor(Color.WHITE);
            savenewUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String Fullname = fullname.getText().toString().trim();
                    Fullname = Fullname.replace(" ", "");
                    String Lastname = lastname.getText().toString().trim();
                    Lastname = Lastname.replace(" ", "");
                    Phone = phone.getText().toString().trim();
                    String About = about.getText().toString().trim();
                    String Yotube= youtubeidvid.getText().toString().trim();
                    String Day1 = day.getText().toString().trim();
                    String Month1 = month.getText().toString().trim();
                    String Year1 = year.getText().toString().trim();
                    int Day=0,Month=0,Year=0;
                    if(!Day1.equals("")) {
                        Day = Integer.parseInt(Day1);
                    }
                    if(!Month1.equals("")) {
                        Month = Integer.parseInt(Month1);
                    }
                    if(!Year1.equals("")) {
                        Year = Integer.parseInt(Year1);
                    }

                    final String gender;
                    if (male.isChecked()) {
                        gender = "male";
                    } else
                        gender = "female";

                    if (TextUtils.isEmpty(Fullname)) {
                        Toast.makeText(ProfileView.this,R.string.str_error_enterfullname, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (TextUtils.isEmpty(Lastname)) {
                        Toast.makeText(ProfileView.this,R.string.str_error_enterlastname, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (TextUtils.isEmpty(gender)) {
                        Toast.makeText(ProfileView.this, R.string.str_error_chooseGender, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if(!isDeceased.isChecked()&&!withOutPhone.isChecked()) {
                        if (TextUtils.isEmpty(Phone)) {
                            Toast.makeText(ProfileView.this, R.string.str_error_enternumber, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if(Phone.length()==9){
                            Phone = "+"+getplace(kedomet.getSelectedItem().toString())+Phone;
                            Log.e("onClick: ",Phone+ " lslslslslslslslslslslslslsl" );
                        }
                        else if(Phone.length() == 10){
                            Phone = Phone.substring(1);
                            Phone = "+"+getplace(kedomet.getSelectedItem().toString())+Phone;
                            Log.e("onClick: ",Phone+ " lslslslslslslslslslslslslsl" );
                        }
                        else {
                            Toast.makeText(ProfileView.this, R.string.str_error_entervalidphone, Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                    else{
                        Phone = "";
                    }
                    if (TextUtils.isEmpty(About)) {
                        About = " ";
                    }
                    if (TextUtils.isEmpty(Day1) || TextUtils.isEmpty(Month1) || TextUtils.isEmpty(Year1)) {
                        Toast.makeText(ProfileView.this,R.string.str_error_enterdateofbirth, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (Day > 0 && Day < 32) {

                    } else {
                        Toast.makeText(ProfileView.this, R.string.str_error_entervalidday, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (Month > 0 && Month < 13) {

                    } else {
                        Toast.makeText(ProfileView.this, R.string.str_error_entervalidmonth, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (Year > 0 && Year < 2020) {

                    } else {
                        Toast.makeText(ProfileView.this,R.string.str_error_entervalidyear, Toast.LENGTH_SHORT).show();
                        return;
                    }


                    if (isDeceased.isChecked()) {
                        FDday = fDday.getText().toString().trim();
                        FDmonth = fDmonth.getText().toString().trim();
                        FDyear = fDyear.getText().toString().trim();



                        if (TextUtils.isEmpty(FDday) || TextUtils.isEmpty(FDmonth) || TextUtils.isEmpty(FDyear)) {
                            Toast.makeText(ProfileView.this, R.string.str_error_enterdateofdeath, Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                    String id = dataBaseUser.push().getKey();
                    //String id, String fullname, String phone, String dateofbirth, String description, String activated, String email, String dateofdeath, String did)
                    String Ddate;
                    if (TextUtils.isEmpty(FDday)) {
                        Ddate = "";
                    } else {
                        Ddate = FDday + "/" + FDmonth + "/" + FDyear;
                    }
                    final User_toUpload temp = new User_toUpload(id, Fullname+ " "+Lastname, Phone, Day + "/" + Month + "/" + Year, About, "inactive", "", Ddate, "");

                    if(!TextUtils.isEmpty(Yotube))
                        try {
                            String s = Yotube.split("/")[3];
                            temp.setYoutubeid(Yotube);
                        }
                        catch (Exception e){
                            Toast.makeText(ProfileView.this,getResources().getString(R.string.str_error_invalidyoutubelink),Toast.LENGTH_LONG).show();
                            return;
                        }


                    StorageReference storageref= mStorage.child("photos").child(id);
                    progressbar.show();
                    if(!ifpicturepicked){
                        if(gender.equals("male")){
                            Uri uri = Uri.parse("android.resource://"+getPackageName()+"/drawable/male");
                            storageref.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    if (who.equals("sibling")) {
                                        temp.setGender(gender);
                                        temp.setPhotolink(taskSnapshot.getDownloadUrl().toString());
                                        fbh.SaveSibling(ProfileView.this, currentuser, temp,progressbar,father,mother);
                                    } else if (who.equals("children")) {
                                        temp.setGender(gender);
                                        temp.setPhotolink(taskSnapshot.getDownloadUrl().toString());

                                        fbh.SaveChildren(ProfileView.this, currentuser, temp, Gender,currentspouse,progressbar);
                                    }
                                }
                            });

                        }
                        else{
                            Uri uri = Uri.parse("android.resource://"+getPackageName()+"/drawable/female");

                            storageref.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    if (who.equals("sibling")) {
                                        temp.setGender(gender);
                                        temp.setPhotolink(taskSnapshot.getDownloadUrl().toString());

                                        fbh.SaveSibling(ProfileView.this, currentuser, temp,progressbar,father,mother);
                                    } else if (who.equals("children")) {
                                        temp.setGender(gender);
                                        temp.setPhotolink(taskSnapshot.getDownloadUrl().toString());

                                        fbh.SaveChildren(ProfileView.this, currentuser, temp, Gender,currentspouse,progressbar);
                                    }
                                }
                            });

                        }
                    }
                    else{
                        storageref.putFile(thepicture).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                if (who.equals("sibling")) {
                                    temp.setGender(gender);
                                    temp.setPhotolink(taskSnapshot.getDownloadUrl().toString());

                                    fbh.SaveSibling(ProfileView.this, currentuser, temp,progressbar,father,mother);
                                } else if (who.equals("children")) {
                                    temp.setGender(gender);
                                    temp.setPhotolink(taskSnapshot.getDownloadUrl().toString());

                                    fbh.SaveChildren(ProfileView.this, currentuser, temp, Gender,currentspouse,progressbar);
                                }
                            }
                        });
                    }

                    setResult(RESULT_OK);

                    savenewUser.setClickable(false);
                }
            });

            profilelayout.addView(savenewUser);

        }
        // Sets a Save User LayOut for siblings and children--------------End










        // Sets a Save User LayOut for parents and spouses--------------Start

        else {
            profilelayout.removeAllViews();

            FDday = "";
            FDmonth = "";
            FDyear = "";

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
            lp.gravity= Gravity.CENTER;
            final CircleImageView kokos = new CircleImageView(this);
            kokos.setLayoutParams(lp);
            kokos.setBorderWidth(20);
            kokos.setBorderColor(Color.parseColor("#FFFFFF"));
            profilepicture=kokos;
            profilelayout.addView(kokos);
            if(who.equals("father"))
            {
                kokos.setImageResource(R.drawable.male);
            }
            else if(who.equals("mother")){
                kokos.setImageResource(R.drawable.female);
            }
            else if(who.equals("spouse")){
                if (Gender.equals("male")) {
                    kokos.setImageResource(R.drawable.female);

                } else {
                    kokos.setImageResource(R.drawable.male);
                }
            }

            fullname = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 14, 0, 0);
            fullname.setHintTextColor(Color.parseColor("#ffffff"));
            fullname.setHint("*"+getResources().getString(R.string.str_fullname));
            fullname.setLayoutParams(lp);
            profilelayout.addView(fullname);

            lastname = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 14, 0, 0);
            lastname.setHint("*"+getResources().getString(R.string.str_lastname));
            lastname.setHintTextColor(Color.parseColor("#ffffff"));
            lastname.setLayoutParams(lp);
            profilelayout.addView(lastname);

            LinearLayout tempphonelayout = new LinearLayout(ProfileView.this);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            tempphonelayout.setOrientation(LinearLayout.HORIZONTAL);
            tempphonelayout.setLayoutParams(lp);

            kedomet = new Spinner(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,0.2f);
            kedomet.setLayoutParams(lp);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(ProfileView.this, android.R.layout.simple_spinner_dropdown_item, codes.countryCodes);
            kedomet.setAdapter(adapter);
            tempphonelayout.addView(kedomet);


            phone = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,0.8f);
            lp.setMargins(0, 14, 0, 0);
            phone.setHint("*"+getResources().getString(R.string.str_phone_number));
            phone.setHintTextColor(Color.parseColor("#ffffff"));
            phone.setInputType(InputType.TYPE_CLASS_NUMBER);
            phone.setLayoutParams(lp);
            tempphonelayout.addView(phone);

            profilelayout.addView(tempphonelayout);

            TextView dateofbirth = new TextView(this);
            dateofbirth.setText(getResources().getString(R.string.str_birth_date)+":");
            profilelayout.addView(dateofbirth);

            LinearLayout ll = new LinearLayout(this);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 14, 0, 0);
            ll.setLayoutParams(lp);

            day = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
            day.setHint("*"+getResources().getString(R.string.str_day));
            day.setHintTextColor(Color.parseColor("#ffffff"));
            day.setInputType(InputType.TYPE_CLASS_NUMBER);
            lp.setMargins(0, 0, 4, 0);
            day.setLayoutParams(lp);
            ll.addView(day);

            month = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
            month.setHint("*"+getResources().getString(R.string.str_month));
            month.setHintTextColor(Color.parseColor("#ffffff"));
            month.setInputType(InputType.TYPE_CLASS_NUMBER);
            lp.setMargins(0, 0, 4, 0);
            month.setLayoutParams(lp);
            ll.addView(month);

            year = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
            year.setHint("*"+getResources().getString(R.string.str_year));
            year.setHintTextColor(Color.parseColor("#ffffff"));
            year.setLayoutParams(lp);
            year.setInputType(InputType.TYPE_CLASS_NUMBER);
            ll.addView(year);

            profilelayout.addView(ll);

            about = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
            about.setSingleLine(false);
            about.setHintTextColor(Color.parseColor("#ffffff"));
            about.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 14, 0, 0);
            about.setHint(getResources().getString(R.string.str_about));
            about.setLayoutParams(lp);
            profilelayout.addView(about);

            youtubeidvid = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
            youtubeidvid.setSingleLine(false);
            youtubeidvid.setHintTextColor(Color.parseColor("#ffffff"));
            youtubeidvid.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 14, 0, 0);
            youtubeidvid.setHint(getResources().getString(R.string.str_keshor));
            youtubeidvid.setLayoutParams(lp);
            profilelayout.addView(youtubeidvid);


            isDeceased = new CheckBox(this);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 14, 0, 0);
            isDeceased.setText(R.string.str_ispersondead);
            isDeceased.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isDeceased.isChecked()) {
                        addDeadEditTexts();
                    } else {
                        isdeceasedlayout.removeAllViews();
                        fDday = null;
                        fDmonth = null;
                        fDyear = null;
                    }
                }
            });
            profilelayout.addView(isDeceased);

            isdeceasedlayout = new LinearLayout(this);
            isdeceasedlayout.setOrientation(LinearLayout.VERTICAL);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            isdeceasedlayout.setLayoutParams(lp);
            profilelayout.addView(isdeceasedlayout);

            withOutPhone = new CheckBox(ProfileView.this);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 14, 0, 0);
            withOutPhone.setText(getResources().getString(R.string.str_withoutphone));
            profilelayout.addView(withOutPhone);


            final Button savenewUser = new Button(this);
            lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 130, 0, 0);
            savenewUser.setText(R.string.str_save);
            savenewUser.setBackgroundResource(R.drawable.drawable_test);
            savenewUser.setLayoutParams(lp);
            savenewUser.setTextColor(Color.WHITE);
            savenewUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String Fullname = fullname.getText().toString().trim();
                    Fullname = Fullname.replace(" ", "");
                    String Lastname = lastname.getText().toString().trim();
                    Lastname = Lastname.replace(" ", "");
                    Phone = phone.getText().toString().trim();
                    String Yotube= youtubeidvid.getText().toString().trim();
                    String About = about.getText().toString().trim();
                    String Day1 = day.getText().toString().trim();
                    String Month1 = month.getText().toString().trim();
                    String Year1 = year.getText().toString().trim();
                    int Day ;
                    int Month;
                    int Year;

                    if(TextUtils.isEmpty(Day1)||TextUtils.isEmpty(Month1)||TextUtils.isEmpty(Year1)){
                        Toast.makeText(ProfileView.this,R.string.str_error_enterdateofbirth, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (TextUtils.isEmpty(Fullname)) {
                        Toast.makeText(ProfileView.this,R.string.str_error_enterfullname, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (TextUtils.isEmpty(Lastname)) {
                        Toast.makeText(ProfileView.this,R.string.str_error_enterlastname, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if(!isDeceased.isChecked()&&!withOutPhone.isChecked()) {
                        if (TextUtils.isEmpty(Phone)) {
                            Toast.makeText(ProfileView.this, R.string.str_error_enternumber, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if(Phone.length()==9){
                            Phone = "+"+getplace(kedomet.getSelectedItem().toString())+Phone;
                            Log.e("onClick: ",Phone+ " lslslslslslslslslslslslslsl" );
                        }
                        else if(Phone.length() == 10){
                            Phone = Phone.substring(1);
                            Phone = "+"+getplace(kedomet.getSelectedItem().toString())+Phone;
                            Log.e("onClick: ",Phone+ " lslslslslslslslslslslslslsl" );
                        }
                        else {
                            Toast.makeText(ProfileView.this, R.string.str_error_entervalidphone, Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                    else{
                        Phone = "";
                    }
                    Day = Integer.parseInt(day.getText().toString().trim());
                    Month= Integer.parseInt(month.getText().toString().trim());
                    Year= Integer.parseInt(year.getText().toString().trim());
                    if (TextUtils.isEmpty(About)) {
                        About = " ";
                    }
                    if (TextUtils.isEmpty(Day1) || TextUtils.isEmpty(Month1) || TextUtils.isEmpty(Year1)) {
                        Toast.makeText(ProfileView.this, R.string.str_error_enterdateofbirth, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (Day > 0 && Day < 32) {

                    } else {
                        Toast.makeText(ProfileView.this,R.string.str_error_entervalidday, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (Month > 0 && Month < 13) {

                    } else {
                        Toast.makeText(ProfileView.this,R.string.str_error_entervalidmonth, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (Year > 0 && Year < 2020) {

                    } else {
                        Toast.makeText(ProfileView.this,R.string.str_error_entervalidyear, Toast.LENGTH_SHORT).show();
                        return;
                    }


                    if (isDeceased.isChecked()) {
                        FDday = fDday.getText().toString().trim();
                        FDmonth = fDmonth.getText().toString().trim();
                        FDyear = fDyear.getText().toString().trim();

                        if (TextUtils.isEmpty(FDday) || TextUtils.isEmpty(FDmonth) || TextUtils.isEmpty(FDyear)) {
                            Toast.makeText(ProfileView.this,R.string.str_error_enterdateofdeath, Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                    String id = dataBaseUser.push().getKey();
                    //String id, String fullname, String phone, String dateofbirth, String description, String activated, String email, String dateofdeath, String did)
                    String Ddate;
                    if (TextUtils.isEmpty(FDday)) {
                        Ddate = "";
                    } else {
                        Ddate = FDday + "/" + FDmonth + "/" + FDyear;
                    }
                    final User_toUpload temp = new User_toUpload(id, Fullname+" "+Lastname, Phone, Day + "/" + Month + "/" + Year, About, "inactive", "", Ddate, "");

                    if(!TextUtils.isEmpty(Yotube))
                        try {
                            String s = Yotube.split("/")[3];
                            temp.setYoutubeid(Yotube);
                        }
                        catch (Exception e){
                            Toast.makeText(ProfileView.this,getResources().getString(R.string.str_error_invalidyoutubelink),Toast.LENGTH_LONG).show();
                            return;
                        }

                    StorageReference storageref= mStorage.child("photos").child(id);

                    //Here it checkes if its the father we're adding or mother also checks if the spouse is male or female

                    progressbar.show();
                    if(!ifpicturepicked) {
                        if (who.equals("father")) {
                            Uri uri = Uri.parse("android.resource://"+getPackageName()+"/drawable/male");
                            storageref.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    temp.setGender("male");
                                    temp.setPhotolink(taskSnapshot.getDownloadUrl().toString());
                                    savenewUser.setClickable(false);
                                    setResult(RESULT_OK);

                                    fbh.SaveParent(ProfileView.this, currentuser, temp, progressbar);
                                }
                            });
                        } else if (who.equals("mother")) {
                            Uri uri = Uri.parse("android.resource://"+getPackageName()+"/drawable/female");
                            storageref.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    temp.setGender("female");
                                    temp.setPhotolink(taskSnapshot.getDownloadUrl().toString());

                                    savenewUser.setClickable(false);
                                    setResult(RESULT_OK);

                                    fbh.SaveParent(ProfileView.this, currentuser, temp, progressbar);

                                }
                            });
                        } else if (who.equals("spouse")) {
                            if (Gender.equals("male")) {
                                Uri uri = Uri.parse("android.resource://"+getPackageName()+"/drawable/female");
                                storageref.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                        temp.setGender("female");
                                        temp.setPhotolink(taskSnapshot.getDownloadUrl().toString());

                                        savenewUser.setClickable(false);
                                        setResult(RESULT_OK);

                                        fbh.SaveSpouse(ProfileView.this, currentuser, temp, progressbar);

                                    }
                                });
                            } else {
                                Uri uri = Uri.parse("android.resource://"+getPackageName()+"/drawable/male");
                                storageref.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                        temp.setGender("male");
                                        temp.setPhotolink(taskSnapshot.getDownloadUrl().toString());

                                        savenewUser.setClickable(false);
                                        setResult(RESULT_OK);

                                        fbh.SaveSpouse(ProfileView.this, currentuser, temp, progressbar);

                                    }
                                });
                            }
                        }
                    }
                    else{
                        if (who.equals("father")) {
                            storageref.putFile(thepicture).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    temp.setGender("male");
                                    temp.setPhotolink(taskSnapshot.getDownloadUrl().toString());

                                    savenewUser.setClickable(false);
                                    setResult(RESULT_OK);

                                    fbh.SaveParent(ProfileView.this, currentuser, temp, progressbar);

                                }
                            });
                        } else if (who.equals("mother")) {
                            storageref.putFile(thepicture).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    temp.setGender("female");
                                    temp.setPhotolink(taskSnapshot.getDownloadUrl().toString());

                                    fbh.SaveParent(ProfileView.this, currentuser, temp, progressbar);
                                    savenewUser.setClickable(false);
                                    setResult(RESULT_OK);


                                }
                            });
                        } else if (who.equals("spouse")) {
                            if (Gender.equals("male")) {
                                storageref.putFile(thepicture).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                        temp.setGender("female");
                                        temp.setPhotolink(taskSnapshot.getDownloadUrl().toString());

                                        fbh.SaveSpouse(ProfileView.this, currentuser, temp, progressbar);
                                        setResult(RESULT_OK);

                                        savenewUser.setClickable(false);

                                    }
                                });
                            } else {
                                storageref.putFile(thepicture).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                        temp.setGender("male");
                                        temp.setPhotolink(taskSnapshot.getDownloadUrl().toString());
                                        fbh.SaveSpouse(ProfileView.this, currentuser, temp, progressbar);
                                        setResult(RESULT_OK);

                                        savenewUser.setClickable(false);
                                    }
                                });
                            }
                        }
                    }
                }
            });

            profilelayout.addView(savenewUser);
        }
        profilepicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ifpicturepicked = true;
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                int gallery = 1;

                startActivityForResult(pickPhoto,gallery);
            }
        });

        // Sets a Save User LayOut for parents and spouses--------------End

    }


    //sets the pop up view for this activity
    private void setPopUp() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .8), (int) (height * 0.8));

    }


    //After we select and image for profile the code to save and display is in this function
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            ifpicturepicked = true;
            Uri selectedImage = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
            } catch (IOException e) {
                e.printStackTrace();
            }

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);



            Glide.with(ProfileView.this)
                    .load(stream.toByteArray())
                    .asBitmap()
                    .into(profilepicture);
            thepicture = selectedImage;

        }
        if(resultCode == RESULT_FIRST_USER){
            User_toUpload kopop = (User_toUpload) data.getSerializableExtra("backeduser");
            person.setLat(kopop.getLat());
            person.setLong(kopop.getLong());
        }


    }
    //After we select and image for profile the code to save and display is in this function


    //asks permission to access location
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case 200:
                if(grantResults[0]==PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(ProfileView.this, R.string.str_error_permissiongranted, Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(ProfileView.this, R.string.str_error_cantsavelocationwithoutpermission, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    //when adding a new user and we push the deceased option for the user
    //this function adds all the edit texts needed to add a deceased user
    private void addDeadEditTexts(){
        LinearLayout ll = new LinearLayout(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0,14,0,0);
        ll.setLayoutParams(lp);

        fDday = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
        lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f);
        fDday.setHint("*"+getResources().getString(R.string.str_day));
        fDday.setHintTextColor(Color.parseColor("#ffffff"));
        fDday.setInputType(InputType.TYPE_CLASS_NUMBER);
        lp.setMargins(0,0,4,0);
        fDday.setLayoutParams(lp);
        ll.addView(fDday);

        fDmonth = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
        lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f);
        fDmonth.setHint("*"+getResources().getString(R.string.str_month));
        fDmonth.setHintTextColor(Color.parseColor("#ffffff"));
        lp.setMargins(0,0,4,0);
        fDmonth.setInputType(InputType.TYPE_CLASS_NUMBER);
        fDmonth.setLayoutParams(lp);
        ll.addView(fDmonth);

        fDyear = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
        lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f);
        fDyear.setHint("*"+getResources().getString(R.string.str_year));
        fDyear.setHintTextColor(Color.parseColor("#ffffff"));
        fDyear.setInputType(InputType.TYPE_CLASS_NUMBER);
        fDyear.setLayoutParams(lp);
        ll.addView(fDyear);

        isdeceasedlayout.addView(ll);
    }

    //when we are viewing a user profile and he is deceased this funciton adds
    //the texts to display information of his death
    private void addDeadEditTextsfordead(){
        LinearLayout ll = new LinearLayout(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0,14,0,0);
        ll.setLayoutParams(lp);

        fDday = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
        lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f);
        fDday.setHint("*"+getResources().getString(R.string.str_day));
        fDday.setInputType(InputType.TYPE_CLASS_NUMBER);
        lp.setMargins(0,0,4,0);
        if(!person.getDateofdeath().equals(""))
            fDday.setText(person.getDateofdeath().split("/")[0]);
        fDday.setLayoutParams(lp);
        ll.addView(fDday);

        fDmonth = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
        lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f);
        fDmonth.setHint("*"+getResources().getString(R.string.str_month));
        lp.setMargins(0,0,4,0);
        fDmonth.setInputType(InputType.TYPE_CLASS_NUMBER);
        if(!person.getDateofdeath().equals(""))
            fDmonth.setText(person.getDateofdeath().split("/")[1]);
        fDmonth.setLayoutParams(lp);
        ll.addView(fDmonth);

        fDyear = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
        lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f);
        fDyear.setHint("*"+getResources().getString(R.string.str_year));
        fDyear.setInputType(InputType.TYPE_CLASS_NUMBER);
        if(!person.getDateofdeath().equals(""))
            fDyear.setText(person.getDateofdeath().split("/")[2]);
        fDyear.setLayoutParams(lp);
        ll.addView(fDyear);

        isdeceasedlayout.addView(ll);
    }


    //A function that came with using the youtube api to displaying youtube videos
    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        youTubePlayer.cueVideo(VIDEOID);
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }
    //A function that came with using the youtube api to displaying youtube videos


    //gets all country kedomot for the phone
    private String getplace(String k){
        int j=-1;
        for(int i = 0;i<codes.countryCodes.length;i++){
            if(codes.countryCodes[i].equals(k)){
                j = i;
            }
        }
        return codes.countryAreaCodes[j];
    }


    //the function that translates our regular date to jewish date
    public static CalendarDate gregorian2Jewish(CalendarDate date2Convert, CalendarImpl i) {
        int absolute = i.absoluteFromGregorianDate(date2Convert);
        CalendarDate dateJewish = i.jewishDateFromAbsolute(absolute);
        return dateJewish;
    }


    private boolean checkinternetconnectivity(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;
        return  connected;
    }




}

