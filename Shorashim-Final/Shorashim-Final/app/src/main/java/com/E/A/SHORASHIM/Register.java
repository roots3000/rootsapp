package com.E.B.SHORASHIM;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Register extends AppCompatActivity {


    EditText email,pass;

    Button register;

    private ProgressDialog progressbar;

    private FirebaseAuth.AuthStateListener mAuthListener;

    private FirebaseAuth firebaseAuth;

    FirebaseUser temp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        progressbar = new ProgressDialog(this);

        firebaseAuth = FirebaseAuth.getInstance();

        email = (EditText) findViewById(R.id.RGemail);

        pass = (EditText) findViewById(R.id.RGpassword);

        register = (Button) findViewById(R.id.RGregister);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String EMAIL = email.getText().toString().trim();
                final String Pass = pass.getText().toString().trim();

                if(TextUtils.isEmpty(EMAIL)){
                    Toast.makeText(Register.this,R.string.str_error_enteremail,Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(Pass)){
                    Toast.makeText(Register.this,R.string.str_error_enterpassword,Toast.LENGTH_SHORT).show();
                    return;
                }

                progressbar.setMessage(getResources().getString(R.string.str_error_registering));
                progressbar.show();

                firebaseAuth.createUserWithEmailAndPassword(EMAIL,Pass).addOnCompleteListener(Register.this,new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressbar.dismiss();
                        progressbar.setMessage(getResources().getString(R.string.str_error_sendemailtovalidate));
                        progressbar.show();
                        if(task.isSuccessful()){
                            temp= firebaseAuth.getCurrentUser();
                            temp.sendEmailVerification();

                            Toast.makeText(Register.this,R.string.str_error_loginafteryouvalidate, Toast.LENGTH_LONG).show();
                            Intent backed = new Intent();
                            backed.putExtra("email",EMAIL);
                            backed.putExtra("pass",Pass);

                            firebaseAuth.signOut();
                            setResult(RESULT_OK,backed);
                            finish();

                        }
                        else{
                            Toast.makeText(Register.this,R.string.str_error_emailalreadyinuseorinvalid, Toast.LENGTH_LONG).show();
                            progressbar.dismiss();
                        }
                    }
                });

            }
        });

    }
}
