package com.E.B.SHORASHIM;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;

import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;
import java.sql.Blob;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Androwsa on 9/12/17.
 */

public class DataBaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = " profilessssssssssssssss.db ";
    public static final String TABLE_NAME = " profile_table1 ";
    public static final String TABLE_NAME_Parents = " parents_table ";
    public static final String TABLE_NAME_children = " children_table ";
    public static final String TABLE_NAME_siblings = " siblings_table ";
    public static final String Col_avtivated = " activated ";
    public static final String Col_dateofbirth = " dateofbirth ";
    public static final String Col_dateofdeath = " dateofdeath ";
    public static final String Col_description = " description ";
    public static final String Col_did = " did ";
    public static final String Col_email = " email ";
    public static final String Col_fullname = " fullname ";
    public static final String Col_gender = " gender ";
    public static final String Col_id = " id ";
    public static final String Col_lat = " lat ";
    public static final String Col_long = " long ";
    public static final String Col_Parent = " parent ";
    public static final String Col_phone = " phone ";
    public static final String Col_photolink = " photolink ";
    public static final String Col_Sibling = " sibling ";
    public static final String Col_Spouse = " spouse ";
    public static final String Col_Children = " children ";
    public static final String Col_yotubeid = " youtubeid ";



    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null , 12);

        SQLiteDatabase db= this.getWritableDatabase();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(" create table if not exists "+TABLE_NAME+
        "("+Col_avtivated+" TEXT," +
                Col_dateofbirth+" TEXT," +
                Col_dateofdeath+" TEXT," +
                Col_description+" TEXT," +
                Col_did+" TEXT," +
                Col_email+" TEXT," +
                Col_fullname+" TEXT," +
                Col_gender +" TEXT," +
                Col_id+" TEXT," +
                Col_lat+" TEXT," +
                Col_long+" TEXT," +
                Col_Parent+" TEXT," +
                Col_phone+" TEXT," +
                Col_photolink+" BLOB," +
                Col_Sibling+" TEXT," +
                Col_Children+" TEXT," +
                Col_yotubeid+" TEXT," +
                Col_Spouse+" TEXT" +
                ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists "+TABLE_NAME);
        onCreate(db);
    }

    public boolean  AddProfile(String activated, String dateofbirth, String dateofdeath, String descreption
            , String did, String email, String fullname, String gender, String id , String Lat, String Long
            , String  parents, String phone, Bitmap photo, String  sibling, String children, String youtubeid, String Spouse
    , Context context){

        SQLiteDatabase db= this.getWritableDatabase();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.PNG, 100, stream);

        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Col_avtivated,activated);
        contentvalues.put(Col_dateofbirth,dateofbirth);
        contentvalues.put(Col_dateofdeath,dateofdeath);
        contentvalues.put(Col_description,descreption);
        contentvalues.put(Col_did,did);
        contentvalues.put(Col_email,email);
        contentvalues.put(Col_fullname,fullname);
        contentvalues.put(Col_gender,gender);
        contentvalues.put(Col_id,id);
        contentvalues.put(Col_lat,Lat);
        contentvalues.put(Col_long,Long);
        contentvalues.put(Col_Parent,parents);
        contentvalues.put(Col_phone,phone);
        contentvalues.put(Col_photolink,stream.toByteArray());
        contentvalues.put(Col_Sibling, sibling);
        contentvalues.put(Col_Children, children);
        contentvalues.put(Col_yotubeid, youtubeid);
        contentvalues.put(Col_Spouse, Spouse);
        if(db.insert(TABLE_NAME,null,contentvalues) == -1)return false;

        return true;
    }

    public Cursor getalldata(){
        SQLiteDatabase db= this.getWritableDatabase();
        try {
            Cursor res = db.rawQuery("select * from " + TABLE_NAME, null);
            return res;
        }
        catch(Exception e){

        }
        return null;
    }

    public boolean updatedata(String activated, String dateofbirth, String dateofdeath, String descreption
            , String did, String email, String fullname, String gender, String id , String Lat, String Long
            , String  parents, String phone, Bitmap photo, String  sibling, String children, String youtubeid, String Spouse){
        SQLiteDatabase db= this.getWritableDatabase();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.PNG, 100, stream);

        ContentValues contentvalues = new ContentValues();
        contentvalues.put(Col_avtivated,activated);
        contentvalues.put(Col_dateofbirth,dateofbirth);
        contentvalues.put(Col_dateofdeath,dateofdeath);
        contentvalues.put(Col_description,descreption);
        contentvalues.put(Col_did,did);
        contentvalues.put(Col_email,email);
        contentvalues.put(Col_fullname,fullname);
        contentvalues.put(Col_gender,gender);
        contentvalues.put(Col_id,id);
        contentvalues.put(Col_lat,Lat);
        contentvalues.put(Col_long,Long);
        contentvalues.put(Col_Parent,parents);
        contentvalues.put(Col_phone,phone);
        contentvalues.put(Col_photolink,stream.toByteArray());
        contentvalues.put(Col_Sibling, sibling);
        contentvalues.put(Col_Children, children);
        contentvalues.put(Col_yotubeid, youtubeid);
        contentvalues.put(Col_Spouse, Spouse);

        String[] temp = new String[1];
        temp[0]= id;
        db.update(TABLE_NAME,contentvalues,Col_id+" = ?",temp);

        return true;
    }
    public boolean updateSpouse(String mainUser, String spouseUser){
        return true;
    }

    public void droptable() {
        SQLiteDatabase db= this.getWritableDatabase();
        db.execSQL("drop table if exists "+TABLE_NAME);
    }


    public void sync(User userprofile,Context context) {
        DataBaseHelper db;
        db = new DataBaseHelper(context);

        Cursor pos = getalldata();

        String parents ="";
        String children ="";
        String siblings ="";
        String spouses ="";
        if(userprofile.getParent()[0]==null){
            parents +=null+"/";
        }
        else
            parents += userprofile.getParent()[0]+"/";

        if(userprofile.getParent()[1]==null){
            parents +=null;
        }
        else
            parents += userprofile.getParent()[1];

        for(int i = 0;i<userprofile.getChildren().size()-1;i++){
            children+=userprofile.getChildren().get(i)+"/";
        }
        if(userprofile.getChildren().size()!=0)
        children+=userprofile.getChildren().get(userprofile.getChildren().size()-1);

        for(int i = 0;i<userprofile.getSibling().size()-1;i++){
            siblings+=userprofile.getSibling().get(i)+"/";
        }
        if(userprofile.getSibling().size()!=0)
        siblings+=userprofile.getSibling().get(userprofile.getSibling().size()-1);

        for(int i = 0;i<userprofile.getSpouse().size()-1;i++){
            spouses+=userprofile.getSpouse().get(i)+"/";
        }
        if(userprofile.getSpouse().size()!=0)
        spouses+=userprofile.getSpouse().get(userprofile.getSpouse().size()-1);

        if(pos!=null&&pos.getCount()>0)
        while (pos.moveToNext()) {

                if (pos.getString(8) != null)
                    if (pos.getString(8).equals(userprofile.getId())) {
                        db.updatedata(userprofile.getActivated(), userprofile.getDateofbirth(), userprofile.getDateofdeath(), userprofile.getDescription(),
                                userprofile.getDid(), userprofile.getEmail(), userprofile.getFullname(), userprofile.getGender(), userprofile.getId(),
                                userprofile.getLat(), userprofile.getLong(), parents, userprofile.getPhone(), userprofile.getPhotolink(), siblings, children, userprofile.getYoutubeid(), spouses);
                        return;
                    }

        }
        db.AddProfile(userprofile.getActivated(), userprofile.getDateofbirth(), userprofile.getDateofdeath(), userprofile.getDescription(),
                userprofile.getDid(), userprofile.getEmail(), userprofile.getFullname(), userprofile.getGender(), userprofile.getId(),
                userprofile.getLat(), userprofile.getLong(), parents, userprofile.getPhone(), userprofile.getPhotolink(), siblings, children, userprofile.getYoutubeid(), spouses,context);

    }



    public boolean checkifexistsinsql(String main){
        Cursor r = getalldata();
        while (r.moveToNext()){
            try {
                if (r.getString(8) != null)
                    if (r.getString(8).equals(main)) {
                        return true;
                    }
            }
            catch (Exception e){}
        }
        return false;
    }
}
