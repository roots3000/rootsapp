package com.E.B.SHORASHIM;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    EditText email,pass;

    Button login,register;

    TextView forgotText;

    SharedPreferences sharedpref;

    SharedPreferences.Editor editor;

    DatabaseReference dataBaseUser;

    private ProgressDialog progressbar;

    private FirebaseAuth firebaseauth;

    private FirebaseUser temp;

    private FirebaseHandler myHandler;

    Button Lang;

    private static String ifloggedin = "iflogged";
    private static String Currentuser = "currentuser";
    private static String language = "languagess";

    private String LANG_CURRENT = "en";

    // Loging Page



    //Called when we register, it puts the email and password automaticly when email verfication is sent
    //also when we change language
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){

            String em = data.getExtras().getString("email");
            String ps = data.getExtras().getString("pass");
            email.setText(em);
            pass.setText(ps);
        }

        if( resultCode == RESULT_FIRST_USER){
            String languageToLoad = data.getExtras().getString(language);// your language

            editor.putString(language,languageToLoad);
            editor.commit();

            changelang(languageToLoad);
            classsetup();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        classsetup();
    }

    private boolean checkinternetconnectivity(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;
        return  connected;
    }

    private void classsetup(){
        sharedpref = this.getSharedPreferences("shared",MODE_PRIVATE);
        editor = sharedpref.edit();

        String languageToLoad="";

        String temporary1= sharedpref.getString(language,"");
        try {
            languageToLoad = sharedpref.getString(language, temporary1); // your language
        }
        catch(Exception e){}
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        this.setContentView(R.layout.activity_main);



        Lang= (Button) findViewById(R.id.langMain);
        Lang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Tree = new Intent(MainActivity.this, LangChoose.class);

                MainActivity.this.startActivityForResult(Tree,0);
            }
        });

        RemoteViews rmv = new RemoteViews(getPackageName(),R.layout.activity_main);

        TextView title = (TextView) findViewById(R.id.textView);
        title.setText(R.string.str_shorashim);

        email = (EditText) findViewById(R.id.LGemail);
        pass = (EditText) findViewById(R.id.LGpassword);

        login = (Button) findViewById(R.id.LGlogin);
        register = (Button) findViewById(R.id.LGregister);

        forgotText = (TextView) findViewById(R.id.LGforgotpass);

        dataBaseUser = FirebaseDatabase.getInstance().getReference("userprofile");

        progressbar = new ProgressDialog(this);

        firebaseauth = FirebaseAuth.getInstance();


        // eza fee internet bet7aber l3ade mn lfirebase
        if(checkinternetconnectivity()) {
            final FirebaseUser temp2 = firebaseauth.getCurrentUser();
            if (temp2 != null) {
                progressbar.setMessage(getResources().getString(R.string.str_error_loggingin));
                progressbar.show();
                myHandler = new FirebaseHandler(null, MainActivity.this);
                dataBaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        boolean flag = true;
                        for (DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                            try {
                                String email = usersnapshot.child("email").getValue(String.class);
                                if (email.equals(temp2.getEmail())) {
                                    //open Tree
                                    Intent Tree = new Intent(MainActivity.this, TreeLayoutTemp.class);
                                    Tree.putExtra("user", usersnapshot.child("id").getValue(String.class));
                                    editor.putInt(ifloggedin, 1);
                                    editor.putString(Currentuser, usersnapshot.child("id").getValue(String.class));
                                    editor.commit();
                                    MainActivity.this.startActivity(Tree);
                                    MainActivity.this.finish();
                                    flag = false;
                                    progressbar.dismiss();
                                }
                            } catch (Exception e) {
                            }
                        }
                        if (flag) {
                            MainActivity.this.startActivity(new Intent(MainActivity.this, SaveData.class));
                            progressbar.dismiss();
                        }
                        progressbar.dismiss();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }
        //eza fsh internet az 3n tree2 lsharedprefrence wb3ml display lal SQL DATA
        else{
            //connect using sharedprefrence
            int temp = sharedpref.getInt(ifloggedin,-1);
            if(temp==1){
                Intent Tree = new Intent(MainActivity.this, TreeLayoutTemp.class);
                String temporary= sharedpref.getString(Currentuser,"");
                sharedpref.getString(Currentuser,temporary);
                Tree.putExtra("user", temporary);
                MainActivity.this.startActivity(Tree);
                MainActivity.this.finish();
                progressbar.dismiss();
            }
        }


        //Forgot Password
        forgotText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkinternetconnectivity())
                    MainActivity.this.startActivity(new Intent(MainActivity.this,Password.class));

                else
                    Toast.makeText(MainActivity.this,R.string.str_error_nointernetconnection,Toast.LENGTH_LONG).show();

            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkinternetconnectivity()){
                    final String EMAIL = email.getText().toString().trim();
                    final String Pass = pass.getText().toString().trim();

                    if (TextUtils.isEmpty(EMAIL)) {
                        Toast.makeText(MainActivity.this, R.string.str_error_enteremail, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (TextUtils.isEmpty(Pass)) {
                        Toast.makeText(MainActivity.this, R.string.str_error_enterpassword, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    progressbar.setMessage(getResources().getString(R.string.str_error_loggingin) + "");
                    progressbar.show();

                    firebaseauth.signInWithEmailAndPassword(EMAIL, Pass).addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                temp = firebaseauth.getCurrentUser();
                                if (temp.isEmailVerified()) {
//                                dataBaseUser.child("1").setValue(user);
//                                dataBaseUser.child("2").setValue(user);
                                    myHandler = new FirebaseHandler(null, MainActivity.this);
                                    dataBaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            boolean flag = true;
                                            for (DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                                                try {
                                                    String email = usersnapshot.child("email").getValue(String.class);
                                                    if (email.equals(temp.getEmail())) {
                                                        //open Tree
                                                        Intent Tree = new Intent(MainActivity.this, TreeLayoutTemp.class);
                                                        Tree.putExtra("user", usersnapshot.child("id").getValue(String.class));
                                                        editor.putInt(ifloggedin, 1);
                                                        editor.putString(Currentuser, usersnapshot.child("id").getValue(String.class));
                                                        editor.commit();
                                                        MainActivity.this.startActivity(Tree);
                                                        MainActivity.this.finish();
                                                        flag = false;
                                                    }
                                                } catch (Exception e) {
                                                }
                                            }
                                            if (flag) {
                                                MainActivity.this.startActivity(new Intent(MainActivity.this, SaveData.class));
                                            }
                                            progressbar.dismiss();
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                                } else {
                                    Toast.makeText(MainActivity.this, R.string.str_error_emailnotverified, Toast.LENGTH_SHORT).show();
                                    progressbar.dismiss();
                                }

                            } else {
                                Toast.makeText(MainActivity.this, R.string.str_error_usernameorpasswordincorrect, Toast.LENGTH_SHORT).show();
                                progressbar.dismiss();

                            }
                        }
                    });
                }
                else{
                    Toast.makeText(MainActivity.this,R.string.str_error_nointernetconnection,Toast.LENGTH_LONG).show();
                }
            }

        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkinternetconnectivity())
                    startActivityForResult(new Intent(MainActivity.this, Register.class),0);

                else
                    Toast.makeText(MainActivity.this,R.string.str_error_nointernetconnection,Toast.LENGTH_LONG).show();
            }
        });
    }

    private void changelang(String lang){
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        this.setContentView(R.layout.activity_main);
    }
}
