package com.E.B.SHORASHIM;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageButton;

import java.util.Locale;

public class LangChoose extends AppCompatActivity {

    private void setPopUp() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .7), (int) (height * 0.8));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lang_choose);
        setPopUp();

        sharedpref = this.getSharedPreferences("shared",MODE_PRIVATE);
        editor = sharedpref.edit();

        ImageButton england = (ImageButton) findViewById(R.id.imageButtonEngland);
        england.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backed = new Intent();
                setResult(RESULT_FIRST_USER,backed);
                backed.putExtra(language,"en");
                editor.putString(language,"en");
                editor.commit();
                LangChoose.this.finish();
            }
        });


        ImageButton israel = (ImageButton) findViewById(R.id.imageButtonIsrael);
        israel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backed = new Intent();
                editor.putString(language,"he");
                editor.commit();
                setResult(RESULT_FIRST_USER,backed);
                backed.putExtra(language,"he");
                LangChoose.this.finish();
            }
        });

    }
    public void changeLang(Context context, String lang) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("Language", lang);
        editor.apply();
    }

    SharedPreferences sharedpref;

    SharedPreferences.Editor editor;

    private static String language = "languagess";

}
