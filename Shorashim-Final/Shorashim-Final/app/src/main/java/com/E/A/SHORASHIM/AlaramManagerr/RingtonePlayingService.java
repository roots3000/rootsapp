package com.E.B.SHORASHIM.AlaramManagerr;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.E.A.SHORASHIM.R;

import java.util.Random;

public class RingtonePlayingService extends Service {

    private boolean isRunning;
    private Context context;
    MediaPlayer mMediaPlayer;
    private int startId;



    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.e("MyActivity", "In the Richard service");
        return null;
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {

        String name = intent.getExtras().getString("fullname");
        String birthdayORdateofdeath = intent.getExtras().getString("or");

        String birthday ="", dateofdeath = "";
        if(birthdayORdateofdeath.equals("birthday"))
            birthday = intent.getExtras().getString("birthday");
        else
            dateofdeath = intent.getExtras().getString("dateofdeath");


        final NotificationManager mNM = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);

        Intent intent1 = new Intent(this.getApplicationContext(), null);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent1, 0);

        Notification mNotify;
        if(birthdayORdateofdeath.equals(birthday)) {
            mNotify = new Notification.Builder(this)
                    .setContentTitle(getResources().getString(R.string.reminder_1))
                    .setContentText(name +"'s"+getResources().getString(R.string.reminder_6)+birthday)
                    .setSmallIcon(R.drawable.rootss)
                    .setContentIntent(pIntent)
                    .setAutoCancel(true)
                    .build();
        }
        else{
            mNotify = new Notification.Builder(this)
                    .setContentTitle(getResources().getString(R.string.reminder_1))
                    .setContentText(name +"'s"+getResources().getString(R.string.reminder_5)+dateofdeath)
                    .setSmallIcon(R.drawable.rootss)
                    .setContentIntent(pIntent)
                    .setAutoCancel(true)
                    .build();
        }

        String state = intent.getExtras().getString("extra");

        Log.e("what is going on here  ", state);

        assert state != null;
        switch (state) {
            case "no":
                startId = 0;
                break;
            case "yes":
                startId = 1;
                break;
            default:
                startId = 0;
                break;
        }


        if(!this.isRunning && startId == 1) {
            Log.e("if there was not sound ", " and you want start");

            mMediaPlayer = MediaPlayer.create(this, R.raw.gets_in_the_way);

            mMediaPlayer.start();

            Random r = new Random();

            mNM.notify(r.nextInt(100000), mNotify);

            this.isRunning = true;
            this.startId = 0;

        }
        else if (!this.isRunning && startId == 0){
            Log.e("if there was not sound ", " and you want end");

            this.isRunning = false;
            this.startId = 0;

        }

        else if (this.isRunning && startId == 1){
            Log.e("if there is sound ", " and you want start");

            this.isRunning = true;
            this.startId = 0;

        }
        else {
            Log.e("if there is sound ", " and you want end");

            mMediaPlayer.stop();
            mMediaPlayer.reset();

            this.isRunning = false;
            this.startId = 0;
        }


        Log.e("MyActivity", "In the service");

        return START_NOT_STICKY;

    }


    @Override
    public void onDestroy() {
        Log.e("JSLog", "on destroy called");
        super.onDestroy();

        this.isRunning = false;
    }







}