package com.E.B.SHORASHIM;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class TreeLayoutTemp extends AppCompatActivity {

    Button Home,SignOut;

    CircleImageView currentuser, father, mother, spouse;

    //Global array for Mother and Father of current user
    User [] popo123;

    //used to check if user is logged in or not
    SharedPreferences sharedpref;
    SharedPreferences.Editor editor;

    boolean swipeleftafterlongpressfix = true;

    // bitmap helper to load all images from firebase
    Bitmap biti;

    TextView currentusertext,fathertext,mothertext,spousetext;

    ImageButton childAdd, siblingadd;

    Button languages;

    StorageReference storageRef ;

    private StorageReference mStorage;

    boolean ifCurrent;

    private float x1,x2;
    static final int MIN_DISTANCE = 150;

    LinearLayout siblingsLayout,childrenLayout;

    private FirebaseAuth firebaseauth;


    ProgressDialog progressBar;

    String Gender;

    DatabaseReference dataBaseUser;

    User currentspouse= null,CurrentUser;
    int spousecount=0;
    ArrayList<User> spouses;

    private static String ifloggedin = "iflogged";

    DataBaseHelper db;

    ArrayList<User>currentUserchildren;

    String userkey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_tree_layout_temp);

        sharedpref = this.getSharedPreferences("shared",MODE_PRIVATE);
        editor = sharedpref.edit();


        //------------A code i used to test the SQL DataBase there is no need for it now
        //------------but maybe after the rewriting we'll need it-----------------------
        db = new DataBaseHelper(this);

//        db.AddProfile("true","ssss",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
//        db.AddProfile("12","12",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
//        db.AddProfile("45","45",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);

//        db.updatedata("69","69",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);

        Cursor alldata= db.getalldata();
        StringBuffer stbuff = new StringBuffer();
        int ererte = alldata.getCount();
        if(alldata!=null)
            if(ererte == 0){
//            Toast.makeText(this,"no data", Toast.LENGTH_SHORT).show();
            }
            else{
                while (alldata.moveToNext()){
                    stbuff.append("activated :"+alldata.getString(0)+"\n");
                    stbuff.append("date of birth :"+alldata.getString(1)+"\n");
                    stbuff.append(alldata.getString(2)+"\n");
                    stbuff.append(alldata.getString(3)+"\n");
                    stbuff.append(alldata.getString(4)+"\n");
                    stbuff.append(alldata.getString(5)+"\n");
                    stbuff.append(alldata.getString(6)+"\n");
                    stbuff.append(alldata.getString(7)+"\n");
                    stbuff.append(alldata.getString(8)+"\n");
                    stbuff.append(alldata.getString(9)+"\n");
                    stbuff.append(alldata.getString(10)+"\n");
                    stbuff.append("Parents: "+alldata.getString(11)+"\n");
                    stbuff.append(alldata.getString(12)+"\n");
//                stbuff.append(alldata.getString(13)+"\n");
                    stbuff.append(alldata.getString(14)+"\n");
                    stbuff.append("children : "+alldata.getString(15)+"\n");
                    stbuff.append(alldata.getString(16)+"\n");
                    stbuff.append("spouse : "+alldata.getString(17)+"\n"+"-----------------------------"+"\n");

                }
            }

        AlertDialog.Builder erios= new AlertDialog.Builder(this);
        erios.setCancelable(true);
        erios.setTitle("erios");
        erios.setMessage(stbuff.toString());

        //to see whats in the sql database we make the erios available
//          erios.show();

        //------------A code i used to test the SQL DataBase there is no need for it now
        //------------but maybe after the rewriting we'll need it-----------------------


        languages = (Button) findViewById(R.id.TreeLanguageButton);
        languages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Tree = new Intent(TreeLayoutTemp.this, LangChoose.class);

                TreeLayoutTemp.this.startActivityForResult(Tree,0);
            }
        });


        Home = (Button) findViewById(R.id.TVhome);
        Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                classSetUp1(userkey);
            }
        });
        Home.setText(R.string.str_home);

        SharedPreferences sharedpref = this.getPreferences(MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedpref.edit();

        TextView tx5 = (TextView) findViewById(R.id.textView5);
        tx5.setText(getResources().getString(R.string.str_contactUs));

        SignOut = (Button) findViewById(R.id.TVlogout);
        SignOut.setText(R.string.str_signout);
        SignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firebaseauth.signOut();
                editor.putInt(ifloggedin,0);
                TreeLayoutTemp.this.startActivity(new Intent(TreeLayoutTemp.this,MainActivity.class));
                TreeLayoutTemp.this.finish();
            }
        });

        firebaseauth = FirebaseAuth.getInstance();

        popo123 = new User[2];

        userkey =  getIntent().getExtras().getString("user");

        mStorage = FirebaseStorage.getInstance().getReference();

        currentuser = (CircleImageView) findViewById(R.id.TVcurrentuserpic);

        father = (CircleImageView) findViewById(R.id.TVfatherpic);

        mother = (CircleImageView) findViewById(R.id.TVmotherpic);

        spouse = (CircleImageView) findViewById(R.id.TVspousepic);

        currentusertext = (TextView) findViewById(R.id.TVcurrentuser);
        fathertext = (TextView) findViewById(R.id.TVfather);
        mothertext = (TextView) findViewById(R.id.TVmother);
        spousetext = (TextView) findViewById(R.id.TVspouse);

        childAdd = (ImageButton) findViewById(R.id.TVchildrenadd);

        childAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ifCurrent) {
                    if(!checkinternetconnectivity()){Toast t =Toast.makeText(TreeLayoutTemp.this,R.string.str_error_nointernetconnection,Toast.LENGTH_SHORT);
                        t.setGravity(Gravity.CENTER,0,0);
                        t.show();
                    }
                    else {
                        Intent i = new Intent(TreeLayoutTemp.this, ProfileView.class);
                        i.putExtra("user", (Serializable) null);
                        i.putExtra("currentUser", userkey);
                        i.putExtra("who", "children");
                        i.putExtra("currentGender", CurrentUser.getGender());
                        if (currentspouse != null)
                            i.putExtra("currentspouse", currentspouse.getId());
                        else
                            i.putExtra("currentspouse", "");


                        TreeLayoutTemp.this.startActivityForResult(i, 1);
                    }
                }
            }
        });

        siblingadd = (ImageButton) findViewById(R.id.TVsiblingsadd);
        siblingadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ifCurrent) {
                    if(!checkinternetconnectivity()){Toast t =Toast.makeText(TreeLayoutTemp.this,R.string.str_error_nointernetconnection,Toast.LENGTH_SHORT);
                        t.setGravity(Gravity.CENTER,0,0);
                        t.show();
                    }
                    else {
                        Intent i = new Intent(TreeLayoutTemp.this, ProfileView.class);

                        User dad = popo123[0], mama = popo123[1];

                        Bitmap BitmapDad = null;
                        Bitmap BitmapMom = null;
                        if (dad != null)
                            BitmapDad = dad.getPhotolink();
                        if (mama != null)
                            BitmapMom = mama.getPhotolink();

                        if (dad != null)
                            dad.setPhotolink(null);
                        if (mama != null)
                            mama.setPhotolink(null);
                        i.putExtra("father1", (Serializable) dad);
                        i.putExtra("mother1", (Serializable) mama);

                        i.putExtra("user", (Serializable) null);
                        i.putExtra("currentUser", userkey);

                        i.putExtra("who", "sibling");
                        i.putExtra("currentGender", Gender);

                        TreeLayoutTemp.this.startActivityForResult(i, 1);
                        if (dad != null)
                            dad.setPhotolink(BitmapDad);
                        if (mama != null)
                            mama.setPhotolink(BitmapMom);
                    }
                }
            }
        });

        siblingsLayout = (LinearLayout) findViewById(R.id.TVsiblingsView);

        childrenLayout = (LinearLayout) findViewById(R.id.TVchildrenView);

        progressBar = new ProgressDialog(this);

        siblingsLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);


                return false;
            }
        });

        classSetUp(userkey);




    }


    private void setupp(){
        languages = (Button) findViewById(R.id.TreeLanguageButton);

        languages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Tree = new Intent(TreeLayoutTemp.this, LangChoose.class);

                TreeLayoutTemp.this.startActivityForResult(Tree,0);
            }
        });


        Home = (Button) findViewById(R.id.TVhome);
        Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                classSetUp(userkey);
            }
        });
        Home.setText(R.string.str_home);

        SharedPreferences sharedpref = this.getPreferences(MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedpref.edit();

        TextView tx5 = (TextView) findViewById(R.id.textView5);
        tx5.setText(getResources().getString(R.string.str_contactUs));

        SignOut = (Button) findViewById(R.id.TVlogout);
        SignOut.setText(R.string.str_signout);
        SignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firebaseauth.signOut();
                editor.putInt(ifloggedin,0);
                TreeLayoutTemp.this.startActivity(new Intent(TreeLayoutTemp.this,MainActivity.class));
                TreeLayoutTemp.this.finish();
            }
        });

        firebaseauth = FirebaseAuth.getInstance();

        popo123 = new User[2];


        mStorage = FirebaseStorage.getInstance().getReference();

        currentuser = (CircleImageView) findViewById(R.id.TVcurrentuserpic);

        father = (CircleImageView) findViewById(R.id.TVfatherpic);

        mother = (CircleImageView) findViewById(R.id.TVmotherpic);

        spouse = (CircleImageView) findViewById(R.id.TVspousepic);

        currentusertext = (TextView) findViewById(R.id.TVcurrentuser);
        fathertext = (TextView) findViewById(R.id.TVfather);
        mothertext = (TextView) findViewById(R.id.TVmother);
        spousetext = (TextView) findViewById(R.id.TVspouse);

        childAdd = (ImageButton) findViewById(R.id.TVchildrenadd);

        childAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ifCurrent) {
                    Intent i = new Intent(TreeLayoutTemp.this, ProfileView.class);
                    i.putExtra("user", (Serializable) null);
                    i.putExtra("currentUser", userkey);
                    i.putExtra("who", "children");
                    i.putExtra("currentGender",CurrentUser.getGender());
                    if(currentspouse!=null)
                        i.putExtra("currentspouse", currentspouse.getId());
                    else
                        i.putExtra("currentspouse", "");


                    TreeLayoutTemp.this.startActivityForResult(i, 1);
                }
            }
        });

        siblingadd = (ImageButton) findViewById(R.id.TVsiblingsadd);
        siblingadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ifCurrent) {
                    Intent i = new Intent(TreeLayoutTemp.this, ProfileView.class);

                    User dad =popo123[0],mama = popo123[1];

                    Bitmap BitmapDad = dad.getPhotolink();
                    Bitmap BitmapMom = mama.getPhotolink();
                    dad.setPhotolink(null);
                    mama.setPhotolink(null);
                    i.putExtra("father1", (Serializable) dad);
                    i.putExtra("mother1", (Serializable) mama);

                    i.putExtra("user", (Serializable) null);
                    i.putExtra("currentUser", userkey);

                    i.putExtra("who", "sibling");
                    i.putExtra("currentGender",Gender);

                    TreeLayoutTemp.this.startActivityForResult(i, 1);
                    dad.setPhotolink(BitmapDad);
                    mama.setPhotolink(BitmapMom);
                }
            }
        });

        siblingsLayout = (LinearLayout) findViewById(R.id.TVsiblingsView);

        childrenLayout = (LinearLayout) findViewById(R.id.TVchildrenView);

        progressBar = new ProgressDialog(this);

        classSetUp1(CurrentUser.getId());

    }

    //Once Tree Activity opens it calls this  function to check if the
    //current logged in user exists in the SQL DATABASE, and if it does it
    //just shows whats in the sql while in the background getting data from user
    //and his family tree from FIREBASE. if it doesnt exist then it will get data for first time
    private void classSetUp(final String key){
        currentspouse = null;
        spouses= new ArrayList<User>();
        childrenLayout.removeAllViews();
        siblingsLayout.removeAllViews();

        father.setImageResource(R.drawable.male);
        father.setBorderColor(Color.parseColor("#FFFFFF"));
        mother.setImageResource(R.drawable.female);
        mother.setBorderColor(Color.parseColor("#FFFFFF"));
        spouse.setImageResource(R.drawable.female);
        spouse.setBorderColor(Color.parseColor("#FFFFFF"));



        fathertext.setText(R.string.str_father);
        mothertext.setText(R.string.str_mother);
        spousetext.setText(R.string.str_spouse);


        if(key.equals(userkey)){
            ifCurrent = true;
        }
        else{
            ifCurrent = false;
        }

        progressBar.setMessage(getResources().getString(R.string.str_error_loading));
        progressBar.show();

        if(db.checkifexistsinsql(key)){
            classSetupForSpouseAndParents11(key);
            gettingmainuserToSync(key);
        }
        else {
            gettingmainuserToSync1(key);
        }
    }
    //Once Tree Activity opens it calls this  function to check if the
    //current logged in user exists in the SQL DATABASE, and if it does it
    //just shows whats in the sql while in the background getting data from user
    //and his family tree from FIREBASE. if it doesnt exist then it will get data for first time





    //called after tree activity has been setup. in this function it only shows whats in sql
    //and it gets the data in firebase for user and his tree(in the background)
    private void classSetUp1(final String key) {
        currentspouse = null;
        spouses = new ArrayList<User>();
        childrenLayout.removeAllViews();
        siblingsLayout.removeAllViews();

        father.setImageResource(R.drawable.male);
        father.setBorderColor(Color.parseColor("#FFFFFF"));
        mother.setImageResource(R.drawable.female);
        mother.setBorderColor(Color.parseColor("#FFFFFF"));
        spouse.setImageResource(R.drawable.female);
        spouse.setBorderColor(Color.parseColor("#FFFFFF"));


        fathertext.setText(R.string.str_father);
        mothertext.setText(R.string.str_mother);
        spousetext.setText(R.string.str_spouse);


        if (key.equals(userkey)) {
            ifCurrent = true;
        } else {
            ifCurrent = false;
        }

        progressBar.setMessage(getResources().getString(R.string.str_error_loading));
        progressBar.show();


        classSetupForSpouseAndParents11(key);

        if(checkinternetconnectivity())
            gettingmainuserToSync(key);

    }
    //called after tree activity is setup in this function it only shows whats in sql
    //and it gets the data in firebase for user and his tree(in the background)




    //the functions that end with an extra 1 are connected to classSetUp1 and the other are connected with classSetUp

    private  void gettingmainuserToSync(final String key){
        dataBaseUser = FirebaseDatabase.getInstance().getReference("userprofile");
        dataBaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                loop1:
                for( DataSnapshot usersnapshot: dataSnapshot.getChildren()){
                    if(usersnapshot.getKey().equals(key)){
                        final User tempCurrent;
                        String fullname = usersnapshot.child("fullname").getValue(String.class);
                        String dateofbirth = usersnapshot.child("dateofbirth").getValue(String.class);
                        String description = usersnapshot.child("description").getValue(String.class);
                        String phone = usersnapshot.child("phone").getValue(String.class);
                        String vidId = usersnapshot.child("youtubeid").getValue(String.class);
                        String id= usersnapshot.child("id").getValue(String.class);
                        String dateofdeath = usersnapshot.child("dateofdeath").getValue(String.class);
                        String did = usersnapshot.child("did").getValue(String.class);
                        String url = usersnapshot.child("photolink").getValue(String.class);
                        String email = usersnapshot.child("email").getValue(String.class);
                        String gender = usersnapshot.child("gender").getValue(String.class);
                        Gender= gender;
                        String activated = usersnapshot.child("activated").getValue(String.class);
                        String Lat = usersnapshot.child("lat").getValue(String.class);
                        String Long = usersnapshot.child("long").getValue(String.class);

                        String [] tempparentArray = new String[2];
                        tempparentArray[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                        tempparentArray[1] = usersnapshot.child("parent").child("mother").getValue(String.class);

                        ArrayList<String> tempspousearray = new ArrayList<String>();
                        for(DataSnapshot spousesnapshot: usersnapshot.child("spouse").getChildren()){
                            tempspousearray.add(spousesnapshot.getKey());
                        }

                        ArrayList<String> tempsiblingArray = new ArrayList<String>();
                        for(DataSnapshot spousesnapshot: usersnapshot.child("sibling").getChildren()){
                            tempsiblingArray.add(spousesnapshot.getKey());
                        }

                        ArrayList<String> tempchildrenArray = new ArrayList<String>();
                        for(DataSnapshot spousesnapshot: usersnapshot.child("children").getChildren()){
                            tempchildrenArray.add(spousesnapshot.getKey());
                        }

                        tempCurrent = new User(id,fullname,phone,dateofbirth,description,activated,email,dateofdeath,did);
                        tempCurrent.setEmail(email);
                        tempCurrent.setLat(Lat);
                        tempCurrent.setLong(Long);
                        tempCurrent.setGender(Gender);
                        tempCurrent.setYoutubeid(vidId);
                        tempCurrent.setParent(tempparentArray);
                        tempCurrent.setSpouse(tempspousearray);
                        tempCurrent.setSibling(tempsiblingArray);
                        tempCurrent.setChildren(tempchildrenArray);


                        final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                        oo.setLayoutParams(lp);
                        Glide.with(getApplicationContext())
                                .load(url)
                                .asBitmap()
                                .listener(new RequestListener<String, Bitmap>() {
                                    @Override
                                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                        biti= resource;
                                        tempCurrent.setPhotolink(biti);
                                        db.sync(tempCurrent,TreeLayoutTemp.this);
                                        ClassSync(tempCurrent);
                                        return false;
                                    }
                                })
                                .into(oo);
                        break loop1;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    private  void gettingmainuserToSync1(final String key){
        dataBaseUser = FirebaseDatabase.getInstance().getReference("userprofile");
        dataBaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                loop1:
                for( DataSnapshot usersnapshot: dataSnapshot.getChildren()){
                    if(usersnapshot.getKey().equals(key)){
                        final User tempCurrent;
                        String fullname = usersnapshot.child("fullname").getValue(String.class);
                        String dateofbirth = usersnapshot.child("dateofbirth").getValue(String.class);
                        String description = usersnapshot.child("description").getValue(String.class);
                        String phone = usersnapshot.child("phone").getValue(String.class);
                        String vidId = usersnapshot.child("youtubeid").getValue(String.class);
                        String id= usersnapshot.child("id").getValue(String.class);
                        String dateofdeath = usersnapshot.child("dateofdeath").getValue(String.class);
                        String did = usersnapshot.child("did").getValue(String.class);
                        String url = usersnapshot.child("photolink").getValue(String.class);
                        String email = usersnapshot.child("email").getValue(String.class);
                        String gender = usersnapshot.child("gender").getValue(String.class);
                        Gender= gender;
                        String activated = usersnapshot.child("activated").getValue(String.class);
                        String Lat = usersnapshot.child("lat").getValue(String.class);
                        String Long = usersnapshot.child("long").getValue(String.class);

                        String [] tempparentArray = new String[2];
                        tempparentArray[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                        tempparentArray[1] = usersnapshot.child("parent").child("mother").getValue(String.class);

                        ArrayList<String> tempspousearray = new ArrayList<String>();
                        for(DataSnapshot spousesnapshot: usersnapshot.child("spouse").getChildren()){
                            tempspousearray.add(spousesnapshot.getKey());
                        }

                        ArrayList<String> tempsiblingArray = new ArrayList<String>();
                        for(DataSnapshot spousesnapshot: usersnapshot.child("sibling").getChildren()){
                            tempsiblingArray.add(spousesnapshot.getKey());
                        }

                        ArrayList<String> tempchildrenArray = new ArrayList<String>();
                        for(DataSnapshot spousesnapshot: usersnapshot.child("children").getChildren()){
                            tempchildrenArray.add(spousesnapshot.getKey());
                        }

                        tempCurrent = new User(id,fullname,phone,dateofbirth,description,activated,email,dateofdeath,did);
                        tempCurrent.setEmail(email);
                        tempCurrent.setLat(Lat);
                        tempCurrent.setLong(Long);
                        tempCurrent.setGender(Gender);
                        tempCurrent.setYoutubeid(vidId);
                        tempCurrent.setParent(tempparentArray);
                        tempCurrent.setSpouse(tempspousearray);
                        tempCurrent.setSibling(tempsiblingArray);
                        tempCurrent.setChildren(tempchildrenArray);

                        final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                        oo.setLayoutParams(lp);
                        Glide.with(getApplicationContext())
                                .load(url)
                                .asBitmap()
                                .listener(new RequestListener<String, Bitmap>() {
                                    @Override
                                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                        biti= resource;
                                        tempCurrent.setPhotolink(biti);
                                        db.sync(tempCurrent,TreeLayoutTemp.this);
                                        ClassSync1(tempCurrent);
                                        Cursor crs = db.getalldata();
                                        if(crs.getCount()>0){
                                            classSetupForSpouseAndParents111(key);
                                        }
                                        return false;
                                    }
                                })
                                .into(oo);
                        break loop1;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    //the ClassSync function gets data from firebase and stores it in sql (runs in the background)
    //it gets data from current user and his own tree
    private void ClassSync(final User main){
        dataBaseUser = FirebaseDatabase.getInstance().getReference("userprofile");
        dataBaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int i =0;
                loop1:
                for( DataSnapshot usersnapshot: dataSnapshot.getChildren()){
                    if(usersnapshot.getKey().equals(main.getParent()[0])||usersnapshot.getKey().equals(main.getParent()[1])){
                        final User tempCurrent;
                        i = 1;
                        String fullname = usersnapshot.child("fullname").getValue(String.class);
                        String dateofbirth = usersnapshot.child("dateofbirth").getValue(String.class);
                        String description = usersnapshot.child("description").getValue(String.class);
                        String phone = usersnapshot.child("phone").getValue(String.class);
                        String vidId = usersnapshot.child("youtubeid").getValue(String.class);
                        String id= usersnapshot.child("id").getValue(String.class);
                        String dateofdeath = usersnapshot.child("dateofdeath").getValue(String.class);
                        String did = usersnapshot.child("did").getValue(String.class);
                        String url = usersnapshot.child("photolink").getValue(String.class);
                        String email = usersnapshot.child("email").getValue(String.class);
                        String gender = usersnapshot.child("gender").getValue(String.class);
                        Gender= gender;
                        String activated = usersnapshot.child("activated").getValue(String.class);
                        String Lat = usersnapshot.child("lat").getValue(String.class);
                        String Long = usersnapshot.child("long").getValue(String.class);

                        String [] tempparentArray = new String[2];
                        tempparentArray[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                        tempparentArray[1] = usersnapshot.child("parent").child("mother").getValue(String.class);

                        ArrayList<String> tempspousearray = new ArrayList<String>();
                        for(DataSnapshot spousesnapshot: usersnapshot.child("spouse").getChildren()){
                            tempspousearray.add(spousesnapshot.getKey());
                        }

                        ArrayList<String> tempsiblingArray = new ArrayList<String>();
                        for(DataSnapshot spousesnapshot: usersnapshot.child("sibling").getChildren()){
                            tempsiblingArray.add(spousesnapshot.getKey());
                        }

                        ArrayList<String> tempchildrenArray = new ArrayList<String>();
                        for(DataSnapshot spousesnapshot: usersnapshot.child("children").getChildren()){
                            tempchildrenArray.add(spousesnapshot.getKey());
                        }

                        tempCurrent = new User(id,fullname,phone,dateofbirth,description,activated,email,dateofdeath,did);
                        tempCurrent.setEmail(email);
                        tempCurrent.setLat(Lat);
                        tempCurrent.setLong(Long);
                        tempCurrent.setGender(Gender);
                        tempCurrent.setYoutubeid(vidId);
                        tempCurrent.setParent(tempparentArray);
                        tempCurrent.setSpouse(tempspousearray);
                        tempCurrent.setSibling(tempsiblingArray);
                        tempCurrent.setChildren(tempchildrenArray);

                        final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                        oo.setLayoutParams(lp);
                        Glide.with(getApplicationContext())
                                .load(url)
                                .asBitmap()
                                .listener(new RequestListener<String, Bitmap>() {
                                    @Override
                                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                        biti= resource;
                                        tempCurrent.setPhotolink(biti);

                                        ClassSyncforSecondCircle(tempCurrent);

                                        db.sync(tempCurrent,TreeLayoutTemp.this);
                                        return false;
                                    }
                                })
                                .into(oo);

                        continue loop1;
                    }

                    loop2:
                    for(String s: main.getSpouse()){
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            String fullname = usersnapshot.child("fullname").getValue(String.class);
                            String dateofbirth = usersnapshot.child("dateofbirth").getValue(String.class);
                            String description = usersnapshot.child("description").getValue(String.class);
                            String phone = usersnapshot.child("phone").getValue(String.class);
                            String vidId = usersnapshot.child("youtubeid").getValue(String.class);
                            String id= usersnapshot.child("id").getValue(String.class);
                            String dateofdeath = usersnapshot.child("dateofdeath").getValue(String.class);
                            String did = usersnapshot.child("did").getValue(String.class);
                            String url = usersnapshot.child("photolink").getValue(String.class);
                            String email = usersnapshot.child("email").getValue(String.class);
                            String gender = usersnapshot.child("gender").getValue(String.class);
                            Gender= gender;
                            String activated = usersnapshot.child("activated").getValue(String.class);
                            String Lat = usersnapshot.child("lat").getValue(String.class);
                            String Long = usersnapshot.child("long").getValue(String.class);

                            String [] tempparentArray = new String[2];
                            tempparentArray[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                            tempparentArray[1] = usersnapshot.child("parent").child("mother").getValue(String.class);

                            ArrayList<String> tempspousearray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("spouse").getChildren()){
                                tempspousearray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempsiblingArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("sibling").getChildren()){
                                tempsiblingArray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempchildrenArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("children").getChildren()){
                                tempchildrenArray.add(spousesnapshot.getKey());
                            }

                            tempCurrent = new User(id,fullname,phone,dateofbirth,description,activated,email,dateofdeath,did);
                            tempCurrent.setEmail(email);
                            tempCurrent.setLat(Lat);
                            tempCurrent.setLong(Long);
                            tempCurrent.setGender(Gender);
                            tempCurrent.setYoutubeid(vidId);
                            tempCurrent.setParent(tempparentArray);
                            tempCurrent.setSpouse(tempspousearray);
                            tempCurrent.setSibling(tempsiblingArray);
                            tempCurrent.setChildren(tempchildrenArray);

                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            ClassSyncforSecondCircle(tempCurrent);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            return false;
                                        }
                                    })
                                    .into(oo);

                            continue loop1;
                        }
                    }

                    loop2:
                    for(int j =0; j< main.getSibling().size();j++){
                        String s = main.getSibling().get(j);
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            String fullname = usersnapshot.child("fullname").getValue(String.class);
                            String dateofbirth = usersnapshot.child("dateofbirth").getValue(String.class);
                            String description = usersnapshot.child("description").getValue(String.class);
                            String phone = usersnapshot.child("phone").getValue(String.class);
                            String vidId = usersnapshot.child("youtubeid").getValue(String.class);
                            String id= usersnapshot.child("id").getValue(String.class);
                            String dateofdeath = usersnapshot.child("dateofdeath").getValue(String.class);
                            String did = usersnapshot.child("did").getValue(String.class);
                            String url = usersnapshot.child("photolink").getValue(String.class);
                            String email = usersnapshot.child("email").getValue(String.class);
                            String gender = usersnapshot.child("gender").getValue(String.class);
                            Gender= gender;
                            String activated = usersnapshot.child("activated").getValue(String.class);
                            String Lat = usersnapshot.child("lat").getValue(String.class);
                            String Long = usersnapshot.child("long").getValue(String.class);

                            String [] tempparentArray = new String[2];
                            tempparentArray[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                            tempparentArray[1] = usersnapshot.child("parent").child("mother").getValue(String.class);

                            ArrayList<String> tempspousearray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("spouse").getChildren()){
                                tempspousearray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempsiblingArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("sibling").getChildren()){
                                tempsiblingArray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempchildrenArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("children").getChildren()){
                                tempchildrenArray.add(spousesnapshot.getKey());
                            }

                            tempCurrent = new User(id,fullname,phone,dateofbirth,description,activated,email,dateofdeath,did);
                            tempCurrent.setEmail(email);
                            tempCurrent.setLat(Lat);
                            tempCurrent.setLong(Long);
                            tempCurrent.setGender(Gender);
                            tempCurrent.setYoutubeid(vidId);
                            tempCurrent.setParent(tempparentArray);
                            tempCurrent.setSpouse(tempspousearray);
                            tempCurrent.setSibling(tempsiblingArray);
                            tempCurrent.setChildren(tempchildrenArray);

                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            ClassSyncforSecondCircle(tempCurrent);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            return false;
                                        }
                                    })
                                    .into(oo);

                            continue loop1;
                        }
                    }

                    loop2:
                    for(String s: main.getChildren()){
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            String fullname = usersnapshot.child("fullname").getValue(String.class);
                            String dateofbirth = usersnapshot.child("dateofbirth").getValue(String.class);
                            String description = usersnapshot.child("description").getValue(String.class);
                            String phone = usersnapshot.child("phone").getValue(String.class);
                            String vidId = usersnapshot.child("youtubeid").getValue(String.class);
                            String id= usersnapshot.child("id").getValue(String.class);
                            String dateofdeath = usersnapshot.child("dateofdeath").getValue(String.class);
                            String did = usersnapshot.child("did").getValue(String.class);
                            String url = usersnapshot.child("photolink").getValue(String.class);
                            String email = usersnapshot.child("email").getValue(String.class);
                            String gender = usersnapshot.child("gender").getValue(String.class);
                            Gender= gender;
                            String activated = usersnapshot.child("activated").getValue(String.class);
                            String Lat = usersnapshot.child("lat").getValue(String.class);
                            String Long = usersnapshot.child("long").getValue(String.class);

                            String [] tempparentArray = new String[2];
                            tempparentArray[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                            tempparentArray[1] = usersnapshot.child("parent").child("mother").getValue(String.class);

                            ArrayList<String> tempspousearray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("spouse").getChildren()){
                                tempspousearray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempsiblingArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("sibling").getChildren()){
                                tempsiblingArray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempchildrenArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("children").getChildren()){
                                tempchildrenArray.add(spousesnapshot.getKey());
                            }

                            tempCurrent = new User(id,fullname,phone,dateofbirth,description,activated,email,dateofdeath,did);
                            tempCurrent.setEmail(email);
                            tempCurrent.setLat(Lat);
                            tempCurrent.setLong(Long);
                            tempCurrent.setGender(Gender);
                            tempCurrent.setYoutubeid(vidId);
                            tempCurrent.setParent(tempparentArray);
                            tempCurrent.setSpouse(tempspousearray);
                            tempCurrent.setSibling(tempsiblingArray);
                            tempCurrent.setChildren(tempchildrenArray);

                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            ClassSyncforSecondCircle(tempCurrent);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            return false;
                                        }
                                    })
                                    .into(oo);

                            continue loop1;
                        }
                    }
                }
//                Cursor crs = db.getalldata();
//                if(crs.getCount()>0){
//                    classSetupForSpouseAndParents11(main.getId());
//                }

            }

            @Override

            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    private void ClassSync1(final User main){
        dataBaseUser = FirebaseDatabase.getInstance().getReference("userprofile");
        dataBaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int i =0;
                loop1:
                for( DataSnapshot usersnapshot: dataSnapshot.getChildren()){
                    if(usersnapshot.getKey().equals(main.getParent()[0])||usersnapshot.getKey().equals(main.getParent()[1])){
                        final User tempCurrent;
                        i = 1;
                        String fullname = usersnapshot.child("fullname").getValue(String.class);
                        String dateofbirth = usersnapshot.child("dateofbirth").getValue(String.class);
                        String description = usersnapshot.child("description").getValue(String.class);
                        String phone = usersnapshot.child("phone").getValue(String.class);
                        String vidId = usersnapshot.child("youtubeid").getValue(String.class);
                        String id= usersnapshot.child("id").getValue(String.class);
                        String dateofdeath = usersnapshot.child("dateofdeath").getValue(String.class);
                        String did = usersnapshot.child("did").getValue(String.class);
                        String url = usersnapshot.child("photolink").getValue(String.class);
                        String email = usersnapshot.child("email").getValue(String.class);
                        String gender = usersnapshot.child("gender").getValue(String.class);
                        Gender= gender;
                        String activated = usersnapshot.child("activated").getValue(String.class);
                        String Lat = usersnapshot.child("lat").getValue(String.class);
                        String Long = usersnapshot.child("long").getValue(String.class);

                        String [] tempparentArray = new String[2];
                        tempparentArray[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                        tempparentArray[1] = usersnapshot.child("parent").child("mother").getValue(String.class);

                        ArrayList<String> tempspousearray = new ArrayList<String>();
                        for(DataSnapshot spousesnapshot: usersnapshot.child("spouse").getChildren()){
                            tempspousearray.add(spousesnapshot.getKey());
                        }

                        ArrayList<String> tempsiblingArray = new ArrayList<String>();
                        for(DataSnapshot spousesnapshot: usersnapshot.child("sibling").getChildren()){
                            tempsiblingArray.add(spousesnapshot.getKey());
                        }

                        ArrayList<String> tempchildrenArray = new ArrayList<String>();
                        for(DataSnapshot spousesnapshot: usersnapshot.child("children").getChildren()){
                            tempchildrenArray.add(spousesnapshot.getKey());
                        }

                        tempCurrent = new User(id,fullname,phone,dateofbirth,description,activated,email,dateofdeath,did);
                        tempCurrent.setEmail(email);
                        tempCurrent.setLat(Lat);
                        tempCurrent.setLong(Long);
                        tempCurrent.setGender(Gender);
                        tempCurrent.setYoutubeid(vidId);
                        tempCurrent.setParent(tempparentArray);
                        tempCurrent.setSpouse(tempspousearray);
                        tempCurrent.setSibling(tempsiblingArray);
                        tempCurrent.setChildren(tempchildrenArray);


                        final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                        oo.setLayoutParams(lp);
                        Glide.with(getApplicationContext())
                                .load(url)
                                .asBitmap()
                                .listener(new RequestListener<String, Bitmap>() {
                                    @Override
                                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                        biti= resource;
                                        tempCurrent.setPhotolink(biti);

                                        ClassSyncforSecondCircle1(tempCurrent);

                                        db.sync(tempCurrent,TreeLayoutTemp.this);
                                        Cursor crs = db.getalldata();
                                        if(crs.getCount()>0){
                                            classSetupForSpouseAndParents111(main.getId());
                                        }
                                        return false;
                                    }
                                })
                                .into(oo);

                        continue loop1;
                    }

                    loop2:
                    for(String s: main.getSpouse()){
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            String fullname = usersnapshot.child("fullname").getValue(String.class);
                            String dateofbirth = usersnapshot.child("dateofbirth").getValue(String.class);
                            String description = usersnapshot.child("description").getValue(String.class);
                            String phone = usersnapshot.child("phone").getValue(String.class);
                            String vidId = usersnapshot.child("youtubeid").getValue(String.class);
                            String id= usersnapshot.child("id").getValue(String.class);
                            String dateofdeath = usersnapshot.child("dateofdeath").getValue(String.class);
                            String did = usersnapshot.child("did").getValue(String.class);
                            String url = usersnapshot.child("photolink").getValue(String.class);
                            String email = usersnapshot.child("email").getValue(String.class);
                            String gender = usersnapshot.child("gender").getValue(String.class);
                            Gender= gender;
                            String activated = usersnapshot.child("activated").getValue(String.class);
                            String Lat = usersnapshot.child("lat").getValue(String.class);
                            String Long = usersnapshot.child("long").getValue(String.class);

                            String [] tempparentArray = new String[2];
                            tempparentArray[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                            tempparentArray[1] = usersnapshot.child("parent").child("mother").getValue(String.class);

                            ArrayList<String> tempspousearray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("spouse").getChildren()){
                                tempspousearray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempsiblingArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("sibling").getChildren()){
                                tempsiblingArray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempchildrenArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("children").getChildren()){
                                tempchildrenArray.add(spousesnapshot.getKey());
                            }

                            tempCurrent = new User(id,fullname,phone,dateofbirth,description,activated,email,dateofdeath,did);
                            tempCurrent.setEmail(email);
                            tempCurrent.setLat(Lat);
                            tempCurrent.setLong(Long);
                            tempCurrent.setGender(Gender);
                            tempCurrent.setYoutubeid(vidId);
                            tempCurrent.setParent(tempparentArray);
                            tempCurrent.setSpouse(tempspousearray);
                            tempCurrent.setSibling(tempsiblingArray);
                            tempCurrent.setChildren(tempchildrenArray);

                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            ClassSyncforSecondCircle1(tempCurrent);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            Cursor crs = db.getalldata();
                                            if(crs.getCount()>0){
                                                classSetupForSpouseAndParents111(main.getId());
                                            }
                                            return false;
                                        }
                                    })
                                    .into(oo);
                            continue loop1;
                        }
                    }

                    loop2:
                    for(int j =0; j< main.getSibling().size();j++){
                        String s = main.getSibling().get(j);
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            String fullname = usersnapshot.child("fullname").getValue(String.class);
                            String dateofbirth = usersnapshot.child("dateofbirth").getValue(String.class);
                            String description = usersnapshot.child("description").getValue(String.class);
                            String phone = usersnapshot.child("phone").getValue(String.class);
                            String vidId = usersnapshot.child("youtubeid").getValue(String.class);
                            String id= usersnapshot.child("id").getValue(String.class);
                            String dateofdeath = usersnapshot.child("dateofdeath").getValue(String.class);
                            String did = usersnapshot.child("did").getValue(String.class);
                            String url = usersnapshot.child("photolink").getValue(String.class);
                            String email = usersnapshot.child("email").getValue(String.class);
                            String gender = usersnapshot.child("gender").getValue(String.class);
                            Gender= gender;
                            String activated = usersnapshot.child("activated").getValue(String.class);
                            String Lat = usersnapshot.child("lat").getValue(String.class);
                            String Long = usersnapshot.child("long").getValue(String.class);

                            String [] tempparentArray = new String[2];
                            tempparentArray[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                            tempparentArray[1] = usersnapshot.child("parent").child("mother").getValue(String.class);

                            ArrayList<String> tempspousearray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("spouse").getChildren()){
                                tempspousearray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempsiblingArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("sibling").getChildren()){
                                tempsiblingArray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempchildrenArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("children").getChildren()){
                                tempchildrenArray.add(spousesnapshot.getKey());
                            }

                            tempCurrent = new User(id,fullname,phone,dateofbirth,description,activated,email,dateofdeath,did);
                            tempCurrent.setEmail(email);
                            tempCurrent.setLat(Lat);
                            tempCurrent.setLong(Long);
                            tempCurrent.setGender(Gender);
                            tempCurrent.setYoutubeid(vidId);
                            tempCurrent.setParent(tempparentArray);
                            tempCurrent.setSpouse(tempspousearray);
                            tempCurrent.setSibling(tempsiblingArray);
                            tempCurrent.setChildren(tempchildrenArray);

                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            ClassSyncforSecondCircle1(tempCurrent);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            Cursor crs = db.getalldata();
                                            if(crs.getCount()>0){
                                                classSetupForSpouseAndParents111(main.getId());
                                            }
                                            return false;
                                        }
                                    })
                                    .into(oo);
                            continue loop1;
                        }
                    }

                    loop2:
                    for(String s: main.getChildren()){
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            String fullname = usersnapshot.child("fullname").getValue(String.class);
                            String dateofbirth = usersnapshot.child("dateofbirth").getValue(String.class);
                            String description = usersnapshot.child("description").getValue(String.class);
                            String phone = usersnapshot.child("phone").getValue(String.class);
                            String vidId = usersnapshot.child("youtubeid").getValue(String.class);
                            String id= usersnapshot.child("id").getValue(String.class);
                            String dateofdeath = usersnapshot.child("dateofdeath").getValue(String.class);
                            String did = usersnapshot.child("did").getValue(String.class);
                            String url = usersnapshot.child("photolink").getValue(String.class);
                            String email = usersnapshot.child("email").getValue(String.class);
                            String gender = usersnapshot.child("gender").getValue(String.class);
                            Gender= gender;
                            String activated = usersnapshot.child("activated").getValue(String.class);
                            String Lat = usersnapshot.child("lat").getValue(String.class);
                            String Long = usersnapshot.child("long").getValue(String.class);

                            String [] tempparentArray = new String[2];
                            tempparentArray[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                            tempparentArray[1] = usersnapshot.child("parent").child("mother").getValue(String.class);

                            ArrayList<String> tempspousearray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("spouse").getChildren()){
                                tempspousearray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempsiblingArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("sibling").getChildren()){
                                tempsiblingArray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempchildrenArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("children").getChildren()){
                                tempchildrenArray.add(spousesnapshot.getKey());
                            }

                            tempCurrent = new User(id,fullname,phone,dateofbirth,description,activated,email,dateofdeath,did);
                            tempCurrent.setEmail(email);
                            tempCurrent.setLat(Lat);
                            tempCurrent.setLong(Long);
                            tempCurrent.setGender(Gender);
                            tempCurrent.setYoutubeid(vidId);
                            tempCurrent.setParent(tempparentArray);
                            tempCurrent.setSpouse(tempspousearray);
                            tempCurrent.setSibling(tempsiblingArray);
                            tempCurrent.setChildren(tempchildrenArray);

                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            ClassSyncforSecondCircle1(tempCurrent);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            Cursor crs = db.getalldata();
                                            if(crs.getCount()>0){
                                                classSetupForSpouseAndParents111(main.getId());
                                            }
                                            return false;
                                        }
                                    })
                                    .into(oo);
                            continue loop1;
                        }
                    }
                }

            }

            @Override

            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    //the ClassSync function gets data from firebase and stores it in sql (runs in the background)
    //it gets data from tree's of users inside the current user's tree
    private void ClassSyncforSecondCircle(final User main){
        dataBaseUser = FirebaseDatabase.getInstance().getReference("userprofile");
        dataBaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                loop1:
                for( DataSnapshot usersnapshot: dataSnapshot.getChildren()){
                    if(usersnapshot.getKey().equals(main.getParent()[0])||usersnapshot.getKey().equals(main.getParent()[1])){
                        final User tempCurrent;
                        String fullname = usersnapshot.child("fullname").getValue(String.class);
                        String dateofbirth = usersnapshot.child("dateofbirth").getValue(String.class);
                        String description = usersnapshot.child("description").getValue(String.class);
                        String phone = usersnapshot.child("phone").getValue(String.class);
                        String vidId = usersnapshot.child("youtubeid").getValue(String.class);
                        String id= usersnapshot.child("id").getValue(String.class);
                        String dateofdeath = usersnapshot.child("dateofdeath").getValue(String.class);
                        String did = usersnapshot.child("did").getValue(String.class);
                        String url = usersnapshot.child("photolink").getValue(String.class);
                        String email = usersnapshot.child("email").getValue(String.class);
                        String gender = usersnapshot.child("gender").getValue(String.class);
                        Gender= gender;
                        String activated = usersnapshot.child("activated").getValue(String.class);
                        String Lat = usersnapshot.child("lat").getValue(String.class);
                        String Long = usersnapshot.child("long").getValue(String.class);

                        String [] tempparentArray = new String[2];
                        tempparentArray[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                        tempparentArray[1] = usersnapshot.child("parent").child("mother").getValue(String.class);

                        ArrayList<String> tempspousearray = new ArrayList<String>();
                        for(DataSnapshot spousesnapshot: usersnapshot.child("spouse").getChildren()){
                            tempspousearray.add(spousesnapshot.getKey());
                        }

                        ArrayList<String> tempsiblingArray = new ArrayList<String>();
                        for(DataSnapshot spousesnapshot: usersnapshot.child("sibling").getChildren()){
                            tempsiblingArray.add(spousesnapshot.getKey());
                        }

                        ArrayList<String> tempchildrenArray = new ArrayList<String>();
                        for(DataSnapshot spousesnapshot: usersnapshot.child("children").getChildren()){
                            tempchildrenArray.add(spousesnapshot.getKey());
                        }

                        tempCurrent = new User(id,fullname,phone,dateofbirth,description,activated,email,dateofdeath,did);
                        tempCurrent.setEmail(email);
                        tempCurrent.setLat(Lat);
                        tempCurrent.setLong(Long);
                        tempCurrent.setGender(Gender);
                        tempCurrent.setYoutubeid(vidId);
                        tempCurrent.setParent(tempparentArray);
                        tempCurrent.setSpouse(tempspousearray);
                        tempCurrent.setSibling(tempsiblingArray);
                        tempCurrent.setChildren(tempchildrenArray);

                        final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                        oo.setLayoutParams(lp);
                        Glide.with(getApplicationContext())
                                .load(url)
                                .asBitmap()
                                .listener(new RequestListener<String, Bitmap>() {
                                    @Override
                                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {

                                        biti= resource;
                                        tempCurrent.setPhotolink(biti);

                                        db.sync(tempCurrent,TreeLayoutTemp.this);
                                        return false;
                                    }
                                })
                                .into(oo);


                        continue loop1;
                    }

                    loop2:
                    for(String s: main.getSpouse()){
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            String fullname = usersnapshot.child("fullname").getValue(String.class);
                            String dateofbirth = usersnapshot.child("dateofbirth").getValue(String.class);
                            String description = usersnapshot.child("description").getValue(String.class);
                            String phone = usersnapshot.child("phone").getValue(String.class);
                            String vidId = usersnapshot.child("youtubeid").getValue(String.class);
                            String id= usersnapshot.child("id").getValue(String.class);
                            String dateofdeath = usersnapshot.child("dateofdeath").getValue(String.class);
                            String did = usersnapshot.child("did").getValue(String.class);
                            String url = usersnapshot.child("photolink").getValue(String.class);
                            String email = usersnapshot.child("email").getValue(String.class);
                            String gender = usersnapshot.child("gender").getValue(String.class);
                            Gender= gender;
                            String activated = usersnapshot.child("activated").getValue(String.class);
                            String Lat = usersnapshot.child("lat").getValue(String.class);
                            String Long = usersnapshot.child("long").getValue(String.class);

                            String [] tempparentArray = new String[2];
                            tempparentArray[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                            tempparentArray[1] = usersnapshot.child("parent").child("mother").getValue(String.class);

                            ArrayList<String> tempspousearray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("spouse").getChildren()){
                                tempspousearray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempsiblingArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("sibling").getChildren()){
                                tempsiblingArray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempchildrenArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("children").getChildren()){
                                tempchildrenArray.add(spousesnapshot.getKey());
                            }

                            tempCurrent = new User(id,fullname,phone,dateofbirth,description,activated,email,dateofdeath,did);
                            tempCurrent.setEmail(email);
                            tempCurrent.setLat(Lat);
                            tempCurrent.setLong(Long);
                            tempCurrent.setGender(Gender);
                            tempCurrent.setYoutubeid(vidId);
                            tempCurrent.setParent(tempparentArray);
                            tempCurrent.setSpouse(tempspousearray);
                            tempCurrent.setSibling(tempsiblingArray);
                            tempCurrent.setChildren(tempchildrenArray);

                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {

                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            return false;
                                        }
                                    })
                                    .into(oo);


                            continue loop1;
                        }
                    }

                    loop2:
                    for(int j =0; j< main.getSibling().size();j++){
                        String s = main.getSibling().get(j);
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            String fullname = usersnapshot.child("fullname").getValue(String.class);
                            String dateofbirth = usersnapshot.child("dateofbirth").getValue(String.class);
                            String description = usersnapshot.child("description").getValue(String.class);
                            String phone = usersnapshot.child("phone").getValue(String.class);
                            String vidId = usersnapshot.child("youtubeid").getValue(String.class);
                            String id= usersnapshot.child("id").getValue(String.class);
                            String dateofdeath = usersnapshot.child("dateofdeath").getValue(String.class);
                            String did = usersnapshot.child("did").getValue(String.class);
                            String url = usersnapshot.child("photolink").getValue(String.class);
                            String email = usersnapshot.child("email").getValue(String.class);
                            String gender = usersnapshot.child("gender").getValue(String.class);
                            Gender= gender;
                            String activated = usersnapshot.child("activated").getValue(String.class);
                            String Lat = usersnapshot.child("lat").getValue(String.class);
                            String Long = usersnapshot.child("long").getValue(String.class);

                            String [] tempparentArray = new String[2];
                            tempparentArray[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                            tempparentArray[1] = usersnapshot.child("parent").child("mother").getValue(String.class);

                            ArrayList<String> tempspousearray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("spouse").getChildren()){
                                tempspousearray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempsiblingArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("sibling").getChildren()){
                                tempsiblingArray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempchildrenArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("children").getChildren()){
                                tempchildrenArray.add(spousesnapshot.getKey());
                            }

                            tempCurrent = new User(id,fullname,phone,dateofbirth,description,activated,email,dateofdeath,did);
                            tempCurrent.setEmail(email);
                            tempCurrent.setLat(Lat);
                            tempCurrent.setLong(Long);
                            tempCurrent.setGender(Gender);
                            tempCurrent.setYoutubeid(vidId);
                            tempCurrent.setParent(tempparentArray);
                            tempCurrent.setSpouse(tempspousearray);
                            tempCurrent.setSibling(tempsiblingArray);
                            tempCurrent.setChildren(tempchildrenArray);


                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {

                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            return false;
                                        }
                                    })
                                    .into(oo);


                            continue loop1;
                        }
                    }

                    loop2:
                    for(String s: main.getChildren()){
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            String fullname = usersnapshot.child("fullname").getValue(String.class);
                            String dateofbirth = usersnapshot.child("dateofbirth").getValue(String.class);
                            String description = usersnapshot.child("description").getValue(String.class);
                            String phone = usersnapshot.child("phone").getValue(String.class);
                            String vidId = usersnapshot.child("youtubeid").getValue(String.class);
                            String id= usersnapshot.child("id").getValue(String.class);
                            String dateofdeath = usersnapshot.child("dateofdeath").getValue(String.class);
                            String did = usersnapshot.child("did").getValue(String.class);
                            String url = usersnapshot.child("photolink").getValue(String.class);
                            String email = usersnapshot.child("email").getValue(String.class);
                            String gender = usersnapshot.child("gender").getValue(String.class);
                            Gender= gender;
                            String activated = usersnapshot.child("activated").getValue(String.class);
                            String Lat = usersnapshot.child("lat").getValue(String.class);
                            String Long = usersnapshot.child("long").getValue(String.class);

                            String [] tempparentArray = new String[2];
                            tempparentArray[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                            tempparentArray[1] = usersnapshot.child("parent").child("mother").getValue(String.class);

                            ArrayList<String> tempspousearray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("spouse").getChildren()){
                                tempspousearray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempsiblingArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("sibling").getChildren()){
                                tempsiblingArray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempchildrenArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("children").getChildren()){
                                tempchildrenArray.add(spousesnapshot.getKey());
                            }

                            tempCurrent = new User(id,fullname,phone,dateofbirth,description,activated,email,dateofdeath,did);
                            tempCurrent.setEmail(email);
                            tempCurrent.setLat(Lat);
                            tempCurrent.setLong(Long);
                            tempCurrent.setGender(Gender);
                            tempCurrent.setYoutubeid(vidId);
                            tempCurrent.setParent(tempparentArray);
                            tempCurrent.setSpouse(tempspousearray);
                            tempCurrent.setSibling(tempsiblingArray);
                            tempCurrent.setChildren(tempchildrenArray);


                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {

                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            return false;
                                        }
                                    })
                                    .into(oo);


                            continue loop1;
                        }
                    }
                }

            }

            @Override

            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    private void ClassSyncforSecondCircle1(final User main){
        dataBaseUser = FirebaseDatabase.getInstance().getReference("userprofile");
        dataBaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                loop1:
                for( DataSnapshot usersnapshot: dataSnapshot.getChildren()){
                    if(usersnapshot.getKey().equals(main.getParent()[0])||usersnapshot.getKey().equals(main.getParent()[1])){
                        final User tempCurrent;
                        String fullname = usersnapshot.child("fullname").getValue(String.class);
                        String dateofbirth = usersnapshot.child("dateofbirth").getValue(String.class);
                        String description = usersnapshot.child("description").getValue(String.class);
                        String phone = usersnapshot.child("phone").getValue(String.class);
                        String vidId = usersnapshot.child("youtubeid").getValue(String.class);
                        String id= usersnapshot.child("id").getValue(String.class);
                        String dateofdeath = usersnapshot.child("dateofdeath").getValue(String.class);
                        String did = usersnapshot.child("did").getValue(String.class);
                        String url = usersnapshot.child("photolink").getValue(String.class);
                        String email = usersnapshot.child("email").getValue(String.class);
                        String gender = usersnapshot.child("gender").getValue(String.class);
                        Gender= gender;
                        String activated = usersnapshot.child("activated").getValue(String.class);
                        String Lat = usersnapshot.child("lat").getValue(String.class);
                        String Long = usersnapshot.child("long").getValue(String.class);

                        String [] tempparentArray = new String[2];
                        tempparentArray[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                        tempparentArray[1] = usersnapshot.child("parent").child("mother").getValue(String.class);

                        ArrayList<String> tempspousearray = new ArrayList<String>();
                        for(DataSnapshot spousesnapshot: usersnapshot.child("spouse").getChildren()){
                            tempspousearray.add(spousesnapshot.getKey());
                        }

                        ArrayList<String> tempsiblingArray = new ArrayList<String>();
                        for(DataSnapshot spousesnapshot: usersnapshot.child("sibling").getChildren()){
                            tempsiblingArray.add(spousesnapshot.getKey());
                        }

                        ArrayList<String> tempchildrenArray = new ArrayList<String>();
                        for(DataSnapshot spousesnapshot: usersnapshot.child("children").getChildren()){
                            tempchildrenArray.add(spousesnapshot.getKey());
                        }

                        tempCurrent = new User(id,fullname,phone,dateofbirth,description,activated,email,dateofdeath,did);
                        tempCurrent.setEmail(email);
                        tempCurrent.setLat(Lat);
                        tempCurrent.setLong(Long);
                        tempCurrent.setGender(Gender);
                        tempCurrent.setYoutubeid(vidId);
                        tempCurrent.setParent(tempparentArray);
                        tempCurrent.setSpouse(tempspousearray);
                        tempCurrent.setSibling(tempsiblingArray);
                        tempCurrent.setChildren(tempchildrenArray);

                        final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                        oo.setLayoutParams(lp);
                        Glide.with(getApplicationContext())
                                .load(url)
                                .asBitmap()
                                .listener(new RequestListener<String, Bitmap>() {
                                    @Override
                                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {

                                        biti= resource;
                                        tempCurrent.setPhotolink(biti);

                                        db.sync(tempCurrent,TreeLayoutTemp.this);
                                        return false;
                                    }
                                })
                                .into(oo);


                        continue loop1;
                    }

                    loop2:
                    for(String s: main.getSpouse()){
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            String fullname = usersnapshot.child("fullname").getValue(String.class);
                            String dateofbirth = usersnapshot.child("dateofbirth").getValue(String.class);
                            String description = usersnapshot.child("description").getValue(String.class);
                            String phone = usersnapshot.child("phone").getValue(String.class);
                            String vidId = usersnapshot.child("youtubeid").getValue(String.class);
                            String id= usersnapshot.child("id").getValue(String.class);
                            String dateofdeath = usersnapshot.child("dateofdeath").getValue(String.class);
                            String did = usersnapshot.child("did").getValue(String.class);
                            String url = usersnapshot.child("photolink").getValue(String.class);
                            String email = usersnapshot.child("email").getValue(String.class);
                            String gender = usersnapshot.child("gender").getValue(String.class);
                            Gender= gender;
                            String activated = usersnapshot.child("activated").getValue(String.class);
                            String Lat = usersnapshot.child("lat").getValue(String.class);
                            String Long = usersnapshot.child("long").getValue(String.class);

                            String [] tempparentArray = new String[2];
                            tempparentArray[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                            tempparentArray[1] = usersnapshot.child("parent").child("mother").getValue(String.class);

                            ArrayList<String> tempspousearray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("spouse").getChildren()){
                                tempspousearray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempsiblingArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("sibling").getChildren()){
                                tempsiblingArray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempchildrenArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("children").getChildren()){
                                tempchildrenArray.add(spousesnapshot.getKey());
                            }

                            tempCurrent = new User(id,fullname,phone,dateofbirth,description,activated,email,dateofdeath,did);
                            tempCurrent.setEmail(email);
                            tempCurrent.setLat(Lat);
                            tempCurrent.setLong(Long);
                            tempCurrent.setGender(Gender);
                            tempCurrent.setYoutubeid(vidId);
                            tempCurrent.setParent(tempparentArray);
                            tempCurrent.setSpouse(tempspousearray);
                            tempCurrent.setSibling(tempsiblingArray);
                            tempCurrent.setChildren(tempchildrenArray);

                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {

                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            return false;
                                        }
                                    })
                                    .into(oo);

                            continue loop1;
                        }
                    }

                    loop2:
                    for(int j =0; j< main.getSibling().size();j++){
                        String s = main.getSibling().get(j);
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            String fullname = usersnapshot.child("fullname").getValue(String.class);
                            String dateofbirth = usersnapshot.child("dateofbirth").getValue(String.class);
                            String description = usersnapshot.child("description").getValue(String.class);
                            String phone = usersnapshot.child("phone").getValue(String.class);
                            String vidId = usersnapshot.child("youtubeid").getValue(String.class);
                            String id= usersnapshot.child("id").getValue(String.class);
                            String dateofdeath = usersnapshot.child("dateofdeath").getValue(String.class);
                            String did = usersnapshot.child("did").getValue(String.class);
                            String url = usersnapshot.child("photolink").getValue(String.class);
                            String email = usersnapshot.child("email").getValue(String.class);
                            String gender = usersnapshot.child("gender").getValue(String.class);
                            Gender= gender;
                            String activated = usersnapshot.child("activated").getValue(String.class);
                            String Lat = usersnapshot.child("lat").getValue(String.class);
                            String Long = usersnapshot.child("long").getValue(String.class);

                            String [] tempparentArray = new String[2];
                            tempparentArray[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                            tempparentArray[1] = usersnapshot.child("parent").child("mother").getValue(String.class);

                            ArrayList<String> tempspousearray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("spouse").getChildren()){
                                tempspousearray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempsiblingArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("sibling").getChildren()){
                                tempsiblingArray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempchildrenArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("children").getChildren()){
                                tempchildrenArray.add(spousesnapshot.getKey());
                            }

                            tempCurrent = new User(id,fullname,phone,dateofbirth,description,activated,email,dateofdeath,did);
                            tempCurrent.setEmail(email);
                            tempCurrent.setLat(Lat);
                            tempCurrent.setLong(Long);
                            tempCurrent.setGender(Gender);
                            tempCurrent.setYoutubeid(vidId);
                            tempCurrent.setParent(tempparentArray);
                            tempCurrent.setSpouse(tempspousearray);
                            tempCurrent.setSibling(tempsiblingArray);
                            tempCurrent.setChildren(tempchildrenArray);

                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {

                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            return false;
                                        }
                                    })
                                    .into(oo);

                            continue loop1;
                        }
                    }

                    loop2:
                    for(String s: main.getChildren()){
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            String fullname = usersnapshot.child("fullname").getValue(String.class);
                            String dateofbirth = usersnapshot.child("dateofbirth").getValue(String.class);
                            String description = usersnapshot.child("description").getValue(String.class);
                            String phone = usersnapshot.child("phone").getValue(String.class);
                            String vidId = usersnapshot.child("youtubeid").getValue(String.class);
                            String id= usersnapshot.child("id").getValue(String.class);
                            String dateofdeath = usersnapshot.child("dateofdeath").getValue(String.class);
                            String did = usersnapshot.child("did").getValue(String.class);
                            String url = usersnapshot.child("photolink").getValue(String.class);
                            String email = usersnapshot.child("email").getValue(String.class);
                            String gender = usersnapshot.child("gender").getValue(String.class);
                            Gender= gender;
                            String activated = usersnapshot.child("activated").getValue(String.class);
                            String Lat = usersnapshot.child("lat").getValue(String.class);
                            String Long = usersnapshot.child("long").getValue(String.class);

                            String [] tempparentArray = new String[2];
                            tempparentArray[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                            tempparentArray[1] = usersnapshot.child("parent").child("mother").getValue(String.class);

                            ArrayList<String> tempspousearray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("spouse").getChildren()){
                                tempspousearray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempsiblingArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("sibling").getChildren()){
                                tempsiblingArray.add(spousesnapshot.getKey());
                            }

                            ArrayList<String> tempchildrenArray = new ArrayList<String>();
                            for(DataSnapshot spousesnapshot: usersnapshot.child("children").getChildren()){
                                tempchildrenArray.add(spousesnapshot.getKey());
                            }

                            tempCurrent = new User(id,fullname,phone,dateofbirth,description,activated,email,dateofdeath,did);
                            tempCurrent.setEmail(email);
                            tempCurrent.setLat(Lat);
                            tempCurrent.setLong(Long);
                            tempCurrent.setGender(Gender);
                            tempCurrent.setYoutubeid(vidId);
                            tempCurrent.setParent(tempparentArray);
                            tempCurrent.setSpouse(tempspousearray);
                            tempCurrent.setSibling(tempsiblingArray);
                            tempCurrent.setChildren(tempchildrenArray);

                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {

                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            return false;
                                        }
                                    })
                                    .into(oo);


                            continue loop1;
                        }
                    }
                }

            }

            @Override

            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    //here we retrieve data from SQL DataBase to set up the layout
    private void classSetupForSpouseAndParents11(final String main){
        User currenttt = null;
        User [] parents = new User[2];
        int i = 0;
        ArrayList<User> spousess = new ArrayList();
        ArrayList<User> siblingss = new ArrayList();
        ArrayList<User> childrenn = new ArrayList();

        Cursor alldata = db.getalldata();
        int zoborte = alldata.getCount();

        if(alldata!=null&& zoborte!=0) {
            loop1:
            while (alldata.moveToNext()) {
                String idd = alldata.getString(8);

                if (idd != null)
                    if (idd.equals(main)) {
                        String activated = alldata.getString(0);
                        String dateofbirth = alldata.getString(1);
                        String dateofdeath = alldata.getString(2);
                        String description = alldata.getString(3);
                        String did = alldata.getString(4);
                        String email = alldata.getString(5);
                        String fullname = alldata.getString(6);
                        String gender = alldata.getString(7);
                        String id = alldata.getString(8);
                        String lat = alldata.getString(9);
                        String llong = alldata.getString(10);
                        String father = alldata.getString(11).split("/")[0];
                        String mother = alldata.getString(11).split("/")[1];
                        String phone = alldata.getString(12);


                        Gender = gender;

                        String[] parentss = new String[2];
                        parentss[0] = father;
                        parentss[1] = mother;

                        ArrayList<String> siblings = new ArrayList<>();
                        String[] siblingarray = alldata.getString(14).split("/");
                        for (String sibl : siblingarray) {
                            siblings.add(sibl);
                        }

                        ArrayList<String> children = new ArrayList<>();
                        String[] childrenarray = alldata.getString(15).split("/");
                        for (String child : childrenarray) {
                            children.add(child);
                        }

                        String youtubeid = alldata.getString(16);

                        ArrayList<String> spouses = new ArrayList<>();
                        String[] spousearray = alldata.getString(17).split("/");
                        for (String spouse : spousearray) {
                            spouses.add(spouse);
                        }
                        User tempCurrent = new User(id, fullname, phone, dateofbirth, description, activated, email, dateofdeath, did);
                        tempCurrent.setEmail(email);
                        tempCurrent.setLat(lat);
                        tempCurrent.setLong(llong);
                        tempCurrent.setGender(gender);
                        tempCurrent.setYoutubeid(youtubeid);
                        tempCurrent.setParent(parentss);
                        tempCurrent.setSpouse(spouses);
                        tempCurrent.setSibling(siblings);
                        tempCurrent.setChildren(children);

                        byte[] photolink = alldata.getBlob(13);
                        Bitmap bitmap = BitmapFactory.decodeByteArray(photolink, 0, photolink.length);
                        tempCurrent.setPhotolink(bitmap);


                        currenttt = tempCurrent;
                        CurrentUser = tempCurrent;
                        break loop1;
                    }

            }
            alldata= db.getalldata();
            String fafa="",mama="";
            if(currenttt.getParent()[0]==null)
                fafa = "";
            else
                fafa = currenttt.getParent()[0];

            if(currenttt.getParent()[1]==null)
                mama = "";
            else
                mama = currenttt.getParent()[1];

            loop2:
            while (alldata.moveToNext()) {
                if(alldata.getString(8)!=null)
                    if (alldata.getString(8).equals(fafa) || alldata.getString(8).equals(mama)) {
                        String activated = alldata.getString(0);
                        String dateofbirth = alldata.getString(1);
                        String dateofdeath = alldata.getString(2);
                        String description = alldata.getString(3);
                        String did = alldata.getString(4);
                        String email = alldata.getString(5);
                        String fullname = alldata.getString(6);
                        String gender = alldata.getString(7);
                        String id = alldata.getString(8);
                        String lat = alldata.getString(9);
                        String llong = alldata.getString(10);
                        String father = alldata.getString(11).split("/")[0];
                        String mother = alldata.getString(11).split("/")[1];
                        String phone = alldata.getString(12);

                        String[] parentss = new String[2];
                        parentss[0] = father;
                        parentss[1] = mother;

                        ArrayList<String> siblings = new ArrayList<>();
                        String[] siblingarray = alldata.getString(14).split("/");
                        for (String sibl : siblingarray) {
                            siblings.add(sibl);
                        }

                        ArrayList<String> children = new ArrayList<>();
                        String[] childrenarray = alldata.getString(15).split("/");
                        for (String child : childrenarray) {
                            children.add(child);
                        }

                        String youtubeid = alldata.getString(16);

                        ArrayList<String> spouses = new ArrayList<>();
                        String[] spousearray = alldata.getString(17).split("/");
                        for (String spouse : spousearray) {
                            spouses.add(spouse);
                        }
                        User tempCurrent = new User(id, fullname, phone, dateofbirth, description, activated, email, dateofdeath, did);
                        tempCurrent.setEmail(email);
                        tempCurrent.setLat(lat);
                        tempCurrent.setLong(llong);
                        tempCurrent.setGender(gender);
                        tempCurrent.setYoutubeid(youtubeid);
                        tempCurrent.setParent(parentss);
                        tempCurrent.setSpouse(spouses);
                        tempCurrent.setSibling(siblings);
                        tempCurrent.setChildren(children);

                        byte[] photolink = alldata.getBlob(13);
                        Bitmap bitmap = BitmapFactory.decodeByteArray(photolink, 0, photolink.length);
                        tempCurrent.setPhotolink(bitmap);

                        if(gender.equals("male"))
                            parents[0] = tempCurrent;
                        else if(gender.equals("female"))
                            parents[1] = tempCurrent;

                        continue loop2;
                    }

                for (String s : currenttt.getSpouse()) {
                    if (s.equals(alldata.getString(8))) {
                        String activated = alldata.getString(0);
                        String dateofbirth = alldata.getString(1);
                        String dateofdeath = alldata.getString(2);
                        String description = alldata.getString(3);
                        String did = alldata.getString(4);
                        String email = alldata.getString(5);
                        String fullname = alldata.getString(6);
                        String gender = alldata.getString(7);
                        String id = alldata.getString(8);
                        String lat = alldata.getString(9);
                        String llong = alldata.getString(10);
                        String father = alldata.getString(11).split("/")[0];
                        String mother = alldata.getString(11).split("/")[1];
                        String phone = alldata.getString(12);

                        String[] parentss = new String[2];
                        parentss[0] = father;
                        parentss[1] = mother;

                        ArrayList<String> siblings = new ArrayList<>();
                        String[] siblingarray = alldata.getString(14).split("/");
                        for (String sibl : siblingarray) {
                            siblings.add(sibl);
                        }

                        ArrayList<String> children = new ArrayList<>();
                        String[] childrenarray = alldata.getString(15).split("/");
                        for (String child : childrenarray) {
                            children.add(child);
                        }

                        String youtubeid = alldata.getString(16);

                        ArrayList<String> spouses = new ArrayList<>();
                        String[] spousearray = alldata.getString(17).split("/");
                        for (String spouse : spousearray) {
                            spouses.add(spouse);
                        }
                        User tempCurrent = new User(id, fullname, phone, dateofbirth, description, activated, email, dateofdeath, did);
                        tempCurrent.setEmail(email);
                        tempCurrent.setLat(lat);
                        tempCurrent.setLong(llong);
                        tempCurrent.setGender(gender);
                        tempCurrent.setYoutubeid(youtubeid);
                        tempCurrent.setParent(parentss);
                        tempCurrent.setSpouse(spouses);
                        tempCurrent.setSibling(siblings);
                        tempCurrent.setChildren(children);

                        byte[] photolink = alldata.getBlob(13);
                        Bitmap bitmap = BitmapFactory.decodeByteArray(photolink, 0, photolink.length);
                        tempCurrent.setPhotolink(bitmap);

                        spousess.add(tempCurrent);
                        continue loop2;
                    }
                }

                for (String s : currenttt.getSibling()) {
                    if (s.equals(alldata.getString(8))) {
                        String activated = alldata.getString(0);
                        String dateofbirth = alldata.getString(1);
                        String dateofdeath = alldata.getString(2);
                        String description = alldata.getString(3);
                        String did = alldata.getString(4);
                        String email = alldata.getString(5);
                        String fullname = alldata.getString(6);
                        String gender = alldata.getString(7);
                        String id = alldata.getString(8);
                        String lat = alldata.getString(9);
                        String llong = alldata.getString(10);
                        String father = alldata.getString(11).split("/")[0];
                        String mother = alldata.getString(11).split("/")[1];
                        String phone = alldata.getString(12);

                        String[] parentss = new String[2];
                        parentss[0] = father;
                        parentss[1] = mother;

                        ArrayList<String> siblings = new ArrayList<>();
                        String[] siblingarray = alldata.getString(14).split("/");
                        for (String sibl : siblingarray) {
                            siblings.add(sibl);
                        }

                        ArrayList<String> children = new ArrayList<>();
                        String[] childrenarray = alldata.getString(15).split("/");
                        for (String child : childrenarray) {
                            children.add(child);
                        }

                        String youtubeid = alldata.getString(16);

                        ArrayList<String> spouses = new ArrayList<>();
                        String[] spousearray = alldata.getString(17).split("/");
                        for (String spouse : spousearray) {
                            spouses.add(spouse);
                        }
                        User tempCurrent = new User(id, fullname, phone, dateofbirth, description, activated, email, dateofdeath, did);
                        tempCurrent.setEmail(email);
                        tempCurrent.setLat(lat);
                        tempCurrent.setLong(llong);
                        tempCurrent.setGender(gender);
                        tempCurrent.setYoutubeid(youtubeid);
                        tempCurrent.setParent(parentss);
                        tempCurrent.setSpouse(spouses);
                        tempCurrent.setSibling(siblings);
                        tempCurrent.setChildren(children);

                        byte[] photolink = alldata.getBlob(13);
                        Bitmap bitmap = BitmapFactory.decodeByteArray(photolink, 0, photolink.length);
                        tempCurrent.setPhotolink(bitmap);

                        siblingss.add(tempCurrent);
                        continue loop2;
                    }
                }

                for (String s : currenttt.getChildren()) {
                    if (s.equals(alldata.getString(8))) {
                        String activated = alldata.getString(0);
                        String dateofbirth = alldata.getString(1);
                        String dateofdeath = alldata.getString(2);
                        String description = alldata.getString(3);
                        String did = alldata.getString(4);
                        String email = alldata.getString(5);
                        String fullname = alldata.getString(6);
                        String gender = alldata.getString(7);
                        String id = alldata.getString(8);
                        String lat = alldata.getString(9);
                        String llong = alldata.getString(10);
                        String father = alldata.getString(11).split("/")[0];
                        String mother = alldata.getString(11).split("/")[1];
                        String phone = alldata.getString(12);

                        String[] parentss = new String[2];
                        parentss[0] = father;
                        parentss[1] = mother;

                        ArrayList<String> siblings = new ArrayList<>();
                        String[] siblingarray = alldata.getString(14).split("/");
                        for (String sibl : siblingarray) {
                            siblings.add(sibl);
                        }

                        ArrayList<String> children = new ArrayList<>();
                        String[] childrenarray = alldata.getString(15).split("/");
                        for (String child : childrenarray) {
                            children.add(child);
                        }

                        String youtubeid = alldata.getString(16);

                        ArrayList<String> spouses = new ArrayList<>();
                        String[] spousearray = alldata.getString(17).split("/");
                        for (String spouse : spousearray) {
                            spouses.add(spouse);
                        }
                        User tempCurrent = new User(id, fullname, phone, dateofbirth, description, activated, email, dateofdeath, did);
                        tempCurrent.setEmail(email);
                        tempCurrent.setLat(lat);
                        tempCurrent.setLong(llong);
                        tempCurrent.setGender(gender);
                        tempCurrent.setYoutubeid(youtubeid);
                        tempCurrent.setParent(parentss);
                        tempCurrent.setSpouse(spouses);
                        tempCurrent.setSibling(siblings);
                        tempCurrent.setChildren(children);

                        byte[] photolink = alldata.getBlob(13);
                        Bitmap bitmap = BitmapFactory.decodeByteArray(photolink, 0, photolink.length);
                        tempCurrent.setPhotolink(bitmap);

                        childrenn.add(tempCurrent);
                        continue loop2;
                    }
                }
            }

            popo123[0]= parents[0];
            popo123[1]= parents[1];

            setUpButtons(parents[0],parents[1],currenttt,spousess,childrenn,siblingss);
        }
    }
    private void classSetupForSpouseAndParents111(final String main){
        User currenttt = null;
        User [] parents = new User[2];
        int i = 0;
        ArrayList<User> spousess = new ArrayList();
        ArrayList<User> siblingss = new ArrayList();
        ArrayList<User> childrenn = new ArrayList();


        Cursor alldata = db.getalldata();
        int zoborte = alldata.getCount();

        if(alldata!=null&& zoborte!=0) {
            loop1:
            while (alldata.moveToNext()) {
                String idd = alldata.getString(8);

                if (idd != null)
                    if (idd.equals(main)) {
                        String activated = alldata.getString(0);
                        String dateofbirth = alldata.getString(1);
                        String dateofdeath = alldata.getString(2);
                        String description = alldata.getString(3);
                        String did = alldata.getString(4);
                        String email = alldata.getString(5);
                        String fullname = alldata.getString(6);
                        String gender = alldata.getString(7);
                        String id = alldata.getString(8);
                        String lat = alldata.getString(9);
                        String llong = alldata.getString(10);
                        String father = alldata.getString(11).split("/")[0];
                        String mother = alldata.getString(11).split("/")[1];
                        String phone = alldata.getString(12);


                        Gender = gender;

                        String[] parentss = new String[2];
                        parentss[0] = father;
                        parentss[1] = mother;

                        ArrayList<String> siblings = new ArrayList<>();
                        String[] siblingarray = alldata.getString(14).split("/");
                        for (String sibl : siblingarray) {
                            siblings.add(sibl);
                        }

                        ArrayList<String> children = new ArrayList<>();
                        String[] childrenarray = alldata.getString(15).split("/");
                        for (String child : childrenarray) {
                            children.add(child);
                        }

                        String youtubeid = alldata.getString(16);

                        ArrayList<String> spouses = new ArrayList<>();
                        String[] spousearray = alldata.getString(17).split("/");
                        for (String spouse : spousearray) {
                            spouses.add(spouse);
                        }
                        User tempCurrent = new User(id, fullname, phone, dateofbirth, description, activated, email, dateofdeath, did);
                        tempCurrent.setEmail(email);
                        tempCurrent.setLat(lat);
                        tempCurrent.setLong(llong);
                        tempCurrent.setGender(gender);
                        tempCurrent.setYoutubeid(youtubeid);
                        tempCurrent.setParent(parentss);
                        tempCurrent.setSpouse(spouses);
                        tempCurrent.setSibling(siblings);
                        tempCurrent.setChildren(children);

                        byte[] photolink = alldata.getBlob(13);
                        Bitmap bitmap = BitmapFactory.decodeByteArray(photolink, 0, photolink.length);
                        tempCurrent.setPhotolink(bitmap);


                        currenttt = tempCurrent;
                        CurrentUser = tempCurrent;
                        break loop1;
                    }

            }
            alldata= db.getalldata();
            String fafa="",mama="";
            if(currenttt.getParent()[0]==null)
                fafa = "";
            else
                fafa = currenttt.getParent()[0];

            if(currenttt.getParent()[1]==null)
                mama = "";
            else
                mama = currenttt.getParent()[1];

            loop2:
            while (alldata.moveToNext()) {
                if(alldata.getString(8)!=null)
                    if (alldata.getString(8).equals(fafa) || alldata.getString(8).equals(mama)) {
                        String activated = alldata.getString(0);
                        String dateofbirth = alldata.getString(1);
                        String dateofdeath = alldata.getString(2);
                        String description = alldata.getString(3);
                        String did = alldata.getString(4);
                        String email = alldata.getString(5);
                        String fullname = alldata.getString(6);
                        String gender = alldata.getString(7);
                        String id = alldata.getString(8);
                        String lat = alldata.getString(9);
                        String llong = alldata.getString(10);
                        String father = alldata.getString(11).split("/")[0];
                        String mother = alldata.getString(11).split("/")[1];
                        String phone = alldata.getString(12);

                        String[] parentss = new String[2];
                        parentss[0] = father;
                        parentss[1] = mother;

                        ArrayList<String> siblings = new ArrayList<>();
                        String[] siblingarray = alldata.getString(14).split("/");
                        for (String sibl : siblingarray) {
                            siblings.add(sibl);
                        }

                        ArrayList<String> children = new ArrayList<>();
                        String[] childrenarray = alldata.getString(15).split("/");
                        for (String child : childrenarray) {
                            children.add(child);
                        }

                        String youtubeid = alldata.getString(16);

                        ArrayList<String> spouses = new ArrayList<>();
                        String[] spousearray = alldata.getString(17).split("/");
                        for (String spouse : spousearray) {
                            spouses.add(spouse);
                        }
                        User tempCurrent = new User(id, fullname, phone, dateofbirth, description, activated, email, dateofdeath, did);
                        tempCurrent.setEmail(email);
                        tempCurrent.setLat(lat);
                        tempCurrent.setLong(llong);
                        tempCurrent.setGender(gender);
                        tempCurrent.setYoutubeid(youtubeid);
                        tempCurrent.setParent(parentss);
                        tempCurrent.setSpouse(spouses);
                        tempCurrent.setSibling(siblings);
                        tempCurrent.setChildren(children);

                        byte[] photolink = alldata.getBlob(13);
                        Bitmap bitmap = BitmapFactory.decodeByteArray(photolink, 0, photolink.length);
                        tempCurrent.setPhotolink(bitmap);

                        if(gender.equals("male"))
                            parents[0] = tempCurrent;
                        else if(gender.equals("female"))
                            parents[1] = tempCurrent;

                        continue loop2;
                    }

                for (String s : currenttt.getSpouse()) {
                    if (s.equals(alldata.getString(8))) {
                        String activated = alldata.getString(0);
                        String dateofbirth = alldata.getString(1);
                        String dateofdeath = alldata.getString(2);
                        String description = alldata.getString(3);
                        String did = alldata.getString(4);
                        String email = alldata.getString(5);
                        String fullname = alldata.getString(6);
                        String gender = alldata.getString(7);
                        String id = alldata.getString(8);
                        String lat = alldata.getString(9);
                        String llong = alldata.getString(10);
                        String father = alldata.getString(11).split("/")[0];
                        String mother = alldata.getString(11).split("/")[1];
                        String phone = alldata.getString(12);

                        String[] parentss = new String[2];
                        parentss[0] = father;
                        parentss[1] = mother;

                        ArrayList<String> siblings = new ArrayList<>();
                        String[] siblingarray = alldata.getString(14).split("/");
                        for (String sibl : siblingarray) {
                            siblings.add(sibl);
                        }

                        ArrayList<String> children = new ArrayList<>();
                        String[] childrenarray = alldata.getString(15).split("/");
                        for (String child : childrenarray) {
                            children.add(child);
                        }

                        String youtubeid = alldata.getString(16);

                        ArrayList<String> spouses = new ArrayList<>();
                        String[] spousearray = alldata.getString(17).split("/");
                        for (String spouse : spousearray) {
                            spouses.add(spouse);
                        }
                        User tempCurrent = new User(id, fullname, phone, dateofbirth, description, activated, email, dateofdeath, did);
                        tempCurrent.setEmail(email);
                        tempCurrent.setLat(lat);
                        tempCurrent.setLong(llong);
                        tempCurrent.setGender(gender);
                        tempCurrent.setYoutubeid(youtubeid);
                        tempCurrent.setParent(parentss);
                        tempCurrent.setSpouse(spouses);
                        tempCurrent.setSibling(siblings);
                        tempCurrent.setChildren(children);

                        byte[] photolink = alldata.getBlob(13);
                        Bitmap bitmap = BitmapFactory.decodeByteArray(photolink, 0, photolink.length);
                        tempCurrent.setPhotolink(bitmap);

                        spousess.add(tempCurrent);
                        continue loop2;
                    }
                }

                for (String s : currenttt.getSibling()) {
                    if (s.equals(alldata.getString(8))) {
                        String activated = alldata.getString(0);
                        String dateofbirth = alldata.getString(1);
                        String dateofdeath = alldata.getString(2);
                        String description = alldata.getString(3);
                        String did = alldata.getString(4);
                        String email = alldata.getString(5);
                        String fullname = alldata.getString(6);
                        String gender = alldata.getString(7);
                        String id = alldata.getString(8);
                        String lat = alldata.getString(9);
                        String llong = alldata.getString(10);
                        String father = alldata.getString(11).split("/")[0];
                        String mother = alldata.getString(11).split("/")[1];
                        String phone = alldata.getString(12);

                        String[] parentss = new String[2];
                        parentss[0] = father;
                        parentss[1] = mother;

                        ArrayList<String> siblings = new ArrayList<>();
                        String[] siblingarray = alldata.getString(14).split("/");
                        for (String sibl : siblingarray) {
                            siblings.add(sibl);
                        }

                        ArrayList<String> children = new ArrayList<>();
                        String[] childrenarray = alldata.getString(15).split("/");
                        for (String child : childrenarray) {
                            children.add(child);
                        }

                        String youtubeid = alldata.getString(16);

                        ArrayList<String> spouses = new ArrayList<>();
                        String[] spousearray = alldata.getString(17).split("/");
                        for (String spouse : spousearray) {
                            spouses.add(spouse);
                        }
                        User tempCurrent = new User(id, fullname, phone, dateofbirth, description, activated, email, dateofdeath, did);
                        tempCurrent.setEmail(email);
                        tempCurrent.setLat(lat);
                        tempCurrent.setLong(llong);
                        tempCurrent.setGender(gender);
                        tempCurrent.setYoutubeid(youtubeid);
                        tempCurrent.setParent(parentss);
                        tempCurrent.setSpouse(spouses);
                        tempCurrent.setSibling(siblings);
                        tempCurrent.setChildren(children);

                        byte[] photolink = alldata.getBlob(13);
                        Bitmap bitmap = BitmapFactory.decodeByteArray(photolink, 0, photolink.length);
                        tempCurrent.setPhotolink(bitmap);

                        siblingss.add(tempCurrent);
                        continue loop2;
                    }
                }

                for (String s : currenttt.getChildren()) {
                    if (s.equals(alldata.getString(8))) {
                        String activated = alldata.getString(0);
                        String dateofbirth = alldata.getString(1);
                        String dateofdeath = alldata.getString(2);
                        String description = alldata.getString(3);
                        String did = alldata.getString(4);
                        String email = alldata.getString(5);
                        String fullname = alldata.getString(6);
                        String gender = alldata.getString(7);
                        String id = alldata.getString(8);
                        String lat = alldata.getString(9);
                        String llong = alldata.getString(10);
                        String father = alldata.getString(11).split("/")[0];
                        String mother = alldata.getString(11).split("/")[1];
                        String phone = alldata.getString(12);

                        String[] parentss = new String[2];
                        parentss[0] = father;
                        parentss[1] = mother;

                        ArrayList<String> siblings = new ArrayList<>();
                        String[] siblingarray = alldata.getString(14).split("/");
                        for (String sibl : siblingarray) {
                            siblings.add(sibl);
                        }

                        ArrayList<String> children = new ArrayList<>();
                        String[] childrenarray = alldata.getString(15).split("/");
                        for (String child : childrenarray) {
                            children.add(child);
                        }

                        String youtubeid = alldata.getString(16);

                        ArrayList<String> spouses = new ArrayList<>();
                        String[] spousearray = alldata.getString(17).split("/");
                        for (String spouse : spousearray) {
                            spouses.add(spouse);
                        }
                        User tempCurrent = new User(id, fullname, phone, dateofbirth, description, activated, email, dateofdeath, did);
                        tempCurrent.setEmail(email);
                        tempCurrent.setLat(lat);
                        tempCurrent.setLong(llong);
                        tempCurrent.setGender(gender);
                        tempCurrent.setYoutubeid(youtubeid);
                        tempCurrent.setParent(parentss);
                        tempCurrent.setSpouse(spouses);
                        tempCurrent.setSibling(siblings);
                        tempCurrent.setChildren(children);

                        byte[] photolink = alldata.getBlob(13);
                        Bitmap bitmap = BitmapFactory.decodeByteArray(photolink, 0, photolink.length);
                        tempCurrent.setPhotolink(bitmap);

                        childrenn.add(tempCurrent);
                        continue loop2;
                    }
                }
            }

            popo123[0]= parents[0];
            popo123[1]= parents[1];

            setUpButtons(parents[0],parents[1],currenttt,spousess,childrenn,siblingss);
        }
    }



    private void setUpButtons(final User Father, final User Mother, final User currentUser, final ArrayList<User> Spouse, ArrayList<User> children, ArrayList<User> siblings){
        if(progressBar.isShowing()) {
            if (progressBar != null)
                progressBar.dismiss();
        }

        spouses= Spouse;
        spousecount = 0;
        currentspouse = getSpouse();

        currentUserchildren = children;

        //Checks if the user is dead or not/ heka bkol el buttons bf7s
        if(currentUser.getDateofdeath().equals(""))
            currentuser.setBorderColor(Color.parseColor("#467777"));
        else{
            currentuser.setBorderColor(Color.parseColor("#160d44"));
        }

        if(Gender.equals("male")){
            currentuser.setImageResource(R.drawable.male);
        }
        else
            currentuser.setImageResource(R.drawable.female);

        String name = currentUser.getFullname()+" ";

        Bitmap bitmap = currentUser.getPhotolink();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        Glide.with(TreeLayoutTemp.this)
                .load(stream.toByteArray())
                .asBitmap()
                .into(currentuser);

        currentusertext.setText(name.split(" ")[0]);
        currentuser.setOnClickListener(null);
        currentuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(TreeLayoutTemp.this,ProfileView.class);
                Bitmap kokos = currentUser.getPhotolink();
                ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
                kokos.compress(Bitmap.CompressFormat.PNG, 100, stream1);

                i.putExtra("phototo",stream1.toByteArray());

                currentUser.setPhotolink(null);
                i.putExtra("user", (Serializable) currentUser);
                i.putExtra("currentUser",currentUser.getId());
                i.putExtra("who","current");
                i.putExtra("mainuser",userkey);
                TreeLayoutTemp.this.startActivityForResult(i,1);
                currentUser.setPhotolink(kokos);

            }
        });

        if(Father!=null) {
            if (Father.getDateofdeath().equals(""))
                father.setBorderColor(Color.parseColor("#467777"));
            else {
                father.setBorderColor(Color.parseColor("#160d44"));

            }
            bitmap = Father.getPhotolink();

            stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Glide.with(TreeLayoutTemp.this)
                    .load(stream.toByteArray())
                    .asBitmap()
                    .into(father);

        }
        else{
        }


        father.setOnClickListener(null);
        fathertext.setText(R.string.str_father);
        father.setOnLongClickListener(null);
        if(!ifCurrent&&Father== null){}
        else {
            try {
                String name1 = Father.getFullname() + " ";
                fathertext.setText(name1.split(" ")[0]);
            }
            catch (Exception e){}
            father.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(TreeLayoutTemp.this, ProfileView.class);
                    if(Father!=null) {
                        Bitmap kokos = Father.getPhotolink();
                        ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
                        kokos.compress(Bitmap.CompressFormat.PNG, 100, stream1);


                        i.putExtra("phototo", stream1.toByteArray());

                        Father.setPhotolink(null);
                        i.putExtra("user", (Serializable) Father);
                        i.putExtra("currentUser", userkey);
                        i.putExtra("who", "father");
                        i.putExtra("mainuser", currentUser.getId());

                        TreeLayoutTemp.this.startActivityForResult(i, 1);
                        Father.setPhotolink(kokos);
                    }
                    else{
                        if(!checkinternetconnectivity()){Toast t =Toast.makeText(TreeLayoutTemp.this,R.string.str_error_nointernetconnection,Toast.LENGTH_SHORT);
                            t.setGravity(Gravity.CENTER,0,0);
                            t.show();
                        }
                        {
                            i.putExtra("phototo", new byte[0]);

                            i.putExtra("user", (Serializable) Father);
                            i.putExtra("currentUser", userkey);
                            i.putExtra("who", "father");
                            i.putExtra("mainuser", currentUser.getId());

                            TreeLayoutTemp.this.startActivityForResult(i, 1);
                        }
                    }
                }
            });

            father.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    swipeleftafterlongpressfix = false;
                    classSetUp1(Father.getId());
                    return true;
                }
            });
        }


        if(Mother!=null) {
            bitmap = Mother.getPhotolink();

            stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Glide.with(TreeLayoutTemp.this)
                    .load(stream.toByteArray())
                    .asBitmap()
                    .into(mother);

            if (Mother.getDateofdeath().equals(""))
                mother.setBorderColor(Color.parseColor("#467777"));
            else {
                mother.setBorderColor(Color.parseColor("#160d44"));

            }
        }
        else{
        }

        mother.setOnClickListener(null);
        mothertext.setText(R.string.str_mother);
        mother.setOnLongClickListener(null);
        if(!ifCurrent&&Mother== null){}
        else {
            try {
                String name2 = Mother.getFullname() + " ";
                mothertext.setText(name2.split(" ")[0]);
            }
            catch (Exception e){}
            mother.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(TreeLayoutTemp.this, ProfileView.class);
                    if(Mother!=null) {
                        Bitmap kokos = Mother.getPhotolink();
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        kokos.compress(Bitmap.CompressFormat.PNG, 100, stream);


                        i.putExtra("phototo", stream.toByteArray());

                        Mother.setPhotolink(null);

                        i.putExtra("user", (Serializable) Mother);
                        i.putExtra("currentUser", userkey);
                        i.putExtra("who", "mother");
                        i.putExtra("mainuser", currentUser.getId());

                        TreeLayoutTemp.this.startActivityForResult(i, 1);
                        Mother.setPhotolink(kokos);
                    }
                    else{

                        if(!checkinternetconnectivity()){Toast t =Toast.makeText(TreeLayoutTemp.this,R.string.str_error_nointernetconnection,Toast.LENGTH_SHORT);
                            t.setGravity(Gravity.CENTER,0,0);
                            t.show();
                        }
                        else {
                            i.putExtra("phototo", new byte[0]);

                            i.putExtra("user", (Serializable) Mother);
                            i.putExtra("currentUser", userkey);
                            i.putExtra("who", "mother");
                            i.putExtra("mainuser", currentUser.getId());

                            TreeLayoutTemp.this.startActivityForResult(i, 1);
                        }
                    }
                }
            });
            mother.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    swipeleftafterlongpressfix = false;
                    classSetUp1(Mother.getId());
                    return true;
                }
            });
        }



        spousetext.setText(R.string.str_spouse);
        if(currentspouse!=null) {
            bitmap = currentspouse.getPhotolink();

            stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Glide.with(TreeLayoutTemp.this)
                    .load(stream.toByteArray())
                    .asBitmap()
                    .into(spouse);
            try {
                String name3 = currentspouse.getFullname() + " ";
                spousetext.setText(name3.split(" ")[0]);
            }
            catch (Exception e){}
            if (currentspouse.getDateofdeath().equals(""))
                spouse.setBorderColor(Color.parseColor("#467777"));
            else {
                spouse.setBorderColor(Color.parseColor("#160d44"));

            }
        }
        else{
        }

        spouse.setOnClickListener(null);
        spouse.setOnLongClickListener(null);
        if(!ifCurrent && currentspouse== null){

        }
        else {
            spouse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(TreeLayoutTemp.this, ProfileView.class);

                    if(currentspouse!=null) {
                        Bitmap kokos = currentspouse.getPhotolink();
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        kokos.compress(Bitmap.CompressFormat.PNG, 100, stream);


                        i.putExtra("phototo", stream.toByteArray());

                        currentspouse.setPhotolink(null);

                        i.putExtra("user", (Serializable) currentspouse);
                        i.putExtra("currentUser", userkey);
                        i.putExtra("who", "spouse");
                        i.putExtra("currentGender", Gender);
                        i.putExtra("mainuser", currentUser.getId());

                        TreeLayoutTemp.this.startActivityForResult(i, 1);
                        currentspouse.setPhotolink(kokos);
                    }
                    else{

                        if(!checkinternetconnectivity()){Toast t =Toast.makeText(TreeLayoutTemp.this,R.string.str_error_nointernetconnection,Toast.LENGTH_SHORT);
                            t.setGravity(Gravity.CENTER,0,0);
                            t.show();
                        }

                        else {
                            i.putExtra("phototo", new byte[1]);

                            i.putExtra("user", (Serializable) currentspouse);
                            i.putExtra("currentUser", userkey);
                            i.putExtra("who", "spouse");
                            i.putExtra("currentGender", currentUser.getGender());
                            i.putExtra("mainuser", currentUser.getId());

                            TreeLayoutTemp.this.startActivityForResult(i, 1);
                        }

                    }

                }
            });
            try {
                String haha = currentspouse.getId();
                spouse.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        swipeleftafterlongpressfix = false;
                        classSetUp1(currentspouse.getId());
                        return true;
                    }
                });
            }
            catch (Exception e){

            }


        }


        setSiblingLayout(siblings,currentUser);

        setChildrenLayout(children,currentUser);



    }


    //-----Gets first spouse from the global spouse array #spouses#------
    private User getSpouse() {
        User temp = null;
        if(spouses.size()>0) {
            temp = spouses.get(0);

        }
        else
            temp = null;
        return temp;
    }
    //-----Gets first spouse from the global spouse array #spouses#------


    private void setChildrenLayout(ArrayList<User> children, final User currentUser) {
        childrenLayout.removeAllViews();
        TextView newtext = new TextView(TreeLayoutTemp.this);
        newtext.setText(getResources().getString(R.string.str_children));
        newtext.setTextColor(Color.parseColor("#000000"));
        newtext.setTextSize(20);
        childrenLayout.addView(newtext);
        try {
            if (!children.isEmpty()&&children.size()>0) {
                childrenLayout.removeAllViews();
                for (final User user : children) {
//                    if(currentspouse!=null)
//                    if(currentspouse.getId()!=null)
                    if ((user.getParent()[0].equals(currentspouse.getId()) && user.getParent()[1].equals(CurrentUser.getId()) )|| (user.getParent()[1].equals(currentspouse.getId()) && user.getParent()[0].equals(CurrentUser.getId()))) {
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT,1.0f);
                        LinearLayout lolo = new LinearLayout(this);
                        lolo.setOrientation(LinearLayout.VERTICAL);
                        lolo.setLayoutParams(lp);

                        lp = new LinearLayout.LayoutParams(220,220,10f);
                        CircleImageView siblingtemp = new CircleImageView(this);
                        siblingtemp.setBorderWidth(10);
                        siblingtemp.setLayoutParams(lp);
                        lolo.addView(siblingtemp);

                        Bitmap bitmap = user.getPhotolink();

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        Glide.with(TreeLayoutTemp.this)
                                .load(stream.toByteArray())
                                .asBitmap()
                                .into(siblingtemp);


                        lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f);
                        TextView siblingtemptext = new TextView(this);
                        lp.gravity= Gravity.CENTER;
                        siblingtemptext.setTextColor(Color.parseColor("#000000"));
                        siblingtemptext.setLayoutParams(lp);

                        lolo.addView(siblingtemptext);

                        user.setFullname(user.getFullname() + " ");
                        siblingtemptext.setText(user.getFullname().split(" ")[0]);
                        if (user.getDateofdeath().equals(""))
                            siblingtemp.setBorderColor(Color.parseColor("#467777"));
                        else {
                            siblingtemp.setBorderColor(Color.parseColor("#160d44"));

                        }
                        siblingtemp.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(TreeLayoutTemp.this, ProfileView.class);
                                Bitmap kokos = user.getPhotolink();
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                kokos.compress(Bitmap.CompressFormat.PNG, 100, stream);


                                i.putExtra("phototo",stream.toByteArray());

                                user.setPhotolink(null);
                                i.putExtra("user", (Serializable) user);
                                i.putExtra("currentUser", userkey);
                                i.putExtra("who", "children");
                                i.putExtra("currentGender", user.getGender());
                                i.putExtra("mainuser", currentUser.getId());
                                i.putExtra("currentspouse", currentspouse.getId());

                                TreeLayoutTemp.this.startActivityForResult(i, 1);
                                user.setPhotolink(kokos);
                            }
                        });
                        siblingtemp.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                swipeleftafterlongpressfix = false;
                                classSetUp1(user.getId());
                                return true;
                            }
                        });
                        siblingtemptext.setTextColor(Color.parseColor("#000000"));
                        childrenLayout.addView(lolo);
                    }
                }
            }
        }
        catch (Exception e){
            Toast.makeText(this,R.string.str_error_pleaseaddotherparenttoview,Toast.LENGTH_LONG).show();
        }
    }

    private void setSiblingLayout(ArrayList<User> siblings, final User currentUser) {
        siblingsLayout.removeAllViews();
        TextView newtext = new TextView(TreeLayoutTemp.this);
        newtext.setText(getResources().getString(R.string.str_siblings));
        newtext.setTextColor(Color.parseColor("#000000"));
        newtext.setTextSize(20);
        siblingsLayout.addView(newtext);
        if(siblings!=null&&siblings.size()>0) {
            siblingsLayout.removeAllViews();
            User user = null;
            for (int i = 0 ; i<siblings.size();i++) {
                user = siblings.get(i);
//                if(user.get)


                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f);
                LinearLayout lolo = new LinearLayout(this);
                lolo.setOrientation(LinearLayout.VERTICAL);
                lolo.setLayoutParams(lp);

                lp = new LinearLayout.LayoutParams(170,170,11f);
                final CircleImageView siblingtemp = new CircleImageView(this);
                siblingtemp.setBorderWidth(10);
                siblingtemp.setLayoutParams(lp);
                lolo.addView(siblingtemp);

                Bitmap bitmap = user.getPhotolink();

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                Glide.with(TreeLayoutTemp.this)
                        .load(stream.toByteArray())
                        .asBitmap()
                        .into(siblingtemp);

                lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f);
                TextView siblingtemptext = new TextView(this);
                lp.gravity= Gravity.CENTER;
                siblingtemptext.setTextColor(Color.parseColor("#000000"));
                siblingtemptext.setLayoutParams(lp);

                lolo.addView(siblingtemptext);

                user.setFullname(user.getFullname() + " ");
                siblingtemptext.setText(user.getFullname().split(" ")[0]);
                if (user.getDateofdeath().equals(""))
                    siblingtemp.setBorderColor(Color.parseColor("#467777"));
                else {
                    siblingtemp.setBorderColor(Color.parseColor("#160d44"));

                }
                final User finalUser = user;
                siblingtemp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(TreeLayoutTemp.this, ProfileView.class);
                        Bitmap kokos = finalUser.getPhotolink();
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        kokos.compress(Bitmap.CompressFormat.PNG, 100, stream);


                        i.putExtra("phototo",stream.toByteArray());

                        finalUser.setPhotolink(null);
                        i.putExtra("user", (Serializable) finalUser);
                        i.putExtra("currentUser", userkey);
                        i.putExtra("who", "sibling");
                        i.putExtra("currentGender", finalUser.getGender());
                        i.putExtra("mainuser",currentUser.getId());

                        TreeLayoutTemp.this.startActivityForResult(i, 1);
                        finalUser.setPhotolink(kokos);
                    }
                });
                siblingtemp.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        swipeleftafterlongpressfix = false;
                        classSetUp1(finalUser.getId());
                        return true;
                    }
                });
                siblingtemptext.setTextColor(Color.parseColor("#000000"));
                siblingsLayout.addView(lolo);
            }
        }

    }


    //---When we change language this is the code that take back what language we chose------------------
    //---also when we close an opened profile it enters this code also and the classSetUp is activated---
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            classSetUp1(userkey);
        }
        if(resultCode == RESULT_FIRST_USER){
            String lang = data.getExtras().getString(language);
            editor.putString(language,lang);
            editor.commit();
            changelang(lang);
            setupp();
        }
    }
    //---When we change language this is the code that take back what language we chose------------------
    //---also when we close an opened profile it enters this code also and the classSetUp is activated---





    //----------------On swipe switch to next/previous spouse------------------
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x2 - x1;
                if (Math.abs(deltaX) > MIN_DISTANCE)
                {
                    if(!swipeleftafterlongpressfix){
                        DisplayMetrics dm = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(dm);
                        x1 = dm.widthPixels;
                        swipeleftafterlongpressfix = true;
                    }
                    //left2right
                    if(x2>x1)
                    {

                        childrenLayout.removeAllViews();
                        boolean sex;
                        if(spousecount == spouses.size()-1){
                            spousecount++;
                            sex = true;
                        }
                        else
                            sex = false;
                        if(sex){
                            currentspouse = null;
                            if(Gender!=null)
                                if(Gender.equals("male"))
                                    spouse.setImageResource(R.drawable.female);
                                else
                                    spouse.setImageResource(R.drawable.male);

                            spouse.setBorderColor(Color.parseColor("#FFFFFF"));

                            spousetext.setText(R.string.str_spouse);

                            if(userkey.equals(CurrentUser.getId())) {
                                spouse.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent i = new Intent(TreeLayoutTemp.this, ProfileView.class);
                                        if(currentspouse!=null) {
                                            Bitmap kokos = currentspouse.getPhotolink();
                                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                            kokos.compress(Bitmap.CompressFormat.PNG, 100, stream);


                                            i.putExtra("phototo", stream.toByteArray());

                                            currentspouse.setPhotolink(null);
                                            i.putExtra("user", (Serializable) currentspouse);
                                            i.putExtra("currentUser", userkey);
                                            i.putExtra("who", "spouse");
                                            i.putExtra("currentGender", Gender);
                                            i.putExtra("mainuser", CurrentUser.getId());

                                            TreeLayoutTemp.this.startActivityForResult(i, 1);
                                            currentspouse.setPhotolink(kokos);
                                        }
                                        else{
                                            if(!checkinternetconnectivity()){Toast t =Toast.makeText(TreeLayoutTemp.this,R.string.str_error_nointernetconnection,Toast.LENGTH_SHORT);
                                                t.setGravity(Gravity.CENTER,0,0);
                                                t.show();
                                            }
                                            else {
                                                i.putExtra("phototo", new byte[0]);

                                                i.putExtra("user", (Serializable) currentspouse);
                                                i.putExtra("currentUser", userkey);
                                                i.putExtra("who", "spouse");
                                                i.putExtra("currentGender", CurrentUser.getGender());
                                                i.putExtra("mainuser", CurrentUser.getId());

                                                TreeLayoutTemp.this.startActivityForResult(i, 1);
                                            }
                                        }

                                    }
                                });
                                try {
                                    String haha = currentspouse.getId();
                                    spouse.setOnLongClickListener(new View.OnLongClickListener() {
                                        @Override
                                        public boolean onLongClick(View v) {
                                            classSetUp1(currentspouse.getId());
                                            return true;
                                        }
                                    });
                                } catch (Exception e) {

                                }
                            }
                            else{
                                spouse.setOnClickListener(null);
                                spouse.setOnLongClickListener(null);
                            }
                            if(currentspouse==null&&CurrentUser.getId().equals(userkey)) {
                                childAdd.setClickable(false);
                                Toast.makeText(TreeLayoutTemp.this,R.string.str_toaddchildaddspouse,Toast.LENGTH_LONG).show();
                            }
                        }
                        else if (spousecount<spouses.size()){
                            childAdd.setClickable(true);
                            currentspouse = spouses.get(++spousecount);

                            Bitmap bitmap = currentspouse.getPhotolink();

                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                            Glide.with(TreeLayoutTemp.this)
                                    .load(stream.toByteArray())
                                    .asBitmap()
                                    .into(spouse);

                            try {
                                String name3 = currentspouse.getFullname() + " ";
                                spousetext.setText(name3.split(" ")[0]);
                            }
                            catch (Exception e){}
                            if (currentspouse.getDateofdeath().equals(""))
                                spouse.setBorderColor(Color.parseColor("#467777"));
                            else {
                                spouse.setBorderColor(Color.parseColor("#160d44"));

                            }

                            spouse.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent i = new Intent(TreeLayoutTemp.this, ProfileView.class);
                                    Bitmap kokos = currentspouse.getPhotolink();
                                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                    kokos.compress(Bitmap.CompressFormat.PNG, 100, stream);


                                    i.putExtra("phototo",stream.toByteArray());

                                    currentspouse.setPhotolink(null);
                                    i.putExtra("user", (Serializable) currentspouse);
                                    i.putExtra("currentUser", userkey);
                                    i.putExtra("who", "spouse");
                                    i.putExtra("currentGender", Gender);
                                    i.putExtra("mainuser", CurrentUser.getId());

                                    TreeLayoutTemp.this.startActivityForResult(i, 1);
                                    currentspouse.setPhotolink(kokos);

                                }
                            });
                            try {
                                String haha = currentspouse.getId();
                                spouse.setOnLongClickListener(new View.OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View v) {
                                        classSetUp1(currentspouse.getId());
                                        return true;
                                    }
                                });
                            } catch (Exception e) {

                            }
                            setChildrenLayout(currentUserchildren,CurrentUser);
                        }

                    }

                    //right2left
                    if(x1>x2)
                    {
                        childAdd.setClickable(true);
                        boolean sex;
                        if(spousecount > 0&&spouses.size()>0){
                            sex = true;
                        }
                        else
                            sex = false;
                        if(sex){
                            childrenLayout.removeAllViews();
                            currentspouse = spouses.get(--spousecount);

                            Bitmap bitmap = currentspouse.getPhotolink();

                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                            Glide.with(TreeLayoutTemp.this)
                                    .load(stream.toByteArray())
                                    .asBitmap()
                                    .into(spouse);
                            try {
                                String name3 = currentspouse.getFullname() + " ";
                                spousetext.setText(name3.split(" ")[0]);
                            }
                            catch (Exception e){}
                            if (currentspouse.getDateofdeath().equals(""))
                                spouse.setBorderColor(Color.parseColor("#467777"));
                            else {
                                spouse.setBorderColor(Color.parseColor("#160d44"));

                            }

                            spouse.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent i = new Intent(TreeLayoutTemp.this, ProfileView.class);
                                    Bitmap kokos = currentspouse.getPhotolink();
                                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                    kokos.compress(Bitmap.CompressFormat.PNG, 100, stream);


                                    i.putExtra("phototo",stream.toByteArray());

                                    currentspouse.setPhotolink(null);
                                    i.putExtra("user", (Serializable) currentspouse);
                                    i.putExtra("currentUser", userkey);
                                    i.putExtra("who", "spouse");
                                    i.putExtra("currentGender", Gender);
                                    i.putExtra("mainuser", CurrentUser.getId());

                                    TreeLayoutTemp.this.startActivityForResult(i, 1);
                                    currentspouse.setPhotolink(kokos);

                                }
                            });
                            try {
                                String haha = currentspouse.getId();
                                spouse.setOnLongClickListener(new View.OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View v) {
                                        classSetUp1(currentspouse.getId());
                                        return true;
                                    }
                                });
                            } catch (Exception e) {

                            }
                            setChildrenLayout(currentUserchildren,CurrentUser);
                        }


                    }

                }
                else
                {
                    // consider as something else - a screen tap for example
                }
                break;
        }
        return super.onTouchEvent(event);
    }
    //----------------On swipe switch to next/previous spouse------------------



    private boolean checkinternetconnectivity(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;
        return  connected;
    }

    private void changelang(String lang){
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        this.setContentView(R.layout.activity_tree_layout_temp);
    }


    private static String language = "languagess";

}
