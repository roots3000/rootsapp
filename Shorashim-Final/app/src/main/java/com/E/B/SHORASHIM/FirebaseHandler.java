package com.E.B.SHORASHIM;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.E.A.SHORASHIM.R;
import com.E.B.SHORASHIM.User_classes.User;
import com.E.B.SHORASHIM.User_classes.User_toUpload;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.Serializable;
import java.util.ArrayList;

import static android.app.Activity.RESULT_FIRST_USER;
import static android.app.Activity.RESULT_OK;

/**
 * Created by Androwsa on 8/21/17.
 */




//honnnnnnnn se3eb aktb kol el share7 ahwn ts2ole ow h2olko keef wesh



public class FirebaseHandler {
    private User_toUpload userprofile;

    StorageReference mountainsRef;

    private StorageReference mStorage;

    UploadTask uploadTask ;

    DatabaseReference dataBaseUser;

    private String msg ;

    DataBaseHelper db;

    DatabaseReference userprofiledatabaseuser = FirebaseDatabase.getInstance().getReference("userprofile");

    public FirebaseHandler(User_toUpload userprofile,Context context) {
        this.userprofile = userprofile;

        msg = context.getResources().getString(R.string.str_sms);

        dataBaseUser = FirebaseDatabase.getInstance().getReference("userprofile");

        db = new DataBaseHelper(context);
    }

    public void saveUser(final Context context, final ProgressDialog progresbar){
        dataBaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean flag= true;
                User temp;
                loop1:
                for (DataSnapshot usersnapshot: dataSnapshot.getChildren()){
                    String fullname = usersnapshot.child("fullname").getValue(String.class);
                    String dateofbirth = usersnapshot.child("dateofbirth").getValue(String.class);
                    String description = usersnapshot.child("description").getValue(String.class);
                    String phone = usersnapshot.child("phone").getValue(String.class);
                    String id= usersnapshot.child("id").getValue(String.class);
                    String dateofdeath = usersnapshot.child("dateofdeath").getValue(String.class);
                    String did = usersnapshot.child("did").getValue(String.class);
                    String activated = usersnapshot.child("activated").getValue(String.class);
                    temp = new User(id,fullname,phone,dateofbirth,description,dateofdeath,did,activated,"");
                    if(userprofile.getPhone().equals(temp.getPhone())){
                        flag = false;

                        Log.e("onDataChange: ",activated);
                        if(activated.equals("active")){
                            Log.e("if active: ", "activeeeeeeeeeeeeeeee");
                            toastCenter(context,R.string.str_error_phonealreadyinuse);
                            progresbar.dismiss();
                        }
                        else{
                            Log.e("if inactive: ", "inactiveeeeeeeeeeeeeeeennnnnnnn");
                            userprofile.setId(usersnapshot.getKey());
                            ArrayList<String> siblings = new ArrayList<String>();
                            ArrayList<String> childrens = new ArrayList<String>();
                            ArrayList<String> spouses = new ArrayList<String>();
                            ArrayList<String> parents = new ArrayList<String>();


                            dataSnapShotFor(usersnapshot , "spouse" , spouses);     // TODO: for into method - DONE
                            dataSnapShotFor(usersnapshot , "children" , childrens);
                            dataSnapShotFor(usersnapshot , "sibling" , siblings);

                            parents.add(usersnapshot.child("parent").child("father").getValue(String.class));
                            parents.add(usersnapshot.child("parent").child("mother").getValue(String.class));

                            dataBaseUser.child(usersnapshot.getKey()).setValue(userprofile);

                            String zsiblings = "";
                            String zchildren = "";
                            String zspouses = "";
                            String zparents = "";
                            for(String s:siblings){
                                dataBaseUser.child(usersnapshot.getKey()).child("sibling").child(s).setValue(s);
                            }
                            for(String s:childrens){
                                dataBaseUser.child(usersnapshot.getKey()).child("children").child(s).setValue(s);
                            }

                            for(String s:spouses){
                                dataBaseUser.child(usersnapshot.getKey()).child("spouse").child(s).setValue(s);
                            }

                            ParentsSet(usersnapshot.getKey() ,parents.get(0), parents.get(1));   // TODO: the next two lines moved into method - DONE


                            toastCenter(context,R.string.str_error_replacedexistinguser);

                            Intent Tree = new Intent(context,TreeLayoutTemp.class);
                            Tree.putExtra("user",userprofile.getId());
                            context.startActivity(Tree);
                            ((Activity)context).finish();
                            progresbar.dismiss();
                        }
                        break loop1;
                    }
                }
                if(flag){
                    Log.e("if new: ", "activeeeeeeeeeeeeeeeesssssssss");
                    dataBaseUser.child(userprofile.getId()).setValue(userprofile);
//                    db.AddProfile(userprofile.getActivated(),userprofile.getDateofbirth(),userprofile.getDateofdeath(),userprofile.getDescription(),
//                            userprofile.getDid(),userprofile.getEmail(),userprofile.getFullname(),userprofile.getGender(),userprofile.getId(),
//                            userprofile.getLat(),userprofile.getLong(),null,userprofile.getPhone(),userprofile.getPhotolink(),null,null,userprofile.getYoutubeid(),null);

                    toastCenter(context,R.string.str_error_madeadnewuser);  // TODO: next block moved into method - DONE

                    Intent Tree = new Intent(context,TreeLayoutTemp.class);
                    Tree.putExtra("user",userprofile.getId());
                    context.startActivity(Tree);
                    ((Activity)context).finish();
                    progresbar.dismiss();
                }

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void EditUser(final User_toUpload user, final Context context, final ProgressDialog progressbar){
        userprofiledatabaseuser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                    if(usersnapshot.child("phone").getValue() != null)
                        if((usersnapshot.child("phone").getValue().equals(user.getPhone())&&!user.getPhone().equals(""))&&!usersnapshot.getKey().equals(user.getId())){
                            if(progressbar!=null)
                                progressbar.dismiss();

                            toastCenter(context,R.string.str_error_phonealreadyinuse);
                            return;
                        }
                }
                for (DataSnapshot usersnapshot: dataSnapshot.getChildren()){
                    if(usersnapshot.child("id").getValue() != null)
                        if(usersnapshot.child("id").getValue(String.class).equals(user.getId())){
                        String father =usersnapshot.child("parent").child("father").getValue(String.class);
                        String mother =usersnapshot.child("parent").child("mother").getValue(String.class);
                        ArrayList<String> spouse= new ArrayList<String>();
                        ArrayList<String> siblings= new ArrayList<String>();
                        ArrayList<String> children= new ArrayList<String>();

                        dataSnapShotFor(usersnapshot , "children" , children);
                        dataSnapShotFor(usersnapshot , "spouse" , spouse);
                        dataSnapShotFor(usersnapshot , "sibling" , siblings);

                        String photolink = usersnapshot.child("photolink").getValue(String.class);
                        if(user.getPhotolink().equals("no")){
                            user.setPhotolink(photolink);
                        }

                        userprofiledatabaseuser.child(user.getId()).setValue(user);

                        String zsiblings = " ";
                        String zchildren = " ";
                        String zspouses = " ";
                        String zparents = " ";
                        for(String sibling:siblings){
                            userprofiledatabaseuser.child(user.getId()).child("sibling").child(sibling).setValue(sibling);
                            zsiblings += sibling+"/";
                        }

                        for(String childrens:children){
                            userprofiledatabaseuser.child(user.getId()).child("children").child(childrens).setValue(childrens);
                            zchildren += childrens+"/";
                        }
                        for(String spouses:spouse){
                            userprofiledatabaseuser.child(user.getId()).child("spouse").child(spouses).setValue(spouses);
                            zspouses += spouses+"/";
                        }
                        ParentsSet(user.getId(),father, mother); // TODO: the next two lines moved into method - DONE

//                        db.updatedata(user.getActivated(),user.getDateofbirth(),user.getDateofdeath(),user.getDescription(),
//                                user.getDid(),user.getEmail(),user.getFullname(),user.getGender(),user.getId(),
//                                user.getLat(),user.getLong(),zparents,user.getPhone(),user.getPhotolink(),zsiblings,zchildren,user.getYoutubeid(),zspouses);

                            toastCenter(context,R.string.str_error_saved);

                            if(progressbar!=null)
                            progressbar.dismiss();

                        Intent i = ((Activity)context).getIntent();
                        ((Activity)context).setResult(RESULT_OK);
                        ((Activity)context).startActivity(i);
                        ((Activity)context).finish();

                    }
                }
                ((Activity)context).finish();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void EditUserForLocation(final User_toUpload user, final Context context, final ProgressDialog progressbar){
        userprofiledatabaseuser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                    if((usersnapshot.child("phone").getValue().equals(user.getPhone())&&!user.getPhone().equals(""))&&!usersnapshot.getKey().equals(user.getId())){
                        if(progressbar!=null)
                            progressbar.dismiss();
                        toastCenter(context,R.string.str_error_phonealreadyinuse);
                        return;
                    }
                }
                for (DataSnapshot usersnapshot: dataSnapshot.getChildren()){
                    if(usersnapshot.child("id").getValue(String.class).equals(user.getId())){
                        String father =usersnapshot.child("parent").child("father").getValue(String.class);
                        String mother =usersnapshot.child("parent").child("mother").getValue(String.class);
                        ArrayList<String> spouse= new ArrayList<String>();
                        ArrayList<String> siblings= new ArrayList<String>();
                        ArrayList<String> children= new ArrayList<String>();


                        dataSnapShotFor(usersnapshot , "children" , children);
                        dataSnapShotFor(usersnapshot , "spouse" , spouse);
                        dataSnapShotFor(usersnapshot , "sibling" , siblings);

                        String photolink = usersnapshot.child("photolink").getValue(String.class);
                        if(user.getPhotolink().equals("no")){
                            user.setPhotolink(photolink);
                        }

                        userprofiledatabaseuser.child(user.getId()).setValue(user);

                        String zsiblings = " ";
                        String zchildren = " ";
                        String zspouses = " ";
                        String zparents = " ";
                        for(String sibling:siblings){
                            userprofiledatabaseuser.child(user.getId()).child("sibling").child(sibling).setValue(sibling);
                            zsiblings += sibling+"/";
                        }

                        for(String childrens:children){
                            userprofiledatabaseuser.child(user.getId()).child("children").child(childrens).setValue(childrens);
                            zchildren += childrens+"/";
                        }
                        for(String spouses:spouse){
                            userprofiledatabaseuser.child(user.getId()).child("spouse").child(spouses).setValue(spouses);
                            zspouses += spouses+"/";
                        }

                        ParentsSet(user.getId(),father, mother);

//                        db.updatedata(user.getActivated(),user.getDateofbirth(),user.getDateofdeath(),user.getDescription(),
//                                user.getDid(),user.getEmail(),user.getFullname(),user.getGender(),user.getId(),
//                                user.getLat(),user.getLong(),zparents,user.getPhone(),user.getPhotolink(),zsiblings,zchildren,user.getYoutubeid(),zspouses);

                        toastCenter(context,R.string.str_error_saved);

                        if(progressbar!=null)
                            progressbar.dismiss();

                        Intent i = ((Activity)context).getIntent();
                        i.putExtra("backeduser",(Serializable) user);
                        ((Activity)context).setResult(RESULT_FIRST_USER,i);
                        ((Activity)context).startActivity(i);
                        ((Activity)context).finish();

                    }
                }
                ((Activity)context).finish();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public boolean checkIfEmailisRelatedToUser(final String Email){
        final boolean[] flag = {false};

        return flag[0];
    }

    public void SaveParent(final Context context, final String mainuserkey, final User_toUpload parent, final ProgressDialog progressbar){

        userprofiledatabaseuser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean sex = true;
                for (final DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                    if (usersnapshot.child("phone").getValue(String.class).equals(parent.getPhone())&&!parent.getPhone().equals("")) {
                        sex = false;
                        progressbar.dismiss();
                        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        alertDialog.setTitle(R.string.str_AddExistingUserToYourtree_title);
                        alertDialog.setMessage(context.getResources().getString(R.string.str_AddExistingUserToYourtree_body));
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getResources().getString(R.string.str_OK),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        userprofiledatabaseuser.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                for (DataSnapshot mainusersnapshot : dataSnapshot.getChildren()){
                                                    if(mainuserkey.equals(mainusersnapshot.getKey())) {
                                                        if (!checkIfUserAddedExistsInTree(mainusersnapshot, usersnapshot)){
                                                            if (usersnapshot.child("gender").getValue(String.class).equals("male")) {
                                                                if (mainusersnapshot.child("parent").child("mother").getValue(String.class) != null) {
                                                                    boolean ifmotherexistsforfather = false;
                                                                    for (DataSnapshot userspouses : usersnapshot.child("spouse").getChildren()) {
                                                                        if (userspouses.getKey().equals(mainusersnapshot.child("parent").child("mother").getValue(String.class))) {
                                                                            userprofiledatabaseuser.child(usersnapshot.getKey()).child("children").child(mainuserkey).setValue(mainuserkey);
                                                                            userprofiledatabaseuser.child(userspouses.getKey()).child("children").child(mainuserkey).setValue(mainuserkey);
                                                                            userprofiledatabaseuser.child(mainuserkey).child("parent").child("father").setValue(usersnapshot.getKey());
                                                                            ifmotherexistsforfather = true;
                                                                        }
                                                                    }
                                                                    if (!ifmotherexistsforfather) {
                                                                        userprofiledatabaseuser.child(usersnapshot.getKey()).child("children").child(mainuserkey).setValue(mainuserkey);
                                                                        userprofiledatabaseuser.child(usersnapshot.getKey()).child("spouse").child(mainusersnapshot.child("parent").child("mother").getValue(String.class)).setValue(mainusersnapshot.child("parent").child("mother").getValue(String.class));
                                                                        userprofiledatabaseuser.child(mainusersnapshot.child("parent").child("mother").getValue(String.class)).child("spouse").child(usersnapshot.getKey()).setValue(usersnapshot.getKey());
                                                                        userprofiledatabaseuser.child(mainuserkey).child("parent").child("father").setValue(usersnapshot.getKey());
                                                                    }
                                                                } else {
                                                                    userprofiledatabaseuser.child(mainuserkey).child("parent").child("father").setValue(usersnapshot.getKey());

                                                                    toastCenter(context,R.string.str_addingexistinguserfather);
                                                                }

                                                                for (DataSnapshot uUuser : usersnapshot.child("children").getChildren()) {
                                                                    if (mainuserkey.equals(uUuser.getKey())) {
                                                                    } else {
                                                                        userprofiledatabaseuser.child(uUuser.getKey()).child("sibling").child(mainuserkey).setValue(mainuserkey);
                                                                        userprofiledatabaseuser.child(mainuserkey).child("sibling").child(uUuser.getKey()).setValue(uUuser.getKey());
                                                                    }
                                                                }

                                                            } else {
                                                                if (mainusersnapshot.child("parent").child("father").getValue(String.class) != null) {
                                                                    boolean ifmotherexistsforfather = false;
                                                                    for (DataSnapshot userspouses : usersnapshot.child("spouse").getChildren()) {
                                                                        if (userspouses.getKey().equals(mainusersnapshot.child("parent").child("father").getValue(String.class))) {
                                                                            userprofiledatabaseuser.child(usersnapshot.getKey()).child("children").child(mainuserkey).setValue(mainuserkey);
                                                                            userprofiledatabaseuser.child(userspouses.getKey()).child("children").child(mainuserkey).setValue(mainuserkey);
                                                                            userprofiledatabaseuser.child(mainuserkey).child("parent").child("mother").setValue(usersnapshot.getKey());
                                                                            ifmotherexistsforfather = true;
                                                                        }
                                                                    }
                                                                    if (!ifmotherexistsforfather) {
                                                                        userprofiledatabaseuser.child(usersnapshot.getKey()).child("children").child(mainuserkey).setValue(mainuserkey);
                                                                        userprofiledatabaseuser.child(usersnapshot.getKey()).child("spouse").child(mainusersnapshot.child("parent").child("father").getValue(String.class)).setValue(mainusersnapshot.child("parent").child("father").getValue(String.class));
                                                                        userprofiledatabaseuser.child(mainusersnapshot.child("parent").child("father").getValue(String.class)).child("spouse").child(usersnapshot.getKey()).setValue(usersnapshot.getKey());
                                                                        userprofiledatabaseuser.child(mainuserkey).child("parent").child("mother").setValue(usersnapshot.getKey());
                                                                    }
                                                                } else {
                                                                    userprofiledatabaseuser.child(mainuserkey).child("parent").child("mother").setValue(usersnapshot.getKey());

                                                                    toastCenter(context,R.string.str_addingexistingusermother);
                                                                }
                                                                for (DataSnapshot uUuser : usersnapshot.child("children").getChildren()) {
                                                                    if (mainuserkey.equals(uUuser.getKey())) {
                                                                    } else {
                                                                        userprofiledatabaseuser.child(uUuser.getKey()).child("sibling").child(mainuserkey).setValue(mainuserkey);
                                                                        userprofiledatabaseuser.child(mainuserkey).child("sibling").child(uUuser.getKey()).setValue(uUuser.getKey());
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else{

                                                            toastCenter(context,R.string.str_addeduserexistsintreealready);
                                                        }
                                                    }
                                                }
                                                progressbar.dismiss();
                                                ((Activity) context).finish();
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });

                                    }
                                });
                        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getResources().getString(R.string.str_NO), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {


                                ((Activity) context).finish();
                                alertDialog.dismiss();
                            }
                        });
                        alertDialog.show();
                    }
                }
                if(sex) {
                    loop1:
                    for (DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                        Log.e("onDataChange: ", mainuserkey + "   lololololololololololololololol");
                        if (usersnapshot.getKey().equals(mainuserkey)) {

                            if (parent.getGender().equals("male")) {
                                userprofiledatabaseuser.child(parent.getId()).setValue(parent);
                                userprofiledatabaseuser.child(mainuserkey).child("parent").child("father").setValue(parent.getId());
                            }
                            if (parent.getGender().equals("female")) {
                                userprofiledatabaseuser.child(parent.getId()).setValue(parent);
                                userprofiledatabaseuser.child(mainuserkey).child("parent").child("mother").setValue(parent.getId());
                            }

                            userprofiledatabaseuser.child(parent.getId()).child("children").child(mainuserkey).setValue(mainuserkey);

                            String father = usersnapshot.child("parent").child("father").getValue(String.class);
                            String mother = usersnapshot.child("parent").child("mother").getValue(String.class);
                            if(!(father==null&& mother==null))
                                if (father == null || mother == null) {
                                    if (parent.getGender().equals("male")) {
                                        userprofiledatabaseuser.child(parent.getId()).child("spouse").child(mother).setValue(mother);
                                        userprofiledatabaseuser.child(mother).child("spouse").child(parent.getId()).setValue(parent.getId());
                                        userprofiledatabaseuser.child(mother).child("children").child(mainuserkey).setValue(mainuserkey);
                                    } else {
                                        userprofiledatabaseuser.child(father).child("spouse").child(parent.getId()).setValue(parent.getId());
                                        userprofiledatabaseuser.child(parent.getId()).child("spouse").child(father).setValue(father);
                                        userprofiledatabaseuser.child(father).child("children").child(mainuserkey).setValue(mainuserkey);
                                    }
                                }
                            progressbar.dismiss();
//                            if(!parent.getPhone().equals("")) {
//                                final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
//                                alertDialog.setTitle(R.string.str_InvitationTitle);
//                                alertDialog.setMessage(context.getResources().getString(R.string.str_InvitationAsk));
//                                final String finalPhone = parent.getPhone();
//                                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getResources().getString(R.string.str_OK),
//                                        new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int which) {
//                                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + finalPhone));
//                                                intent.putExtra("sms_body", msg);
//                                                context.startActivity(intent);
//                                                ((Activity) context).finish();
//                                            }
//                                        });
//                                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getResources().getString(R.string.str_NO), new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        ((Activity) context).finish();
//                                        alertDialog.dismiss();
//                                    }
//                                });
//                                alertDialog.show();
//
//                            }
//                            else{
//                                progressbar.dismiss();
//                                ((Activity) context).finish();
//                            }
                            break loop1;
                        }
                    }
                    progressbar.dismiss();
                    ((Activity) context).finish();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void SaveSpouse(final Context context, final String mainuserkey, final User_toUpload spouse, final ProgressDialog progressbar){
        userprofiledatabaseuser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                boolean sex = true;
                for (final DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                    if (usersnapshot.child("phone").getValue(String.class).equals(spouse.getPhone())&&!spouse.getPhone().equals("")) {
                        progressbar.dismiss();
                        sex = false;
                        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        alertDialog.setTitle(R.string.str_AddExistingUserToYourtree_title);
                        alertDialog.setMessage(context.getResources().getString(R.string.str_AddExistingUserToYourtree_body));
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getResources().getString(R.string.str_OK),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        for (DataSnapshot mainusersnapshot : dataSnapshot.getChildren()){
                                            if(mainuserkey.equals(mainusersnapshot.getKey())) {
                                                if (!checkIfUserAddedExistsInTree(mainusersnapshot, usersnapshot)) {
                                                    userprofiledatabaseuser.child(mainuserkey).child("spouse").child(usersnapshot.getKey()).setValue(usersnapshot.getKey());
                                                    userprofiledatabaseuser.child(usersnapshot.getKey()).child("spouse").child(mainuserkey).setValue(mainuserkey);
                                                    ((Activity) context).finish();
                                                    alertDialog.dismiss();
                                                }
                                                else {

                                                    toastCenter(context,R.string.str_addeduserexistsintreealready);
                                                }
                                            }
                                        }
                                        ((Activity) context).finish();
                                        alertDialog.dismiss();
                                    }
                                });
                        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getResources().getString(R.string.str_NO), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                ((Activity) context).finish();
                                alertDialog.dismiss();
                            }
                        });
                        alertDialog.show();
                    }
                }
                if(sex) {
                    loop1:
                    for (final DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                        if (usersnapshot.getKey().equals(mainuserkey)) {
                            progressbar.dismiss();
                            userprofiledatabaseuser.child(spouse.getId()).setValue(spouse);
                            userprofiledatabaseuser.child(mainuserkey).child("spouse").child(spouse.getId()).setValue(spouse.getId());
                            userprofiledatabaseuser.child(spouse.getId()).child("spouse").child(mainuserkey).setValue(mainuserkey);
                            for(final DataSnapshot children:usersnapshot.child("children").getChildren()){
                                userprofiledatabaseuser.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        for (DataSnapshot childrensnapshot: dataSnapshot.getChildren()){
                                            if(childrensnapshot.getKey().equals(children.getKey())){
                                                if(spouse.getGender().equals("male")){
                                                    if(childrensnapshot.child("parent").child("father").getValue()==null){
                                                        userprofiledatabaseuser.child(childrensnapshot.getKey()).child("parent").child("father").setValue(spouse.getId());
                                                        userprofiledatabaseuser.child(spouse.getId()).child("children").child(childrensnapshot.getKey()).setValue(childrensnapshot.getKey());
                                                    }
                                                }
                                                else{
                                                    if(childrensnapshot.child("parent").child("mother").getValue()==null){
                                                        userprofiledatabaseuser.child(childrensnapshot.getKey()).child("parent").child("mother").setValue(spouse.getId());
                                                        userprofiledatabaseuser.child(spouse.getId()).child("children").child(childrensnapshot.getKey()).setValue(childrensnapshot.getKey());
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                            }
                            break loop1;
                        }
                    }
                    progressbar.dismiss();
                    ((Activity) context).finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    public void SaveSibling(final Context context, final String mainuserkey, final User_toUpload sibling, final ProgressDialog progressbar,final User father,final User mother){
        userprofiledatabaseuser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                boolean sex = true;
                for (DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                    if (usersnapshot.getKey().equals(mainuserkey)) {
                        progressbar.dismiss();
                        String[] parents = new String[2];
                        parents[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                        parents[1] = usersnapshot.child("parent").child("mother").getValue(String.class);
                        if (parents[0] == null || parents[1] == null) {
                            Toast toast = Toast.makeText(context, R.string.str_error_pleaseaddparents, Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            ((Activity) context).finish();
                        }
                    }
                }

                for (final DataSnapshot usersnapshot : dataSnapshot.getChildren()) {

                    if (usersnapshot.child("phone").getValue(String.class).equals(sibling.getPhone())&&!sibling.getPhone().equals("")) {
                        progressbar.dismiss();
                        sex = false;
                        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        alertDialog.setTitle(R.string.str_AddExistingUserToYourtree_title);
                        alertDialog.setMessage(context.getResources().getString(R.string.str_AddExistingUserToYourtree_body));
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getResources().getString(R.string.str_OK),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        for (DataSnapshot mainusersnapshot : dataSnapshot.getChildren()) {
                                            if(mainuserkey.equals(mainusersnapshot.getKey())) {
                                                if (!checkIfUserAddedExistsInTree(mainusersnapshot, usersnapshot)) {

                                                    String fath, math;
                                                    if (usersnapshot.child("parent").child("father").getValue(String.class) == null) {
                                                        fath = "";
                                                    } else {
                                                        fath = usersnapshot.child("parent").child("father").getValue(String.class);
                                                    }
                                                    if (usersnapshot.child("parent").child("mother").getValue(String.class) == null) {
                                                        math = "";
                                                    } else {
                                                        math = usersnapshot.child("parent").child("mother").getValue(String.class);
                                                    }
                                                    if (fath.equals(father.getId()) || math.equals(mother.getId())) {
                                                        if (fath.equals(father.getId()) && math.equals(mother.getId())) {
                                                            userprofiledatabaseuser.child(mainuserkey).child("sibling").child(usersnapshot.getKey()).setValue(usersnapshot.getKey());
                                                            userprofiledatabaseuser.child(usersnapshot.getKey()).child("sibling").child(mainuserkey).setValue(mainuserkey);

                                                            for (String uUuser : father.getChildren()) {
                                                                for (String motherUuser : mother.getChildren()) {
                                                                    if (uUuser.equals(mainuserkey)) {
                                                                    } else if (uUuser.equals(motherUuser)) {
                                                                        userprofiledatabaseuser.child(uUuser).child("sibling").child(usersnapshot.getKey()).setValue(usersnapshot.getKey());
                                                                        userprofiledatabaseuser.child(uUuser).child("sibling").child(mainuserkey).setValue(mainuserkey);
                                                                        userprofiledatabaseuser.child(usersnapshot.getKey()).child("sibling").child(uUuser).setValue(uUuser);
                                                                        userprofiledatabaseuser.child(mainuserkey).child("sibling").child(uUuser).setValue(uUuser);

                                                                    }
                                                                }
                                                            }


                                                        } else if (fath.equals(father.getId())) {
                                                            if (usersnapshot.child("parent").child("mother").getValue(String.class) == null) {
                                                                toastCenter(context,R.string.str_error_siblingmustaddmotherfirst);
                                                            } else {
                                                                userprofiledatabaseuser.child(father.getId()).child("children").child(usersnapshot.getKey()).setValue(usersnapshot.getKey());
                                                                for (String uUuser : father.getChildren()) {
                                                                    userprofiledatabaseuser.child(uUuser).child("sibling").child(usersnapshot.getKey()).setValue(usersnapshot.getKey());
                                                                    userprofiledatabaseuser.child(uUuser).child("sibling").child(mainuserkey).setValue(mainuserkey);
                                                                    userprofiledatabaseuser.child(usersnapshot.getKey()).child("sibling").child(uUuser).setValue(uUuser);
                                                                    userprofiledatabaseuser.child(mainuserkey).child("sibling").child(uUuser).setValue(uUuser);
                                                                }
                                                            }
                                                        } else if (usersnapshot.child("parent").child("mother").getValue(String.class).equals(mother.getId())) {
                                                            if (usersnapshot.child("parent").child("father").getValue(String.class) == null) {

                                                                toastCenter(context,R.string.str_error_siblingmustaddfatherfirst);
                                                            } else {
                                                                userprofiledatabaseuser.child(mother.getId()).child("children").child(usersnapshot.getKey()).setValue(usersnapshot.getKey());
                                                                for (String uUuser : mother.getChildren()) {
                                                                    userprofiledatabaseuser.child(uUuser).child("sibling").child(usersnapshot.getKey()).setValue(usersnapshot.getKey());
                                                                    userprofiledatabaseuser.child(uUuser).child("sibling").child(mainuserkey).setValue(mainuserkey);
                                                                    userprofiledatabaseuser.child(usersnapshot.getKey()).child("sibling").child(uUuser).setValue(uUuser);
                                                                    userprofiledatabaseuser.child(mainuserkey).child("sibling").child(uUuser).setValue(uUuser);
                                                                }
                                                            }
                                                        }

                                                    } else {
                                                        //type that there are no common parents
                                                        toastCenter(context,R.string.str_error_nocommonparents);

                                                    }
                                                }
                                                else {
                                                    toastCenter(context,R.string.str_addeduserexistsintreealready);
                                                }
                                            }
                                        }
                                        ((Activity) context).finish();
                                        alertDialog.dismiss();

                                    }
                                });
                        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getResources().getString(R.string.str_NO), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {


                                progressbar.dismiss();
                                ((Activity) context).finish();
                                alertDialog.dismiss();
                            }
                        });
                        alertDialog.show();
                    }
                }
                if (sex){
                    progressbar.dismiss();
                    loop1:
                    for (DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                        if (usersnapshot.getKey().equals(mainuserkey)) {
                            progressbar.dismiss();
                            String[] parents = new String[2];
                            parents[0] = usersnapshot.child("parent").child("father").getValue(String.class);
                            parents[1] = usersnapshot.child("parent").child("mother").getValue(String.class);
                            if (parents[0] == null || parents[1] == null) {
                                toastCenter(context,R.string.str_error_pleaseaddparents);
                                ((Activity)context).finish();
                            } else {

                                userprofiledatabaseuser.child(sibling.getId()).setValue(sibling);
                                userprofiledatabaseuser.child(sibling.getId()).child("sibling").child(mainuserkey).setValue(mainuserkey);
                                ArrayList<String> siblings = new ArrayList<String>();
                                for (DataSnapshot siblingssnapshot : usersnapshot.child("sibling").getChildren()) {
                                    userprofiledatabaseuser.child(sibling.getId()).child("sibling").child(siblingssnapshot.getKey()).setValue(siblingssnapshot.getKey());

                                    userprofiledatabaseuser.child(siblingssnapshot.getKey()).child("sibling").child(sibling.getId()).setValue(sibling.getId());

                                }
                                // sibling.setSibling(siblings);
                                userprofiledatabaseuser.child(sibling.getId()).child("parent").child("father").setValue(parents[0]);
                                userprofiledatabaseuser.child(sibling.getId()).child("parent").child("mother").setValue(parents[1]);
                                userprofiledatabaseuser.child(parents[0]).child("children").child(sibling.getId()).setValue(sibling.getId());
                                userprofiledatabaseuser.child(parents[1]).child("children").child(sibling.getId()).setValue(sibling.getId());

                                userprofiledatabaseuser.child(mainuserkey).child("sibling").child(sibling.getId()).setValue(sibling.getId());
                                ((Activity) context).finish();
                            }
                            break loop1;
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void SaveChildren(final Context context, final String mainuserkey, final User_toUpload children, final String Gender, final String spouse1, final ProgressDialog progressbar){
        userprofiledatabaseuser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {

                boolean sex = true;
                for (DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                    if (usersnapshot.getKey().equals(mainuserkey)) {

                        String Spouse = null;
                        for (DataSnapshot spouse : usersnapshot.child("spouse").getChildren()) {
                            if (spouse.getKey().equals(spouse1)) {
                                Spouse = spouse.getKey();
                            }
                        }

                        if (Spouse == null) {
                            toastCenter(context,R.string.str_error_addspousefirst);

                            ((Activity) context).finish();
                            progressbar.dismiss();
                            return;
                        }
                    }
                }
                for (final DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                    String phono;

                    if(usersnapshot.child("phone").getValue(String.class)==null){
                        phono ="";
                    }
                    else
                        phono = usersnapshot.child("phone").getValue(String.class);

                    if (phono.equals(children.getPhone())&&!children.getPhone().equals("")) {
                        sex = false;
                        progressbar.dismiss();
                        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        alertDialog.setTitle(R.string.str_AddExistingUserToYourtree_title);
                        alertDialog.setMessage(context.getResources().getString(R.string.str_AddExistingUserToYourtree_body));
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getResources().getString(R.string.str_OK),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        for (DataSnapshot mainusersnapshot : dataSnapshot.getChildren()) {
                                            if(mainuserkey.equals(mainusersnapshot.getKey())) {
                                                if (!checkIfUserAddedExistsInTree(mainusersnapshot, usersnapshot)) {
                                                    if (Gender.equals("male")) {
                                                        if (usersnapshot.child("parent").child("father").getValue(String.class) != null) {

                                                            toastCenter(context,R.string.str_alreadyHasDaddy);

                                                            ((Activity) context).finish();
                                                            alertDialog.dismiss();
                                                            return;
                                                        } else {
                                                            userprofiledatabaseuser.child(spouse1).child("children").child(usersnapshot.getKey()).setValue(usersnapshot.getKey());
                                                            userprofiledatabaseuser.child(mainuserkey).child("children").child(usersnapshot.getKey()).setValue(usersnapshot.getKey());
                                                            userprofiledatabaseuser.child(usersnapshot.getKey()).child("parent").child("father").setValue(mainuserkey);
                                                            userprofiledatabaseuser.child(usersnapshot.getKey()).child("parent").child("mother").setValue(spouse1);
                                                        }
                                                        ArrayList<String> siblings = new ArrayList<String>();
                                                        for (DataSnapshot siblingssnapshot : mainusersnapshot.child("children").getChildren()) {
                                                            userprofiledatabaseuser.child(usersnapshot.getKey()).child("sibling").child(siblingssnapshot.getKey()).setValue(siblingssnapshot.getKey());
                                                            userprofiledatabaseuser.child(siblingssnapshot.getKey()).child("sibling").child(usersnapshot.getKey()).setValue(usersnapshot.getKey());

                                                        }
                                                    } else {
                                                        if (usersnapshot.child("parent").child("mother").getValue(String.class) != null) {

                                                            toastCenter(context,R.string.str_alreadyHasDaddy);

                                                            ((Activity) context).finish();
                                                            alertDialog.dismiss();
                                                            return;
                                                        } else {
                                                            userprofiledatabaseuser.child(spouse1).child("children").child(usersnapshot.getKey()).setValue(usersnapshot.getKey());
                                                            userprofiledatabaseuser.child(mainuserkey).child("children").child(usersnapshot.getKey()).setValue(usersnapshot.getKey());
                                                            userprofiledatabaseuser.child(usersnapshot.getKey()).child("parent").child("father").setValue(spouse1);
                                                            userprofiledatabaseuser.child(usersnapshot.getKey()).child("parent").child("mother").setValue(mainuserkey);
                                                            for(DataSnapshot childrennsnas:usersnapshot.child("children").getChildren()){
                                                                userprofiledatabaseuser.child(childrennsnas.getKey()).child("sibling").child(children.getId()).setValue(children.getId());
                                                                userprofiledatabaseuser.child(children.getId()).child("sibling").child(childrennsnas.getKey()).setValue(childrennsnas.getKey());
                                                            }
                                                        }
                                                    }
                                                }
                                                else {

                                                    toastCenter(context,R.string.str_addeduserexistsintreealready);
                                                }

                                            }
                                        }
                                        ((Activity) context).finish();
                                        if (progressbar != null)
                                            progressbar.dismiss();
                                        alertDialog.dismiss();
                                    }
                                });
                        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getResources().getString(R.string.str_NO), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                progressbar.dismiss();
                                ((Activity) context).finish();
                                alertDialog.dismiss();
                            }
                        });
                        alertDialog.show();
                    }
                }
                if(sex) {
                    loop1:
                    for (DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                        if (usersnapshot.getKey().equals(mainuserkey)) {

                            String Spouse = null;
                            for (DataSnapshot spouse : usersnapshot.child("spouse").getChildren()) {
                                if(spouse.getKey().equals(spouse1)) {
                                    Spouse = spouse.getKey();
                                }
                            }

                            if (Spouse == null) {

                                toastCenter(context,R.string.str_error_addspousefirst);
                                ((Activity)context).finish();
                                progressbar.dismiss();
                                return;
                            } else {
                                userprofiledatabaseuser.child(children.getId()).setValue(children);
                                if (Gender.equals("male")) {
                                    userprofiledatabaseuser.child(children.getId()).child("parent").child("father").setValue(mainuserkey);
                                    userprofiledatabaseuser.child(children.getId()).child("parent").child("mother").setValue(Spouse);
                                } else {
                                    userprofiledatabaseuser.child(children.getId()).child("parent").child("father").setValue(Spouse);
                                    userprofiledatabaseuser.child(children.getId()).child("parent").child("mother").setValue(mainuserkey);
                                }

                                ArrayList<String> siblings = new ArrayList<String>();
                                for (DataSnapshot siblingssnapshot : usersnapshot.child("children").getChildren()) {
                                    userprofiledatabaseuser.child(children.getId()).child("sibling").child(siblingssnapshot.getKey()).setValue(siblingssnapshot.getKey());
                                    userprofiledatabaseuser.child(siblingssnapshot.getKey()).child("sibling").child(children.getId()).setValue(children.getId());

                                }

                                userprofiledatabaseuser.child(Spouse).child("children").child(children.getId()).setValue(children.getId());
                                userprofiledatabaseuser.child(mainuserkey).child("children").child(children.getId()).setValue(children.getId());
                                progressbar.dismiss();
                                ((Activity) context).finish();

                            }

                            break loop1;
                        }

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private boolean checkIfUserAddedExistsInTree(DataSnapshot mainuser , DataSnapshot addeduser){
        String father = mainuser.child("parent").child("father").getValue(String.class);
        String mother = mainuser.child("parent").child("mother").getValue(String.class);
        if(father != null){
            if(addeduser.getKey().equals(father)) {

                return true;
            }
        }
        if(mother != null){
            if(addeduser.getKey().equals(mother)) {
                return true;
            }
        }

        for(DataSnapshot sibling: mainuser.child("sibling").getChildren()){
            if(sibling.getKey().equals(addeduser.getKey()))
                return true;
        }
        for(DataSnapshot spouses: mainuser.child("spouse").getChildren()){
            if(spouses.getKey().equals(addeduser.getKey()))
                return true;
        }
        for(DataSnapshot children: mainuser.child("children").getChildren()){
            if(children.getKey().equals(addeduser.getKey()))
                return true;
        }


        return false;
    }


    public void syncronisedatabases(){

    }


    private void toastCenter(Context context,int s){
        Toast toast=Toast.makeText(context,s, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private void ParentsSet(String id, String father, String mother){
        dataBaseUser.child(id).child("parent").child("father").setValue(father);
        dataBaseUser.child(id).child("parent").child("mother").setValue(mother);
    }

    private void dataSnapShotFor(DataSnapshot usersnapshot , String child , ArrayList<String> arr){
        for(DataSnapshot snapshot: usersnapshot.child(child).getChildren()){
            arr.add(snapshot.getKey());
        }
    }
}
