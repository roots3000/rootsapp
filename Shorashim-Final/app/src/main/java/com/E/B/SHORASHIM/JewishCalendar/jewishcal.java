package com.E.B.SHORASHIM.JewishCalendar;

import android.content.Context;

import com.E.A.SHORASHIM.R;

/**
 * Created by Androwsa on 9/5/17.
 */

public class jewishcal {

    public Context context;

    public jewishcal(Context context1) {
        context =context1;
    }

    public static boolean hebrewLeapYear(int year) {
        if ((((year*7)+1) % 19) < 7)
            return true;
        else
            return false;
    }

    public String getJewishMonthName(int monthNumber, int year) {
        String[] jewishMonthNamesLeap = {this.context.getResources().getString(R.string.hebrew_month_nisan), this.context.getResources().getString(R.string.hebrew_month_Iyar),this.context.getResources().getString(R.string.hebrew_month_Sivan), this.context.getResources().getString(R.string.hebrew_month_Tammuz),
                this.context.getResources().getString(R.string.hebrew_month_Av),this.context.getResources().getString(R.string.hebrew_month_Elul),this.context.getResources().getString(R.string.hebrew_month_Tishri),this.context.getResources().getString(R.string.hebrew_month_Heshvan),
                this.context.getResources().getString(R.string.hebrew_month_Kislev),this.context.getResources().getString(R.string.hebrew_month_Tevet),this.context.getResources().getString(R.string.hebrew_month_Shevat),this.context.getResources().getString(R.string.hebrew_month_Adar1),this.context.getResources().getString(R.string.hebrew_month_Adar2)};
        String[] jewishMonthNamesNonLeap = {String.valueOf(R.string.hebrew_month_nisan), this.context.getResources().getString(R.string.hebrew_month_Iyar),this.context.getResources().getString(R.string.hebrew_month_Sivan), this.context.getResources().getString(R.string.hebrew_month_Tammuz),
                this.context.getResources().getString(R.string.hebrew_month_Av),this.context.getResources().getString(R.string.hebrew_month_Elul),this.context.getResources().getString(R.string.hebrew_month_Tishri),this.context.getResources().getString(R.string.hebrew_month_Heshvan),
                this.context.getResources().getString(R.string.hebrew_month_Kislev),this.context.getResources().getString(R.string.hebrew_month_Tevet),this.context.getResources().getString(R.string.hebrew_month_Shevat), this.context.getResources().getString(R.string.hebrew_month_Adar)};

        if(hebrewLeapYear(year)) {
            return jewishMonthNamesLeap[monthNumber-1];
        } else {
            return jewishMonthNamesNonLeap[monthNumber-1];
        }
    }

    public String getJewishYearinHebrew(int year){
        int alaf = year/1000;
        int me2at =year%1000/100;
        int day = year%10;
        int s3sharat = year%100/10;

        String yom = "";
        String S3sharat = "";
        String Me2at = "";
        String Alaf = "";
        switch (day){ // TODO: move switch into method
            case 1:
                yom = context.getResources().getString(R.string.hebrew_year_1);
                break;
            case 2:
                yom = context.getResources().getString(R.string.hebrew_year_2);
                break;
            case 3:
                yom = context.getResources().getString(R.string.hebrew_year_3);
                break;
            case 4:
                yom = context.getResources().getString(R.string.hebrew_year_4);
                break;
            case 5:
                yom = context.getResources().getString(R.string.hebrew_year_5);
                break;
            case 6:
                yom = context.getResources().getString(R.string.hebrew_year_6);
                break;
            case 7:
                yom = context.getResources().getString(R.string.hebrew_year_7);
                break;
            case 8:
                yom = context.getResources().getString(R.string.hebrew_year_8);
                break;
            case 9:
                yom = context.getResources().getString(R.string.hebrew_year_9);
                break;
        }

        switch (s3sharat){
            case 1:
                S3sharat = context.getResources().getString(R.string.hebrew_year_10);
                break;
            case 2:
                S3sharat = context.getResources().getString(R.string.hebrew_year_20);
                break;
            case 3:
                S3sharat = context.getResources().getString(R.string.hebrew_year_30);
                break;
            case 4:
                S3sharat = context.getResources().getString(R.string.hebrew_year_40);
                break;
            case 5:
                S3sharat = context.getResources().getString(R.string.hebrew_year_50);
                break;
            case 6:
                S3sharat = context.getResources().getString(R.string.hebrew_year_60);
                break;
            case 7:
                S3sharat = context.getResources().getString(R.string.hebrew_year_70);
                break;
            case 8:
                S3sharat = context.getResources().getString(R.string.hebrew_year_80);
                break;
            case 9:
                S3sharat = context.getResources().getString(R.string.hebrew_year_90);
                break;
        }

        switch (me2at){
            case 1:
                Me2at = context.getResources().getString(R.string.hebrew_year_100);
                break;
            case 2:
                Me2at = context.getResources().getString(R.string.hebrew_year_200);
                break;
            case 3:
                Me2at = context.getResources().getString(R.string.hebrew_year_300);
                break;
            case 4:
                Me2at = context.getResources().getString(R.string.hebrew_year_400);
                break;
            case 5:
                Me2at = context.getResources().getString(R.string.hebrew_year_400)+context.getResources().getString(R.string.hebrew_year_100);
                break;
            case 6:
                Me2at = context.getResources().getString(R.string.hebrew_year_400)+context.getResources().getString(R.string.hebrew_year_200);
                break;
            case 7:
                Me2at = context.getResources().getString(R.string.hebrew_year_400)+context.getResources().getString(R.string.hebrew_year_300);
                break;
            case 8:
                Me2at = context.getResources().getString(R.string.hebrew_year_400)+context.getResources().getString(R.string.hebrew_year_400);
                break;
            case 9:
                Me2at = context.getResources().getString(R.string.hebrew_year_400)+context.getResources().getString(R.string.hebrew_year_400)+context.getResources().getString(R.string.hebrew_year_100);
                break;
        }

        switch (alaf){
            case 1:
                Alaf = context.getResources().getString(R.string.hebrew_year_1);
                break;
            case 2:
                Alaf = context.getResources().getString(R.string.hebrew_year_2);
                break;
            case 3:
                Alaf = context.getResources().getString(R.string.hebrew_year_3);
                break;
            case 4:
                Alaf = context.getResources().getString(R.string.hebrew_year_4);
                break;
            case 5:
                Alaf = context.getResources().getString(R.string.hebrew_year_5);
                break;
            case 6:
                Alaf = context.getResources().getString(R.string.hebrew_year_6);
                break;
            case 7:
                Alaf = context.getResources().getString(R.string.hebrew_year_7);
                break;
            case 8:
                Alaf = context.getResources().getString(R.string.hebrew_year_8);
                break;
            case 9:
                Alaf = context.getResources().getString(R.string.hebrew_year_9);
                break;
        }

        String returned;
        if(yom.equals(""))
        {
            returned =Alaf + Me2at + S3sharat;
        }
        else {
            returned = Alaf + Me2at + S3sharat + '"' + yom;
        }
        return returned;
    }




    public String getJewishdayinHebrew(int DDay){
        int day = DDay%10;
        int s3sharat = DDay/10;

        String yom = "";
        String S3sharat = "";
        switch (day){
            case 1:
                yom = context.getResources().getString(R.string.hebrew_year_1);
                break;
            case 2:
                yom = context.getResources().getString(R.string.hebrew_year_2);
                break;
            case 3:
                yom = context.getResources().getString(R.string.hebrew_year_3);
                break;
            case 4:
                yom = context.getResources().getString(R.string.hebrew_year_4);
                break;
            case 5:
                yom = context.getResources().getString(R.string.hebrew_year_5);
                break;
            case 6:
                yom = context.getResources().getString(R.string.hebrew_year_6);
                break;
            case 7:
                yom = context.getResources().getString(R.string.hebrew_year_7);
                break;
            case 8:
                yom = context.getResources().getString(R.string.hebrew_year_8);
                break;
            case 9:
                yom = context.getResources().getString(R.string.hebrew_year_9);
                break;
        }

        switch (s3sharat){
            case 1:
                S3sharat = context.getResources().getString(R.string.hebrew_year_10);
                break;
            case 2:
                S3sharat = context.getResources().getString(R.string.hebrew_year_20);
                break;
            case 3:
                S3sharat = context.getResources().getString(R.string.hebrew_year_30);
                break;
            case 4:
                S3sharat = context.getResources().getString(R.string.hebrew_year_40);
                break;
            case 5:
                S3sharat = context.getResources().getString(R.string.hebrew_year_50);
                break;
            case 6:
                S3sharat = context.getResources().getString(R.string.hebrew_year_60);
                break;
            case 7:
                S3sharat = context.getResources().getString(R.string.hebrew_year_70);
                break;
            case 8:
                S3sharat = context.getResources().getString(R.string.hebrew_year_80);
                break;
            case 9:
                S3sharat = context.getResources().getString(R.string.hebrew_year_90);
                break;
        }

        String returned;
        if(yom.equals(""))
        {
            returned = S3sharat;
        }
        else {
            returned =S3sharat + '"' + yom;
        }
        if(S3sharat.equals("")){
            returned = yom;
        }

        return returned;
    }
    //5911
}
