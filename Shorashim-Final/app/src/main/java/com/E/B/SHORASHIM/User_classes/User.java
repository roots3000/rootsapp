package com.E.B.SHORASHIM.User_classes;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;

/**
 * Created by Androwsa on 8/21/17.
 */

public class User extends Main_User{ // TODO: Ahmad: check with androw that this works

    private Bitmap photolink=null;
    public String url ;

    public User(String id, String fullname, String phone, String dateofbirth, String description, String activated, String email, String dateofdeath, String did) {
        super(id, fullname, phone, dateofbirth, description, activated, email, dateofdeath, did);
    }

    public User(DataSnapshot usersnapshot)
    {
        super(usersnapshot.child("id").getValue(String.class),usersnapshot.child("fullname").getValue(String.class),usersnapshot.child("phone").getValue(String.class),
                usersnapshot.child("dateofbirth").getValue(String.class),usersnapshot.child("description").getValue(String.class),
                usersnapshot.child("activated").getValue(String.class),usersnapshot.child("email").getValue(String.class),
                usersnapshot.child("dateofdeath").getValue(String.class),usersnapshot.child("did").getValue(String.class));
        String [] tempparentArray = new String[2];
        tempparentArray[0] = usersnapshot.child("parent").child("father").getValue(String.class);
        tempparentArray[1] = usersnapshot.child("parent").child("mother").getValue(String.class);
        url = usersnapshot.child("photolink").getValue(String.class);
        ArrayList<String> tempspousearray ; //TODO: EXTRACT TO METHOD that gets a string array
        tempspousearray = get_relative_temp_array("spouse", usersnapshot);
        ArrayList<String> tempsiblingArray ;
        tempsiblingArray = get_relative_temp_array("sibling", usersnapshot);
        ArrayList<String> tempchildrenArray ;
        tempchildrenArray = get_relative_temp_array("children", usersnapshot);
        String vidId = usersnapshot.child("youtubeid").getValue(String.class);
        String gender = usersnapshot.child("gender").getValue(String.class);
        String Lat = usersnapshot.child("lat").getValue(String.class);
        String Long = usersnapshot.child("long").getValue(String.class);
        this.setLat(Lat);
        this.setLong(Long);
        this.setGender(gender);
        this.setYoutubeid(vidId);
        this.setParent(tempparentArray);
        this.setSpouse(tempspousearray);
        this.setSibling(tempsiblingArray);
        this.setChildren(tempchildrenArray);
    }

    public User(Cursor alldata)
    {
      super(alldata.getString(8),alldata.getString(6),alldata.getString(12),
              alldata.getString(1),alldata.getString(3),
              alldata.getString(0),alldata.getString(5),
              alldata.getString(2),alldata.getString(4));
        String father = alldata.getString(11).split("/")[0];
        String mother = alldata.getString(11).split("/")[1];

        String[] parentss = new String[2];
        parentss[0] = father;
        parentss[1] = mother;

        ArrayList<String> siblings = new ArrayList<>();
        String[] siblingarray = alldata.getString(14).split("/");
        for (String sibl : siblingarray) {
            siblings.add(sibl);
        }

        ArrayList<String> children = new ArrayList<>();
        String[] childrenarray = alldata.getString(15).split("/");
        for (String child : childrenarray) {
            children.add(child);
        }

        String youtubeid = alldata.getString(16);

        ArrayList<String> spouses = new ArrayList<>();
        String[] spousearray = alldata.getString(17).split("/");
        for (String spouse : spousearray) {
            spouses.add(spouse);
        }
        this.setGender(alldata.getString(7));
        this.setYoutubeid(youtubeid);
        this.setParent(parentss);
        this.setSpouse(spouses);
        this.setSibling(siblings);
        this.setChildren(children);

        byte[] photolink = alldata.getBlob(13);
        Bitmap bitmap = BitmapFactory.decodeByteArray(photolink, 0, photolink.length);
        this.setPhotolink(bitmap);
    }
    protected ArrayList<String> get_relative_temp_array(String relationship, DataSnapshot usersnapshot)
    {
        ArrayList<String> tempRelativeArray = new ArrayList<>();
        for(DataSnapshot spousesnapshot: usersnapshot.child(relationship).getChildren()){
            tempRelativeArray.add(spousesnapshot.getKey());
        }
        return tempRelativeArray;
    }
    public Bitmap getPhotolink() {
        return photolink;
    }

    public void setPhotolink(Bitmap photolink) {
        this.photolink = photolink;
    }
}
