package com.E.B.SHORASHIM;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.E.A.SHORASHIM.R;
import com.E.B.SHORASHIM.User_classes.User;
import com.E.B.SHORASHIM.User_classes.User_toUpload;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StreamDownloadTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Date;
import java.util.stream.Stream;

import de.hdodenhof.circleimageview.CircleImageView;

public class SaveData extends AppCompatActivity {


    EditText firstname,lastname,phone,day,month,year,About,youtubeVid;

    Button back, save;

    DatabaseReference dataBaseUser;
    User user;
    String Phone;

    CircleImageView profilepicture;

    Spinner kedomet;

    LinearLayout iffemaleLayout;

    Bitmap bit;

    StorageReference storageRef ;

    private ProgressDialog progressbar;

    StorageReference mountainsRef;

    private StorageReference mStorage;

    Uri thepicture;

    UploadTask uploadTask ;

    String id;

    private static final int PICK_FROM_GALLERY = 1;

    RadioButton male, female;

    private FirebaseAuth.AuthStateListener mAuthListener;

    private FirebaseAuth firebaseAuth;

    String previouslastname;

    EditText previouslastnameet;

    boolean ifpicturepicked = false;

    countryCodes codes = new countryCodes();

    String googleFacebookSignIn;


    //After You register this is the page to create your profile just like the profile view works the same way

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_data); // TODO : create an abstract class to all the implements an interface that will be decided on
        tzbeterView();

        profilepicture = (CircleImageView) findViewById(R.id.SDprofile_image);

        youtubeVid = (EditText) findViewById(R.id.SDYoutubevideo);

        googleFacebookSignIn = (String) getIntent().getExtras().getString("google_facebook");


        kedomet = (Spinner) findViewById(R.id.spinner);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, codes.countryCodes);

        kedomet.setAdapter(adapter);

        firebaseAuth = FirebaseAuth.getInstance();

        progressbar = new ProgressDialog(this);

        male= (RadioButton) findViewById(R.id.SDMale);

        mStorage = FirebaseStorage.getInstance().getReference();

        female = (RadioButton) findViewById(R.id.SDFemale);

        storageRef = FirebaseStorage.getInstance().getReference();

        About= (EditText) findViewById(R.id.SDabout);

        iffemaleLayout = (LinearLayout) findViewById(R.id.SDifemalelayout);

        dataBaseUser = FirebaseDatabase.getInstance().getReference("userprofile");

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveData.this.startActivity(new Intent(SaveData.this,MainActivity.class));
                firebaseAuth.signOut();
                finish();
            }
        });

        male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iffemaleLayout.removeAllViews();
                if(!ifpicturepicked) {
                    Uri uri = Uri.parse("android.resource://" + getPackageName() + "/drawable/male");
                    profilepicture.setImageURI(uri);
                }

                previouslastname = " ";
            }
        });

        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previouslastnameet = new EditText(SaveData.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f);
                lp.setMargins(0,10,0,0);
                previouslastnameet.setLayoutParams(lp);

                previouslastnameet.setHint("Previous last name");

                iffemaleLayout.addView(previouslastnameet);


                if(!ifpicturepicked) {
                    Uri uri = Uri.parse("android.resource://" + getPackageName() + "/drawable/female");

                    profilepicture.setImageURI(uri);
                }

                previouslastname = previouslastnameet.getText().toString().trim();
            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String Firstname = firstname.getText().toString().trim();
                String Lastname = lastname.getText().toString().trim();
                Phone = phone.getText().toString().trim();
                String days=day.getText().toString().trim();
                String months=month.getText().toString().trim();
                String years=year.getText().toString().trim();
                String about = About.getText().toString().trim();
                String Yotube= youtubeVid.getText().toString().trim();


                if(TextUtils.isEmpty(Firstname)){
                    Toast.makeText(SaveData.this,R.string.str_error_enterfullname,Toast.LENGTH_SHORT).show();
                    return;
                }



                if(TextUtils.isEmpty(Lastname)){
                    Toast.makeText(SaveData.this,R.string.str_error_enterlastname,Toast.LENGTH_SHORT).show();
                    return;
                }

                if(TextUtils.isEmpty(Phone)){
                    Toast.makeText(SaveData.this,R.string.str_error_enternumber,Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(days)||TextUtils.isEmpty(months)||TextUtils.isEmpty(years)){
                    Toast.makeText(SaveData.this,R.string.str_error_entervalidday,Toast.LENGTH_SHORT).show();
                    return;
                }

                int Day = Integer.parseInt(days);
                int Month = Integer.parseInt(months);
                int Year = Integer.parseInt(years);
                Firstname = Firstname.replace(" ", "");
                Lastname = Lastname.replace(" ", "");
                String Date = Day+"/"+Month+"/"+Year;

                if(Day>0&&Day<32){

                }
                else{
                    Toast.makeText(SaveData.this,R.string.str_error_entervalidday,Toast.LENGTH_SHORT).show();
                    return;
                }

                if(Month>0&&Month<13){

                }
                else{
                    Toast.makeText(SaveData.this,R.string.str_error_entervalidmonth,Toast.LENGTH_SHORT).show();
                    return;
                }

                if(Year>0&&Year<2020){

                }
                else{
                    Toast.makeText(SaveData.this,R.string.str_error_entervalidyear,Toast.LENGTH_SHORT).show();
                    return;
                }

                if(Phone.length()==9){
                    Phone = "+"+getplace(kedomet.getSelectedItem().toString())+Phone;
                    Log.e("onClick: ",Phone+ " lslslslslslslslslslslslslsl" );
                }
                else if(Phone.length() == 10){
                    Phone = Phone.substring(1);
                    Phone = "+"+getplace(kedomet.getSelectedItem().toString())+Phone;
                    Log.e("onClick: ",Phone+ " lslslslslslslslslslslslslsl" );
                }
                else {
                    Toast.makeText(SaveData.this, R.string.str_error_entervalidphone, Toast.LENGTH_SHORT).show();
                    return;
                }

                if(!male.isChecked()&&!female.isChecked()){
                    Toast.makeText(SaveData.this,R.string.str_error_chooseGender,Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(about)){
                    about = " ";
                }

                String gender;
                if(male.isChecked()) {
                    gender = "male";

                }else
                    gender = "female";


                Lastname+=" "+ previouslastname;


                progressbar.setMessage(getResources().getString(R.string.str_error_saving));
                progressbar.show();

                id = dataBaseUser.push().getKey();

                final User_toUpload user = new User_toUpload(id,Firstname+" "+Lastname,Phone,Date,about,"active",firebaseAuth.getCurrentUser().getEmail());

                user.setGender(gender);

                if(!TextUtils.isEmpty(Yotube))
                    try {
                        String s = Yotube.split("/")[3];
                        user.setYoutubeid(Yotube);
                    }
                    catch (Exception e){
                        Toast.makeText(SaveData.this,getResources().getString(R.string.str_error_invalidyoutubelink),Toast.LENGTH_LONG).show();
                        progressbar.dismiss();
                        return;
                    }

                StorageReference storageref= mStorage.child("photos").child(id);
                if(!ifpicturepicked){
                    if(gender.equals("male")){
                        save_user("android.resource://"+getPackageName()+"/drawable/male",user);
                    }
                    else{
                        save_user("android.resource://"+getPackageName()+"/drawable/female",user);
                    }
                }
                else{
                    storageref.putFile(thepicture).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            user.setPhotolink(taskSnapshot.getDownloadUrl().toString());

                            FirebaseHandler firehandler = new FirebaseHandler(user,SaveData.this);

                            firehandler.saveUser(SaveData.this,progressbar);
                        }
                    });
                }

            }
        });

        profilepicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                int gallery = 1;

                startActivityForResult(pickPhoto,gallery);

            }
        });


        if(googleFacebookSignIn!=null)ifGoogle_FacebookSignedIn();

    }


    //sets all the viewup
    private void tzbeterView(){
        firstname = (EditText) findViewById(R.id.SDname);
        firstname.requestFocus();
        lastname = (EditText) findViewById(R.id.SDlastname);
        phone= (EditText) findViewById(R.id.SDphone);
        day= (EditText) findViewById(R.id.SDdateday);
        month = (EditText) findViewById(R.id.SDdatemonth);
        year= (EditText) findViewById(R.id.SDdateyear);

        back= (Button) findViewById(R.id.SDback);
        save= (Button) findViewById(R.id.SDsave);

    }


    //Requests permission to use camera (Not Used Yet)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 200:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
                } else {
                    Toast.makeText(SaveData.this,R.string.str_error_allowpermissionforpicture,Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    //After choosing picture from gallery this activity is called to view and store the picture
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case 1:
                if(resultCode == RESULT_OK){
                    ifpicturepicked = true;
                    Uri selectedImage = data.getData();
                    Glide.with(this)
                            .load(selectedImage)
                            .into(profilepicture);
                    thepicture = selectedImage;

                }
                break;
        }
    }


    //kedomot
    private String getplace(String k){
        int j=-1;
        for(int i = 0;i<codes.countryCodes.length;i++){
            if(codes.countryCodes[i].equals(k)){
                j = i;
            }
        }
        return codes.countryAreaCodes[j];
    }

    private void save_user(String ur, final User_toUpload user){
        Uri uri;
        StorageReference storageref= mStorage.child("photos").child(id);
        uri = Uri.parse(ur);
        storageref.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                user.setPhotolink(taskSnapshot.getDownloadUrl().toString());


                FirebaseHandler firehandler = new FirebaseHandler(user,SaveData.this);

                firehandler.saveUser(SaveData.this,progressbar);
            }

        });
    }


    private void ifGoogle_FacebookSignedIn(){
        final String FN = (String) getIntent().getStringExtra("google_facebook_FN");
        final String LN = (String) getIntent().getStringExtra("google_facebook_LN");
        final String BD = (String) getIntent().getStringExtra("google_facebook_BD");
        final String GN = (String) getIntent().getStringExtra("google_facebook_GN");
        String uriPath = (String) getIntent().getStringExtra("google_facebook_PU");
        String PhoneNumber = (String) getIntent().getStringExtra("google_facebook_PH");
        Uri uri = Uri.parse(uriPath);

        Log.e("--------------------asfasfasfasfads", "ifGoogle_FacebookSignedIn: "+ uriPath );

        firstname.setText(FN);
        lastname.setText(LN);
        if(BD!=null && !BD.equals("")) {

            String month = BD.split("/")[0];
            String day = BD.split("/")[1];
            String year = BD.split("/")[2];

            this.month.setText(month);
            this.day.setText(day);
            this.year.setText(year);
        }

            if(GN!=null && !GN.equals("")) {
                if (GN.equals("male")) this.male.setChecked(true);
                else if (GN.equals("female")) this.female.setChecked(true);
            }
//        Glide.with(this)
//                .load(uri)
//                .into(profilepicture);

//        thepicture = uri;
        phone.setText(PhoneNumber);
//        ifpicturepicked = true;
    }

}
