package com.E.B.SHORASHIM;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.view.Gravity;
import android.view.View.OnClickListener;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.E.A.SHORASHIM.R;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Account;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText email,pass;

    GoogleSignInOptions gso;

    Button login,register;

    TextView forgotText;

    private static final String FacebookEmailAlreadyExistsError = "An account already exists with the same email address but different sign-in credentials. Sign in using a provider associated with this email address.";

    CallbackManager mCallbackManager;
    LoginButton Facebook;
    SignInButton Google1;
    GoogleSignInClient mGoogleSignInClient;

    SharedPreferences sharedpref;

    SharedPreferences.Editor editor;

    DatabaseReference dataBaseUser;

    private ProgressDialog progressbar;

    private FirebaseAuth firebaseauth;

    private FirebaseUser temp;

    private FirebaseHandler myHandler;
    private GoogleApiClient mGoogleApiClient;

    Button Lang;

    private static String ifloggedin = "iflogged";
    private static String Currentuser = "currentuser";
    private static String language = "languagess";

    private String LANG_CURRENT = "en";

    // Loging Page



    //Called when we register, it puts the email and password automaticly when email verfication is sent
    //also when we change language
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        LoginManager.getInstance().logOut();
        if(resultCode == RESULT_OK){

            String em = data.getExtras().getString("email");
            String ps = data.getExtras().getString("pass");
            email.setText(em);
            pass.setText(ps);
        }

        if( resultCode == RESULT_FIRST_USER){
            String languageToLoad = data.getExtras().getString(language);// your language

            editor.putString(language,languageToLoad);
            editor.commit();

            changelang(languageToLoad);
            classsetup();
        }
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == 101) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);


            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w("bjakgdajkshfjweh", "Google sign in failed", e);
                // ...
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        setContentView(R.layout.activity_main);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        classsetup();
    }

    private boolean checkinternetconnectivity(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;
        return  connected;
    }

    private void classsetup(){
        sharedpref = this.getSharedPreferences("shared",MODE_PRIVATE);
        editor = sharedpref.edit();


        String languageToLoad="";

        String temporary1= sharedpref.getString(language,"");
        try {
            languageToLoad = sharedpref.getString(language, temporary1); // your language
        }
        catch(Exception e){}
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        this.setContentView(R.layout.activity_main);
//-----------------------------------
        // Configure Google Sign In
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestScopes(Plus.SCOPE_PLUS_PROFILE)
                .requestScopes(Plus.SCOPE_PLUS_LOGIN)
                .requestScopes(new Scope("https://www.googleapis.com/auth/user.birthday.read"))
                .requestProfile()
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addScope(new Scope("https://www.googleapis.com/auth/user.birthday.read"))
                .addScope(new Scope(Scopes.PLUS_LOGIN))
                .addScope(new Scope(Scopes.PROFILE))
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addApi(Plus.API)
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this,gso);
        Facebook = (LoginButton) findViewById(R.id.login_button_facebook);
        Facebook.setOnClickListener(this);

        Google1 = (SignInButton) findViewById(R.id.google_main1);
        Log.e( "yooo","onClick:------------------ -1  " );
        Google1.setOnClickListener(this);
        Log.e( "yooo","onClick:------------------ -1  " );
        Log.e("----------------", "facebook:onSuccess:ksjkdhkashkfjwekd-----0");
        mCallbackManager = CallbackManager.Factory.create();
//        Facebook.setReadPermissions("email", "public_profile");
        Facebook.setReadPermissions(Arrays.asList("email", "public_profile","user_gender","user_birthday"));
//-----------------------------------


        Lang= (Button) findViewById(R.id.langMain);

        LangButtonTextChange(languageToLoad);

        Lang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Tree = new Intent(MainActivity.this, LangChoose.class);

                MainActivity.this.startActivityForResult(Tree,0);
            }
        });

        RemoteViews rmv = new RemoteViews(getPackageName(),R.layout.activity_main);

        TextView title = (TextView) findViewById(R.id.textView);
        title.setText(R.string.str_shorashim);

        email = (EditText) findViewById(R.id.LGemail);
        pass = (EditText) findViewById(R.id.LGpassword);

        login = (Button) findViewById(R.id.LGlogin);
        register = (Button) findViewById(R.id.LGregister);

        forgotText = (TextView) findViewById(R.id.LGforgotpass);

        dataBaseUser = FirebaseDatabase.getInstance().getReference("userprofile");

        progressbar = new ProgressDialog(this);

        firebaseauth = FirebaseAuth.getInstance();


        // eza fee internet bet7aber l3ade mn lfirebase
        if(checkinternetconnectivity()) {
            final FirebaseUser temp2 = firebaseauth.getCurrentUser();
            if (temp2 != null) {
                signedIn("","","","","","","");
            }
        }
        //eza fsh internet az 3n tree2 lsharedprefrence wb3ml display lal SQL DATA
        else{
            //connect using sharedprefrence
            int temp = sharedpref.getInt(ifloggedin,-1);
            if(temp==1){
                Intent Tree = new Intent(MainActivity.this, TreeLayoutTemp.class);
                String temporary= sharedpref.getString(Currentuser,"");
                sharedpref.getString(Currentuser,temporary);
                Tree.putExtra("user", temporary);
                MainActivity.this.startActivity(Tree);
                MainActivity.this.finish();
                progressbar.dismiss();
            }
        }


        //Forgot Password
        forgotText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkinternetconnectivity())
                    MainActivity.this.startActivity(new Intent(MainActivity.this,Password.class));

                else
                    Toast.makeText(MainActivity.this,R.string.str_error_nointernetconnection,Toast.LENGTH_LONG).show();

            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkinternetconnectivity()){
                    final String EMAIL = email.getText().toString().trim();
                    final String Pass = pass.getText().toString().trim();

                    boolean flag = ifEmpty(EMAIL, getResources().getString(R.string.str_error_enteremail)) || ifEmpty(Pass, getResources().getString(R.string.str_error_enterpassword));

                    if(flag) return;

                    progressbar.setMessage(getResources().getString(R.string.str_error_loggingin) + "");
                    progressbar.show();

                    firebaseauth.signInWithEmailAndPassword(EMAIL, Pass).addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                    signedIn("","","","","","",null);
                            } else {
                                Toast.makeText(MainActivity.this, R.string.str_error_usernameorpasswordincorrect, Toast.LENGTH_SHORT).show();
                                progressbar.dismiss();

                            }
                        }
                    });
                }
                else{
                    Toast.makeText(MainActivity.this,R.string.str_error_nointernetconnection,Toast.LENGTH_LONG).show();
                }
            }

        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkinternetconnectivity())
                    startActivityForResult(new Intent(MainActivity.this, Register.class),0);

                else
                    Toast.makeText(MainActivity.this,R.string.str_error_nointernetconnection,Toast.LENGTH_LONG).show();
            }
        });
    }

    private void changelang(String lang){
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        this.setContentView(R.layout.activity_main);
    }

    private boolean ifEmpty(String str , String text){
        if(TextUtils.isEmpty(str)){
            Toast.makeText(MainActivity.this,text,Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    private void LangButtonTextChange(String languageToLoad){
        if(languageToLoad.equals("he")) Lang.setText(R.string.hebrew);
        else if(languageToLoad.equals("en")) Lang.setText(R.string.english);
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {

        Log.e("-------------------", acct.getGrantedScopes()+"  --\n-"+acct.getIdToken());
        // G+
        Log.e("--------------", "---------------------: "+ mGoogleApiClient.hasConnectedApi(Plus.API));
        if(mGoogleApiClient.hasConnectedApi(Plus.API)){
            Person person = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
//            if(person!=null) {
                Log.e("-------------------", "--------------------------------");
            Log.e("-------------------", "Display Name: " + person.getDisplayName());
//                if (person.hasGender())
                    Log.e("-------------------", "Gender: " + person.getGender());
            Log.e("-------------------", "AboutMe: " + person.getAboutMe());
//                if (person.hasBirthday())
                    Log.e("-------------------", "Birthday: " + person.getBirthday());
                    Log.e("-------------------", "Age range: " + person.getAgeRange());
            Log.e("-------------------", "Current Location: " + person.getCurrentLocation());
            Log.e("-------------------", "Language: " + person.getLanguage());
//            }
        }

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        firebaseauth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            signedIn(acct.getGivenName(),acct.getFamilyName(),acct.getPhotoUrl().toString(),"","","","yes");
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("", "signInWithCredential:failure", task.getException());
                            Toast.makeText(MainActivity.this,"gett Fuckedddddd",Toast.LENGTH_LONG).show();

                        }

                        // ...
                    }
                });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
           if(R.id.login_button_facebook==id)
        {
            Log.e("----------------", "facebook:onSuccess:ksjkdhkashkfjwekd-----9");
            Facebook.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Log.e("----------------", "facebook:onSuccess:ksjkdhkashkfjwekd-----1");
                    setFacebookData(loginResult);
                }

                @Override
                public void onCancel() {
                    Log.e("----------------", "facebook:onSuccess:ksjkdhkashkfjwekd-----2");
                    // ...
                }

                @Override
                public void onError(FacebookException error) {
                    Log.e("----------------", "facebook:onSuccess:ksjkdhkashkfjwekd-----3");
                    // ...
                }
            });
        }

         else if(R.id.google_main1==id)
        {
            Log.e( "yooo","onClick:------------------ 1  " );
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, 101);
            Log.e( "yooo","onClick:------------------ 2  " );
        }
    }

    private void handleFacebookAccessToken(AccessToken token, final String [] strs) {
        Log.d("-----", "handleFacebookAccessToken:" + token);

        Log.e("----------------", "facebook:onSuccess:ksjkdhkashkfjwekd-----4");

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        firebaseauth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.e("----------------", "facebook:onSuccess:ksjkdhkashkfjwekd-----5");
                            FirebaseUser user = firebaseauth.getCurrentUser();
                            String photoURL = user.getPhotoUrl().toString();
                            String PhoneNumber = "";

                            Log.e("Login------in"+ "FirstName", strs[1]+"");
                            Log.e("Login------in" + "LastName", strs[2]+"");
                            Log.e("Login------in" + "Gender", strs[3]+"");
                            Log.e("Login------in" + "Birth day", strs[4]+"");

                           String firstName =     strs[1];
                            String lastName  =     strs[2];
                            String gender =       strs[3];
                            String birthday  =     strs[4];


                            signedIn(firstName,lastName,photoURL,PhoneNumber,birthday,gender,"yes");

                            Log.e("----------------", "facebook:onSuccess:ksjkdhkashkfjwekd-----6");
                            //save data or tree lay out temp
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("-------", "signInWithCredential:failure - " + task.getException().getLocalizedMessage());
                            if(task.getException().getLocalizedMessage().equals(FacebookEmailAlreadyExistsError))
                                middleToast(R.string.FacebookEmailAlreadyExistsError);
                            LoginManager.getInstance().logOut();
                            Log.e("----------------", "facebook:onSuccess:ksjkdhkashkfjwekd-----7");
                        }

                        // ...
                    }
                });
    }
    private void middleToast(int id){
        Toast toast=Toast.makeText(this,id,Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
    private void signedIn(final String firstname, final String lastname , final String photoURL, final String phone, final String birthday, final String gender, final String yesOrNo) {
        FirebaseUser user = firebaseauth.getCurrentUser();
        temp = firebaseauth.getCurrentUser();
        boolean provider = false;
        for (UserInfo user1: user.getProviderData()) {

            if (user1.getProviderId().equals("facebook.com") || user1.getProviderId().equals("google.com")) {
                //For linked facebook account
             provider = true;
            }

        }
        if (temp.isEmailVerified() || (yesOrNo!=null&&yesOrNo.equals("yes")) || provider) {
        progressbar.setMessage(getResources().getString(R.string.str_error_loggingin) + "");
        progressbar.show();
        // Sign in success, update UI with the signed-in user's information
        Log.d("", "signInWithCredential:success");

//                                dataBaseUser.child("1").setValue(user);
//                                dataBaseUser.child("2").setValue(user);
            myHandler = new FirebaseHandler(null, MainActivity.this);
            dataBaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    boolean flag = true;
                    for (DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                        try {
                            String email = usersnapshot.child("email").getValue(String.class);
                            if (email.equals(temp.getEmail())) {
                                //open Tree
                                Intent Tree = new Intent(MainActivity.this, TreeLayoutTemp.class);
                                Tree.putExtra("user", usersnapshot.child("id").getValue(String.class));
                                editor.putInt(ifloggedin, 1);
                                editor.putString(Currentuser, usersnapshot.child("id").getValue(String.class));
                                editor.commit();
                                MainActivity.this.startActivity(Tree);
                                MainActivity.this.finish();
                                flag = false;
                            }
                        } catch (Exception e) {
                        }
                    }
                    if (flag) {
                        Intent i = new Intent(MainActivity.this, SaveData.class);
                        i.putExtra("google_facebook_FN",firstname);
                        i.putExtra("google_facebook_LN",lastname);
                        i.putExtra("google_facebook_PU",photoURL);
                        i.putExtra("google_facebook_PH",phone);
                        i.putExtra("google_facebook_BD",birthday);
                        i.putExtra("google_facebook_GN",gender);
                        i.putExtra("google_facebook",yesOrNo);


                        MainActivity.this.startActivity(i);
                    }
                    progressbar.dismiss();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {
            Toast.makeText(MainActivity.this, R.string.str_error_emailnotverified, Toast.LENGTH_SHORT).show();
            progressbar.dismiss();
        }
    }


    private void setFacebookData(final LoginResult loginResult)
    {
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        // Application code
                        try {
                            Log.i("Response",response.toString());

                            String email = response.getJSONObject().getString("email");
                            String firstName  = response.getJSONObject().getString("first_name");
                            String lastName = response.getJSONObject().getString("last_name");
                            String gender = response.getJSONObject().getString("gender");
                            String birthday =  object.getString("birthday");
                            handleFacebookAccessToken(loginResult.getAccessToken(),new String[]{email,firstName,lastName,gender,birthday});


                            Profile profile = Profile.getCurrentProfile();
//                            String id = profile.getId();
//                            String link = profile.getLinkUri().toString();
//                            Log.i("Link",link);
                            if (Profile.getCurrentProfile()!=null)
                            {
                                Log.i("Login", "ProfilePic" + Profile.getCurrentProfile().getProfilePictureUri(200, 200));
                            }


//                            Log.e("Login" + "Email", email);        
//                            Log.e("Login"+ "FirstName", firstName); 
//                            Log.e("Login" + "LastName", lastName);  
//                            Log.e("Login" + "Gender", gender);      
//                            Log.e("Login" + "Birth day", birthday); 

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("Login" + "Gender", "erorrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr: "+e.getLocalizedMessage());

                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,first_name,last_name,gender,birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }

   }

