package com.E.B.SHORASHIM.User_classes;

/**
 * Created by Androwsa on 8/21/17.
 */

public class User_toUpload extends Main_User{


    private String photolink="";

    public User_toUpload(String id, String fullname, String phone, String dateofbirth, String description, String activated, String email, String dateofdeath, String did) {
        super(id, fullname, phone, dateofbirth, description, activated, email, dateofdeath, did);
    }

    public User_toUpload(String id, String fullname, String phone, String dateofbirth, String description, String activated, String email) {
        super(id, fullname, phone, dateofbirth, description, activated, email);
    }

    public String getPhotolink() {
        return photolink;
    }

    public void setPhotolink(String photolink) {
        this.photolink = photolink;
    }

}
