package com.E.B.SHORASHIM;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.E.A.SHORASHIM.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Register extends AppCompatActivity {


    EditText email,pass;

    Button register;

    private ProgressDialog progressbar;

    private FirebaseAuth.AuthStateListener mAuthListener;

    private FirebaseAuth firebaseAuth;

    FirebaseUser temp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        progressbar = new ProgressDialog(this);

        firebaseAuth = FirebaseAuth.getInstance();

        email = (EditText) findViewById(R.id.RGemail);

        pass = (EditText) findViewById(R.id.RGpassword);

        register = (Button) findViewById(R.id.RGregister);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String EMAIL = email.getText().toString().trim();
                final String Pass = pass.getText().toString().trim();
                
                if (are_details_empty(EMAIL, Pass)) {
                    return;
                }
                progressbar.setMessage(getResources().getString(R.string.str_error_registering));
                progressbar.show();

                firebaseAuth.createUserWithEmailAndPassword(EMAIL,Pass).addOnCompleteListener(Register.this,new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressbar.dismiss();
                        progressbar.setMessage(getResources().getString(R.string.str_error_sendemailtovalidate));
                        progressbar.show();
                        if(task.isSuccessful()){
                            temp= firebaseAuth.getCurrentUser();
                            temp.sendEmailVerification();

                            Toast.makeText(Register.this,R.string.str_error_loginafteryouvalidate, Toast.LENGTH_LONG).show();
                            Intent backed = new Intent();
                            backed.putExtra("email",EMAIL);
                            backed.putExtra("pass",Pass);

                            firebaseAuth.signOut();
                            setResult(RESULT_OK,backed);
                            finish();

                        }
                        else{
                            Toast.makeText(Register.this,R.string.str_error_emailalreadyinuseorinvalid, Toast.LENGTH_LONG).show();
                            progressbar.dismiss();
                        }
                    }
                });

            }
        });



    }
    private boolean handle_empty_text_edit(String text)
    {
        if(TextUtils.isEmpty(text)== false){ // TODO extract a method if something is empty print a custom message
            return false;
    }
    return true;
}
    private boolean are_details_empty(String email, String  pass)
    {
        boolean empty_details = handle_empty_text_edit(email)|| handle_empty_text_edit(pass);
        if (empty_details)
        {
            Toast.makeText(Register.this, "Inavlid Email or Password, please check", Toast.LENGTH_SHORT).show();
        }
        return empty_details;
    }

    private boolean ifEmpty(String str , String text){
        if(TextUtils.isEmpty(str)){
            Toast.makeText(Register.this,text,Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }
}
