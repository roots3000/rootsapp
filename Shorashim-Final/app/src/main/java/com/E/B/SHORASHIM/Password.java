package com.E.B.SHORASHIM;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.E.A.SHORASHIM.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class Password extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;

    EditText email;

    private ProgressDialog progressbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);

        firebaseAuth = FirebaseAuth.getInstance();

        email= (EditText) findViewById(R.id.PFemail);

        progressbar = new ProgressDialog(this);

        Button Send = (Button) findViewById(R.id.PFsend);
        Send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressbar.setMessage(getResources().getString(R.string.str_error_sendingemail));
                progressbar.show();
                firebaseAuth.sendPasswordResetEmail(email.getText().toString().trim()).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            toast(getResources().getString(R.string.str_error_emailsent));
                            finish();
                        }
                        else {
                            toast(getResources().getString(R.string.str_error_invalidemail));
                        }

                    }
                });
            }
        });
    }

    private void toast(String text){
        Toast.makeText(Password.this, text, Toast.LENGTH_SHORT).show();
        progressbar.dismiss();
    }
}
