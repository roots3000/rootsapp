package com.E.B.SHORASHIM;

import android.app.ProgressDialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.E.A.SHORASHIM.R;
import com.E.B.SHORASHIM.User_classes.User_toUpload;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.Serializable;

public class LocationSaver extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private LocationManager locationManager;
    private LocationListener listener;

    FirebaseHandler fbh;

    User_toUpload person;

    GoogleApiClient gg;
    LocationRequest locationrequest;
    FusedLocationProviderApi locationProvider = LocationServices.FusedLocationApi;

    int i =0;

    Double myLatitude,myLongitude;

    ProgressDialog progressbar;

    Button save;

    String main,current;

    //BRdoooo tb3asoosh hon abel mtestsherone cus took a while to set up and understand


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_saver);
        setPopUp();

        main = getIntent().getStringExtra("Main");
        current = getIntent().getStringExtra("Current");

        progressbar= new ProgressDialog(this);

        fbh = new FirebaseHandler(null,LocationSaver.this);

        person = (User_toUpload) getIntent().getSerializableExtra("user");

        gg=new GoogleApiClient.Builder(this) // move the new to method that receives LocationSaver as input, or as a LocationSaver method
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        locationrequest = newLocation();


        save = (Button) findViewById(R.id.savelocationn);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(main.equals(current)) {
                    LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

                    if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        progressbar.setMessage(getResources().getString(R.string.str_error_loading));
                        progressbar.show();
                        gg.connect();
                    } else {
                        showGPSDisabledAlertToUser();
                    }
                }
                else{
                    // TODO: move the toast code block into method - DONE in this class only
                    toastC(getResources().getString(R.string.noPermission));
                }
            }

        });

        Button gooto = (Button) findViewById(R.id.gotolocationn);
        if((person.getLong()==null&&person.getLat()==null)||(person.getLong().equals(" ")&&person.getLat().equals(" "))) {
            gooto.setBackgroundResource(R.drawable.roundedsilver);
            gooto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toastC(getResources().getString(R.string.str_error_savelocationfirst));
                }
            });
        }
        else {
            gooto.setBackgroundResource(R.drawable.drawable_test);
            gooto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OpenNav(person.getLat(), person.getLong());
                }
            });
        }

    }

    private LocationRequest newLocation() {
        LocationRequest l = new LocationRequest();
        l.setInterval(1000);
        l.setFastestInterval(1000);
        l.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        return l;
    }

    private void showGPSDisabledAlertToUser(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(LocationSaver.this.getResources().getString(R.string.turnLocationOn))
                .setCancelable(false)
                .setPositiveButton(LocationSaver.this.getResources().getString(R.string.GoTurnLocation),
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton(LocationSaver.this.getResources().getString(R.string.cancle),
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void requestLocationUpdates() {
        if (Build.VERSION.SDK_INT >= 23) {
            String[] perms = {"android.permission.ACCESS_FINE_LOCATION"};
            int permsRequestCode = 200;
            if (!((LocationSaver.this).checkSelfPermission(perms[0]) == PackageManager.PERMISSION_GRANTED)) {
                LocationSaver.this.requestPermissions(perms, permsRequestCode);
            } else {
                LocationServices.FusedLocationApi.requestLocationUpdates(gg, locationrequest, this);
            }
        }
        else{
            LocationServices.FusedLocationApi.requestLocationUpdates(gg, locationrequest, this);
        }

    }

    private void sendbackLatandLong(){
        person.setLong(myLongitude+"");
        person.setLat(myLatitude+"");
        Intent backed = new Intent();
        backed.putExtra("backeduser",(Serializable) person);
        setResult(RESULT_FIRST_USER,backed);
        fbh.EditUserForLocation(person, LocationSaver.this,progressbar);

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        requestLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        i++;

        myLatitude = location.getLatitude();
        myLongitude = location.getLongitude();

        Log.e("Location     :", "Long = " + myLongitude + " , Lat = " + myLatitude);
        if(i==2){
            LocationServices.FusedLocationApi.removeLocationUpdates(gg,this);
            gg.disconnect();
            sendbackLatandLong();
        }
        Log.e("Location     :", "I = " + i );
    }

    @Override
    protected void onStart() {
        super.onStart();
//        gg.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(gg.isConnected()){
            requestLocationUpdates();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        gg.disconnect();
    }



    private void setPopUp() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .8), (int) (1050));

    }


    //---------------------OPENING NAVIGATION-------------------------------//
    public void OpenNav(String latitude, String longtitude) {
        String uri = "geo: " + latitude + " , " + longtitude;
        startActivity(new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse(uri)));
    }
    //---------------------OPENING NAVIGATION-------------------------------//


    private void toastC(String text){
        Toast toast= Toast.makeText(LocationSaver.this,text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
