package com.E.B.SHORASHIM;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.icu.util.Calendar;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.E.A.SHORASHIM.R;
import com.E.B.SHORASHIM.JewishCalendar.CalendarDate;
import com.E.B.SHORASHIM.JewishCalendar.CalendarImpl;
import com.E.B.SHORASHIM.JewishCalendar.jewishcal;
import com.E.B.SHORASHIM.User_classes.User;
import com.E.B.SHORASHIM.User_classes.User_toUpload;
import com.bumptech.glide.Glide;
import com.bumptech.glide.util.Util;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileView extends  YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener{

    private LinearLayout profilelayout;

    private User person;

    private String Phone;
    private Spinner kedomet;

    private User_toUpload temp;

    private LinearLayout.LayoutParams lp;

    int height,width;

    private int MP = ViewGroup.LayoutParams.MATCH_PARENT ,WC = ViewGroup.LayoutParams.WRAP_CONTENT , C = Gravity.CENTER , TOP = 48;

    private RadioGroup genderLayout;
    private LinearLayout ifDeceasedLayout;

    private User father,mother;

    private EditText fullname,lastname, phone, day, month, year, about,youtubeidvid;

    private EditText fDday, fDmonth, fDyear;
    private String FDday, FDmonth, FDyear;

    private RadioButton male,female;

    private Button save;


    private CircleImageView profilepicture;


    private Uri thepicture;

    private CheckBox isDeceased,withOutPhone;

    private DatabaseReference dataBaseUser;

    private boolean ifpicturepicked = false;
    private StorageReference mStorage;


    private LinearLayout isdeceasedlayout;

    private ProgressDialog progressbar;

    private byte[] biti;

    private FirebaseHandler fbh;

    private String currentuser, who, Gender,MainUser,currentspouse;
    private String VIDEOID = "";

    countryCodes codes = new countryCodes();



    private   String Fullname ;
    private   String Lastname ;
    private   String Yotube   ;
    private   String About    ;
    private   String Day1     ;
    private   String Month1   ;
    private   String Year1    ;
    private   int Day    ;
    private   int Month   ;
    private   int Year    ;



    //---------------------OPENING NAVIGATION-------------------------------//
    public void OpenNav(String latitude, String longtitude) {
        String uri = "geo: " + latitude + " , " + longtitude;
        startActivity(new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse(uri)));
    }
    //---------------------OPENING NAVIGATION-------------------------------//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_view);
        setPopUp();

        dataBaseUser = FirebaseDatabase.getInstance().getReference("userprofile");

        profilelayout = (LinearLayout) findViewById(R.id.PVlayout);

        progressbar = new ProgressDialog(this);

        mStorage = FirebaseStorage.getInstance().getReference();

        //all the getIntent....... gets data ive sent from the treeLayOut to know why the profile view was called


        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;


        //getSerializableExtra it retrieves an Object
        mother = (User) getIntent().getSerializableExtra("mother1");
        father = (User) getIntent().getSerializableExtra("father1");

        person = (User) getIntent().getSerializableExtra("user");

        //hai el split 3la keef m2na ktbt el code lal youtube url
        try {
            VIDEOID= person.getYoutubeid().split("/")[3];
        }
        catch (Exception e){}

        currentuser = getIntent().getExtras().getString("currentUser");
        who = getIntent().getExtras().getString("who"); // get_intent.getextra.getstring refactoring

        MainUser = getIntent().getExtras().getString("mainuser");

        if(who.equals("children")){
            currentspouse = getIntent().getExtras().getString("currentspouse");
        }

        try {
            Gender = getIntent().getExtras().getString("currentGender");
        } catch (Exception e) {
        }

        biti = getIntent().getExtras().getByteArray("phototo");

        fbh = new FirebaseHandler(null,ProfileView.this);
        if (person != null) {
            buildView();
        } else {
            buildViewForNull();
        }


    }


    //This activity build view for viewing existing user
    private void buildView() {
        if (currentuser.equals(MainUser)){
            if ((person.getId().equals(MainUser) || person.getActivated().equals("inactive"))&& checkinternetconnectivity()) {
                final Button Edit = new Button(this);
                Edit.setText(getResources().getString(R.string.ste_edit));
                setParams(width/6,width/8,TOP,0, 15, 0, 0, Gravity.RIGHT);
                Edit.setLayoutParams(lp);


                //An Edit button to enable editing for the profile we chose IFFFF the above is accepted-----------Start
                Edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!checkinternetconnectivity()) {
                            Toast t = Toast.makeText(ProfileView.this, R.string.str_error_nointernetconnection, Toast.LENGTH_SHORT);
                            t.setGravity(Gravity.CENTER, 0, 0);
                            t.show();
                        }
                        else{

                            tzbeterView();

                            LinearLayout saveDLO = (LinearLayout) findViewById(R.id.SaveData_LayOut);
                            LinearLayout saveBackLO= (LinearLayout) findViewById(R.id.SaveData_SaveBack);
                            saveBackLO.setVisibility(View.GONE);

                            FDday = "";
                            FDmonth = "";
                            FDyear = "";


                            if (person.getGender().equals("male")) {
                                male.setChecked(true);

                            } else {
                                female.setChecked(true);
                            }


                            final ImageButton Vsave = new ImageButton(ProfileView.this);
                            setParams(width/6,width/6,TOP,0, 15, 0, 0);
                            Vsave.setBackgroundResource(R.drawable.checked_1);
                            lp.gravity = Gravity.RIGHT;
                            Vsave.setLayoutParams(lp);
                            Vsave.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    progressbar.setMessage(getResources().getString(R.string.str_error_loading));
                                    if(!saving_Vars(person.getActivated(),person.getId())) return;

                                    final String gender;
                                    if (male.isChecked()) {
                                        gender = "male";
                                    } else if(female.isChecked())
                                        gender = "female";
                                    else gender = "";

                                    if (TextUtils.isEmpty(gender)) {
                                        Toast.makeText(ProfileView.this, R.string.str_error_chooseGender, Toast.LENGTH_SHORT).show();
                                        return;
                                    }

//
                                    temp.setEmail(person.getEmail());

                                    if (isDeceased.isChecked()) {
                                        temp.setLong(person.getLong());
                                        temp.setLat(person.getLat());
                                    } else {
                                        temp.setLong(" ");
                                        temp.setLat(" ");
                                    }

                                    temp.setGender(gender);



                                    StorageReference storageref = mStorage.child("photos").child(person.getId());

                                    progressbar.show();
                                    if (!ifpicturepicked) {
                                        Vsave.setClickable(false);
                                        fbh.EditUser(temp, ProfileView.this, progressbar);
                                    } else {
                                        save_users(temp,Edit,gender,"editString");
                                    }
                                }
                            });
                            saveDLO.addView(Vsave,0);


                            Glide.with(ProfileView.this)
                                    .load(biti)
                                    .asBitmap()
                                    .into(profilepicture);



                            fullname.setText(person.getFullname().split(" ")[0]);

                            lastname.setText(person.getFullname().split(" ")[1]);


                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(ProfileView.this, android.R.layout.simple_spinner_dropdown_item, codes.countryCodes);
                            kedomet.setAdapter(adapter);


                            try {
                                phone.setText(person.getPhone().substring(4));
                            } catch (Exception e) {

                            }



                            day.setText(person.getDateofbirth().split("/")[0]);


                            month.setText(person.getDateofbirth().split("/")[1]);


                            year.setText(person.getDateofbirth().split("/")[2]);


                            if (!person.getDescription().equals(" "))
                                about.setText(person.getDescription());

                            if (!person.getYoutubeid().equals(""))
                                youtubeidvid.setText(person.getYoutubeid());


                            if (!person.getDateofdeath().equals("")) {
                                isDeceased.performClick();
                                fDday.setText(person.getDateofdeath().split("/")[0]);

                                fDmonth.setText(person.getDateofdeath().split("/")[1]);


                                fDyear.setText(person.getDateofdeath().split("/")[2]);
                            }


                        }
                    }
                });
                profilelayout.addView(Edit);
            }
        }

        //An Edit button to enable editing for the profile we chose IFFFF the above is accepted-----------END




        //Sets the Profile view with all the data

        setParams(520,520,TOP,0, 0, 0, 0,C);
        final CircleImageView kokos = new CircleImageView(ProfileView.this);
        kokos.setLayoutParams(lp);
        profilepicture =kokos;
        kokos.setImageResource(R.drawable.male);
        kokos.setBorderWidth(width/100);
        kokos.setBorderColor(Color.parseColor("#FFFFFF"));
        kokos.getLayoutParams().width= width/3;
        kokos.getLayoutParams().height= width/3;
        profilelayout.addView(kokos);

        Glide.with(ProfileView.this)
                .load(biti)
                .asBitmap()
                .into(kokos);


        TextView fullname = new TextView(this);
        setParams(MP,WC,TOP,0, 15, 0, 0,C);
        fullname.setGravity(Gravity.CENTER);
        fullname.setTextSize(20);
        fullname.setText(person.getFullname());
        fullname.setLayoutParams(lp);
        profilelayout.addView(fullname);

        TextView date = new TextView(this);
        int day = Integer.parseInt(person.getDateofbirth().split("/")[0]);
        int month = Integer.parseInt(person.getDateofbirth().split("/")[1]);
        int year = Integer.parseInt(person.getDateofbirth().split("/")[2]);
        Log.e("buildView: ", day + "/"+month+"/"+year);
        CalendarDate gregoriandate= new CalendarDate(day,month,year);
        CalendarImpl imp = new CalendarImpl();
        CalendarDate jewishCal = gregorian2Jewish(gregoriandate,imp);
        jewishcal jewiishhelper = new jewishcal(this);
        setParams(MP,WC,TOP,0, 15, 0, 0,C);
        date.setGravity(Gravity.CENTER);
        date.setTextSize(15);
        date.setText(person.getDateofbirth()+"\n"+jewiishhelper.getJewishdayinHebrew(jewishCal.getDay())+"   "+jewiishhelper.getJewishMonthName(jewishCal.getMonth(),jewishCal.getYear())+"   "+jewiishhelper.getJewishYearinHebrew(jewishCal.getYear()));
        date.setLayoutParams(lp);
        profilelayout.addView(date);


        // sameday BUTTONS sets alarms to notify on birthday/date of death
        Button sameday = new Button(ProfileView.this);
        setParams(WC,WC,TOP,0, 0, 0, 0,C);
        sameday.setLayoutParams(lp);  //TODO: put those three lines in one method of samedat
        sameday.setBackgroundResource(R.drawable.drawable_test);
        sameday.setText(R.string.birthday_notfication);
        final int finalMonth = month;
        final int finalDay = day;
        final int finalYear = year;
        sameday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random r = new Random();

                // abel mteb3aso eshe hon estsherone

                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
                notificationIntent.addCategory("android.intent.category.DEFAULT");

                notificationIntent.putExtra("fullname",person.getFullname()); // TODO: those 4 linse extracted into method
                notificationIntent.putExtra("or","dateofbirth");
                notificationIntent.putExtra("date",finalDay+"/"+finalMonth);
                notificationIntent.putExtra("id",MainUser);


                int uniquenum= (((((finalMonth+finalDay+finalYear)*23)/4)+finalDay+finalMonth+finalYear)*29)/3+(person.getFullname().length()*18)+69;
                PendingIntent broadcast = PendingIntent.getBroadcast(ProfileView.this,uniquenum, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.MONTH,finalMonth-1); // TODO: the set lines extracted into method
                cal.set(Calendar.DAY_OF_MONTH,finalDay);
                cal.set(Calendar.HOUR_OF_DAY,8);
                cal.set(Calendar.MINUTE,0);
                cal.set(Calendar.SECOND,0);
//                    cal.add(Calendar.DAY_OF_YEAR, 362);
//                    cal.add(Calendar.DAY_OF_YEAR, 358);
//                    cal.add(Calendar.DAY_OF_YEAR, 365);
//                    cal.set(Calendar.HOUR_OF_DAY,8);

//                    cal.add(Calendar.SECOND, 60);

                long pika = cal.getTimeInMillis();

                Long pikachu = Calendar.getInstance().getTimeInMillis();

                AlarmManager alarmManager3 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                Intent notificationIntent3 = new Intent("android.media.action.DISPLAY_NOTIFICATION");
                notificationIntent3.addCategory("android.intent.category.DEFAULT");

                notificationIntent3.putExtra("fullname",person.getFullname());
                if (!person.getDateofdeath().equals("")) {
                    notificationIntent3.putExtra("or", "dateofdeath");
                }
                else {
                    notificationIntent3.putExtra("or", "dateofbirth");
                }
                notificationIntent3.putExtra("date",finalDay+"/"+finalMonth);
                notificationIntent3.putExtra("id",MainUser);

                PendingIntent broadcast3 = PendingIntent.getBroadcast(ProfileView.this,uniquenum+1, notificationIntent3, PendingIntent.FLAG_UPDATE_CURRENT);



                Calendar zoborte = Calendar.getInstance();
                zoborte.add(Calendar.SECOND, 60);
                Calendar zoborte2 = Calendar.getInstance();
                zoborte2.add(Calendar.SECOND, 15);
                Calendar zoborte3 = Calendar.getInstance();
                zoborte3.add(Calendar.SECOND, 30);

                if(pika < pikachu){ //TODO: no pikachu
                    cal.set(Calendar.YEAR,cal.get(Calendar.YEAR)+1);
                }

                long pikas = cal.getTimeInMillis();

                int day = cal.get(Calendar.DAY_OF_MONTH); // TODO: day month year should extracted into date class https://developer.android.com/reference/java/sql/Date
                int month = cal.get(Calendar.MONTH);
                int year = cal.get(Calendar.YEAR);
                Log.e("check","pika   "+day+"/"+month+"/"+year);

                Calendar threedays = Calendar.getInstance();
                threedays.setTimeInMillis(pikas-259200000);
                day = threedays.get(Calendar.DAY_OF_MONTH); // TODO: those gets extacted into a seperate method
                month = threedays.get(Calendar.MONTH);
                year = threedays.get(Calendar.YEAR);
                Log.e("check","pika   "+day+"/"+month+"/"+year);

                Calendar oneweek = Calendar.getInstance();
                oneweek.setTimeInMillis(pikas-604800000);
                day = oneweek.get(Calendar.DAY_OF_MONTH);
                month = oneweek.get(Calendar.MONTH);
                year = oneweek.get(Calendar.YEAR);
                Log.e("check","pika   "+day+"/"+month+"/"+year);

                alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);
                alarmManager3.setExact(AlarmManager.RTC_WAKEUP, threedays.getTimeInMillis(), broadcast3);

                Toast.makeText(ProfileView.this,R.string.savednotification,Toast.LENGTH_SHORT).show();

            }
        });
        profilelayout.addView(sameday);
        Button SendSMS = new Button(ProfileView.this);
        setParams(MP,WC,TOP,0, 50, 0, 0);
        SendSMS.setBackgroundResource(R.drawable.placeholder);
        if(person.getPhone().equals("")|| person.getActivated().equals("active")) {
            SendSMS.setText(getResources().getString(R.string.str_invite)); //TODO: setText and setbackround resource should be extracted
            SendSMS.setBackgroundResource(R.drawable.roundedsilver);
        }
        else {
            SendSMS.setBackgroundResource(R.drawable.roundedb2);
            SendSMS.setText(getResources().getString(R.string.str_invite));
        }
        SendSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (person.getPhone().equals("")){
                    Toast toast=Toast.makeText(ProfileView.this,R.string.str_errorcannotinvitebecauseuserisnotconnectedwithaphone,Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    return;
                }
                try {


                    String toNumber = person.getPhone();// Replace with mobile phone number without +Sign or leading zeros.
                    Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                    sendIntent.setData(Uri.parse("http://api.whatsapp.com/send?phone="+toNumber +"&text="+
                            getResources().getString(R.string.str_greet)+" " + person.getFullname()+"," +getResources().getString(R.string.str_sms)));
            sendIntent.setPackage("com.whatsapp");
            startActivity(sendIntent);
        }
        catch (Exception e){
            e.printStackTrace();
            Toast.makeText(ProfileView.this,"it may be you dont have whats app",Toast.LENGTH_LONG).show();

        }
                }
            });
        SendSMS.setLayoutParams(lp);

        if (person.getDateofdeath().equals("")) {
        } else {
            TextView deathdate = new TextView(this);
            day = Integer.parseInt(person.getDateofdeath().split("/")[0]);
            month = Integer.parseInt(person.getDateofdeath().split("/")[1]);
            year = Integer.parseInt(person.getDateofdeath().split("/")[2]);
            Log.e("buildView: ", day + "/"+month+"/"+year);
            gregoriandate= new CalendarDate(day,month,year);
            imp = new CalendarImpl();
            jewishCal = gregorian2Jewish(gregoriandate,imp);
            jewiishhelper = new jewishcal(this);
            setParams(MP,WC,TOP,0, 15, 0, 0,C);
            deathdate.setGravity(Gravity.CENTER); //TODO: sets into method
            deathdate.setTextSize(20);
            deathdate.setText(getResources().getString(R.string.str_dateofdeath) + "\n" + person.getDateofdeath()+"\n"+jewiishhelper.getJewishdayinHebrew(jewishCal.getDay())+"   "+jewiishhelper.getJewishMonthName(jewishCal.getMonth(),jewishCal.getYear())+"   "+jewiishhelper.getJewishYearinHebrew(jewishCal.getYear()));
            deathdate.setLayoutParams(lp);
            profilelayout.addView(deathdate);

            LinearLayout ll = new LinearLayout(this);
            setParams(WC,WC,TOP,0, 30, 0, 0);
            ll.setOrientation(LinearLayout.HORIZONTAL);
            ll.setLayoutParams(lp);

            TextView temp= new TextView(ProfileView.this);
            temp.setText(getResources().getString(R.string.str_savelocationforgrave));
            temp.setTextSize(20);
            setParams(MP,WC,TOP,0, 200, 0, 0);
            temp.setGravity(Gravity.CENTER);
            temp.setLayoutParams(lp);
            profilelayout.addView(temp);

            ImageButton getlocation = new ImageButton(ProfileView.this);
            setParams(220,220,TOP,0, 0, 0, 0,C);
            getlocation.setBackgroundResource(R.drawable.placeholder);
            getlocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //erios is an temp User_toUpload sex blbne wz3tr
                        Intent i = new Intent(ProfileView.this,LocationSaver.class);
                        User_toUpload erios = new User_toUpload(person.getId(),person.getFullname(),person.getPhone(),person.getDateofbirth(),person.getDescription(),
                                person.getActivated(),person.getEmail(),person.getDateofdeath(),person.getDid());
                        erios.setGender(person.getGender()); //TODO: move sets to seperate methods
                        erios.setPhotolink("no");
                        erios.setYoutubeid(person.getYoutubeid());
                        erios.setLat(person.getLat());
                        erios.setLong(person.getLong());

                        i.putExtra("user",(Serializable)erios);
                        i.putExtra("Main",MainUser);
                        i.putExtra("Current",currentuser);
                        ProfileView.this.startActivityForResult(i,0);


                }
            });
            getlocation.setLayoutParams(lp);
            profilelayout.addView(getlocation);


        }

        profilelayout.addView(SendSMS);

        TextView te2or = new TextView(this);
        setParams(MP,WC,TOP,0, 15, 0, 0,C); //TODO: move sets to seperate methods
        te2or.setGravity(Gravity.CENTER);
        te2or.setTextSize(20);
        te2or.setText(person.getDescription());
        te2or.setLayoutParams(lp);
        profilelayout.addView(te2or);

        if(!VIDEOID.equals("")) {
            setParams(WC,WC,TOP,0, 0, 0, 0);
            YouTubePlayerView youTubePlayerView = new YouTubePlayerView(ProfileView.this);
            youTubePlayerView.setLayoutParams(lp);
            youTubePlayerView.initialize("kokos", ProfileView.this);

            profilelayout.addView(youTubePlayerView);
        }

    }


    //This activity build a view for adding a new user
    private void buildViewForNull() {
        tzbeterView();


        // Sets a Save User LayOut for siblings and children--------------Start
        if (who.equals("sibling") || who.equals("children")) {
            male.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!ifpicturepicked)
                        profilepicture.setImageResource(R.drawable.male);

                }
            });
            female.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!ifpicturepicked)
                        profilepicture.setImageResource(R.drawable.female);
                }
            });

            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id = dataBaseUser.push().getKey();
                    if(!saving_Vars("inactive", id)) return ;

                    final String gender;
                    if (male.isChecked()) {
                        gender = "male";
                    } else if(female.isChecked())
                        gender = "female";
                    else gender = "";

                    if (TextUtils.isEmpty(gender)) {
                        Toast.makeText(ProfileView.this, R.string.str_error_chooseGender, Toast.LENGTH_SHORT).show();
                        return;
                    }


                    progressbar.show();
                    if(!ifpicturepicked){
                        if(gender.equals("male")){
                            String uri="android.resource://"+getPackageName()+"/drawable/male";
                                    if (who.equals("sibling")) {
                                        save_usersu(temp,save,gender,uri,"siblingsString");
                                    } else if (who.equals("children")) {
                                        save_usersu(temp,save,gender,uri,"childrenString");
                                    }

                        }
                        else{
                            String uri="android.resource://"+getPackageName()+"/drawable/female";


                                    if (who.equals("sibling")) {
                                        save_usersu(temp,save,gender,uri,"siblingsString");
                                    } else if (who.equals("children")) {
                                        save_usersu(temp,save,gender,uri,"childrenString");
                                    }



                        }
                    }
                    else{

                                if (who.equals("sibling")) {
                                    save_users(temp,save,gender,"siblingsString");
                                } else if (who.equals("children")) {
                                    save_users(temp,save,gender,"childrenString");
                                }
                            }

                    setResult(RESULT_OK);
                    save.setClickable(false);
                }
            });


        }
        // Sets a Save User LayOut for siblings and children--------------End




        // Sets a Save User LayOut for parents and spouses--------------Start

        else {
            genderLayout.setVisibility(View.GONE);

            if(who.equals("father"))
            {
                profilepicture.setImageResource(R.drawable.male);
            }
            else if(who.equals("mother")){
                profilepicture.setImageResource(R.drawable.female);
            }
            else if(who.equals("spouse")){
                if (Gender.equals("male")) {
                    profilepicture.setImageResource(R.drawable.female);

                } else {
                    profilepicture.setImageResource(R.drawable.male);
                }
            }



            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id = dataBaseUser.push().getKey();
                    if(!saving_Vars("inactive",id))return;
                    //Here it checkes if its the father we're adding or mother also checks if the spouse is male or female

                    progressbar.show();
                    if(!ifpicturepicked) {
                        if (who.equals("father")) {
                            save_usersu(temp,save,"male","android.resource://"+getPackageName()+"/drawable/male","parentsString");
                        } else if (who.equals("mother")) {
                            save_usersu(temp,save,"female","android.resource://"+getPackageName()+"/drawable/female","parentsString");
                        } else if (who.equals("spouse")) {
                            if (Gender.equals("male")) {
                                save_usersu(temp,save,"female","android.resource://"+getPackageName()+"/drawable/female","spouseString");

                            } else {
                                save_usersu(temp,save,"male","android.resource://"+getPackageName()+"/drawable/male","spouseString");
                            }
                        }
                    }
                    else{
                        if (who.equals("father")) {

                            save_users(temp,save,"male","parentsString");
                        } else if (who.equals("mother")) {
                            save_users(temp,save,"female","parentsString");
                        } else if (who.equals("spouse")) {
                            if (Gender.equals("male")) {
                                save_users(temp,save,"female","spouseString");
                            } else {
                                save_users(temp,save,"male","spouseString");
                            }
                        }
                    }
                }
            });
//
//            profilelayout.addView(savenewUser);
        }
            // Sets a Save User LayOut for parents and spouses--------------End

    }

    //sets the pop up view for this activity
    private void setPopUp() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .8), (int) (height * 0.8));

    }

    //After we select and image for profile the code to save and display is in this function
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            ifpicturepicked = true;
            Uri selectedImage = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
            } catch (IOException e) {
                e.printStackTrace();
            }

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);



            Glide.with(ProfileView.this)
                    .load(stream.toByteArray())
                    .asBitmap()
                    .into(profilepicture);
            thepicture = selectedImage;

        }
        if(resultCode == RESULT_FIRST_USER){
            User_toUpload kopop = (User_toUpload) data.getSerializableExtra("backeduser");
            person.setLat(kopop.getLat());
            person.setLong(kopop.getLong());
        }


    }
    //After we select and image for profile the code to save and display is in this function


    //asks permission to access location
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case 200:
                if(grantResults[0]==PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(ProfileView.this, R.string.str_error_permissiongranted, Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(ProfileView.this, R.string.str_error_cantsavelocationwithoutpermission, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    //when adding a new user and we push the deceased option for the user
    //this function adds all the edit texts needed to add a deceased user
    private void addDeadEditTexts(){
        LinearLayout ll = new LinearLayout(this);
        setParams(MP,WC,TOP,0, 14, 0, 0);
        ll.setLayoutParams(lp);

        fDday = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle)); //TODO: editText treatment should be moved to method
        fDday.setHint("*"+getResources().getString(R.string.str_day));
        fDday.setHintTextColor(Color.parseColor("#ffffff"));
        fDday.setInputType(InputType.TYPE_CLASS_NUMBER);
        setParams(MP,WC,1f,0, 0, 4, 0);
        fDday.setLayoutParams(lp);
        ll.addView(fDday);

        fDmonth = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
        fDmonth.setHint("*"+getResources().getString(R.string.str_month));
        fDmonth.setHintTextColor(Color.parseColor("#ffffff"));
        setParams(MP,WC,1f,0, 0,4, 0);
        fDmonth.setInputType(InputType.TYPE_CLASS_NUMBER);
        fDmonth.setLayoutParams(lp);
        ll.addView(fDmonth);

        fDyear = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
        setParams(MP,WC,1f,0, 0, 0, 0);
        fDyear.setHint("*"+getResources().getString(R.string.str_year));
        fDyear.setHintTextColor(Color.parseColor("#ffffff"));
        fDyear.setInputType(InputType.TYPE_CLASS_NUMBER);
        fDyear.setLayoutParams(lp);
        ll.addView(fDyear);

        isdeceasedlayout.addView(ll);
    }

    //when we are viewing a user profile and he is deceased this funciton adds
    //the texts to display information of his death
    private void addDeadEditTextsfordead(){
        LinearLayout ll = new LinearLayout(this);
        setParams(MP,WC,TOP,0, 14, 0, 0);
        ll.setLayoutParams(lp);

        fDday = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle)); //TODO: editText treatment should be moved to method
        fDday.setHint("*"+getResources().getString(R.string.str_day));
        fDday.setInputType(InputType.TYPE_CLASS_NUMBER);
        setParams(MP,WC,1f,0, 0, 4, 0);
        if(!person.getDateofdeath().equals(""))
            fDday.setText(person.getDateofdeath().split("/")[0]);
        fDday.setLayoutParams(lp);
        ll.addView(fDday);

        fDmonth = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
        fDmonth.setHint("*"+getResources().getString(R.string.str_month));
        setParams(MP,WC,1f,0, 0, 4, 0);
        fDmonth.setInputType(InputType.TYPE_CLASS_NUMBER);
        if(!person.getDateofdeath().equals(""))
            fDmonth.setText(person.getDateofdeath().split("/")[1]);
        fDmonth.setLayoutParams(lp);
        ll.addView(fDmonth);

        fDyear = new EditText(new ContextThemeWrapper(ProfileView.this, R.style.EditTextCustomStyle));
        setParams(MP,WC,1f,0, 0, 0, 0);
        fDyear.setHint("*"+getResources().getString(R.string.str_year));
        fDyear.setInputType(InputType.TYPE_CLASS_NUMBER);
        if(!person.getDateofdeath().equals(""))
            fDyear.setText(person.getDateofdeath().split("/")[2]);
        fDyear.setLayoutParams(lp);
        ll.addView(fDyear);

        isdeceasedlayout.addView(ll);
    }


    //A function that came with using the youtube api to displaying youtube videos
    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        youTubePlayer.cueVideo(VIDEOID);
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }
    //A function that came with using the youtube api to displaying youtube videos


    //gets all country kedomot for the phone
    private String getplace(String k){
        int j=-1;
        for(int i = 0;i<codes.countryCodes.length;i++){
            if(codes.countryCodes[i].equals(k)){
                j = i;
            }
        }
        return codes.countryAreaCodes[j];
    }


    //the function that translates our regular date to jewish date
    public static CalendarDate gregorian2Jewish(CalendarDate date2Convert, CalendarImpl i) {
        int absolute = i.absoluteFromGregorianDate(date2Convert);
        CalendarDate dateJewish = i.jewishDateFromAbsolute(absolute);
        return dateJewish;
    }


    private boolean checkinternetconnectivity(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;
        return  connected;
    }


    private void setParams(int width , int height, float weight , int left , int top , int right , int bottom , int gravity ){
        lp = new LinearLayout.LayoutParams(width, height , weight);
        lp.setMargins(left, top , right, bottom);
        lp.gravity = gravity;
    }

    private void setParams(int width , int height, float weight , int left , int top , int right , int bottom ){
        lp = new LinearLayout.LayoutParams(width, height , weight);
        lp.setMargins(left, top , right, bottom);
    }

    private boolean saving_Vars(String activated, String id){
        Fullname = fullname.getText().toString().trim();
        Fullname = Fullname.replace(" ", "");
        Lastname = lastname.getText().toString().trim();
        Lastname = Lastname.replace(" ", "");
        Phone = phone.getText().toString().trim();
        Yotube= youtubeidvid.getText().toString().trim();
        About = about.getText().toString().trim();
        Day1 = day.getText().toString().trim();
        Month1 = month.getText().toString().trim();
        Year1 = year.getText().toString().trim();

        int Day=0,Month=0,Year=0;
        if(!Day1.equals("")) {
            Day = Integer.parseInt(Day1);
        }
        if(!Month1.equals("")) {
            Month = Integer.parseInt(Month1);
        }
        if(!Year1.equals("")) {
            Year = Integer.parseInt(Year1);
        }

        if (TextUtils.isEmpty(Fullname)) {
            Toast.makeText(ProfileView.this,R.string.str_error_enterfullname, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(Lastname)) {
            Toast.makeText(ProfileView.this,R.string.str_error_enterlastname, Toast.LENGTH_SHORT).show();
            return false;
        }

            if(Phone.length()==9){
                Phone = "+"+getplace(kedomet.getSelectedItem().toString())+Phone;
                Log.e("onClick: ",Phone+ " lslslslslslslslslslslslslsl" );
            }
            else if(Phone.length() == 10){
                Phone = Phone.substring(1);
                Phone = "+"+getplace(kedomet.getSelectedItem().toString())+Phone;
                Log.e("onClick: ",Phone+ " lslslslslslslslslslslslslsl" );
            }

            else if(Phone.length() == 0){
                Phone = "";
            }
            else {
                Toast.makeText(ProfileView.this, R.string.str_error_entervalidphone, Toast.LENGTH_SHORT).show();
                return false;
            }


        if (TextUtils.isEmpty(About)) {
            About = " ";
        }
        if (TextUtils.isEmpty(Day1) || TextUtils.isEmpty(Month1) || TextUtils.isEmpty(Year1)) {
            Toast.makeText(ProfileView.this,R.string.str_error_enterdateofbirth, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (Day < 0 || Day > 32) {
            Toast.makeText(ProfileView.this, R.string.str_error_entervalidday, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (Month < 0 || Month > 13) {

            Toast.makeText(ProfileView.this, R.string.str_error_entervalidmonth, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (Year < 0) {
            Toast.makeText(ProfileView.this,R.string.str_error_entervalidyear, Toast.LENGTH_SHORT).show();
            return false;
        }


        if (isDeceased.isChecked()) {
            FDday = fDday.getText().toString().trim(); // TODO: those 3 variables should be one class date
            FDmonth = fDmonth.getText().toString().trim();
            FDyear = fDyear.getText().toString().trim();



            if (TextUtils.isEmpty(FDday) || TextUtils.isEmpty(FDmonth) || TextUtils.isEmpty(FDyear)) {
                Toast.makeText(ProfileView.this, R.string.str_error_enterdateofdeath, Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        String Ddate;
        if (TextUtils.isEmpty(FDday)) {
            Ddate = "";
        } else {
            Ddate = FDday + "/" + FDmonth + "/" + FDyear;
        }
        temp = new User_toUpload(id, Fullname + " " + Lastname, Phone, Day + "/" + Month + "/" + Year, About, activated, "", Ddate, "");

        if (!TextUtils.isEmpty(Yotube))
            try {
                String s = Yotube.split("/")[3];
                temp.setYoutubeid(Yotube);
            } catch (Exception e) {
                Toast.makeText(ProfileView.this, getResources().getString(R.string.str_error_invalidyoutubelink), Toast.LENGTH_LONG).show();
                return false;
            }
        if (!ifpicturepicked) temp.setPhotolink("no");

        return true;
    }

    private void save_users(final User_toUpload temp, final Button savenewUser, final String gender, final String wuser){
        StorageReference storageref= mStorage.child("photos").child(temp.getId());
        storageref.putFile(thepicture).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                temp.setGender(gender);
                temp.setPhotolink(taskSnapshot.getDownloadUrl().toString());

                switch(wuser){
                    case "spouseString":
                        fbh.SaveSpouse(ProfileView.this, currentuser, temp, progressbar);
                        break;
                    case "parentsString":
                        fbh.SaveParent(ProfileView.this, currentuser, temp, progressbar);
                        break;
                    case "siblingsString":
                        fbh.SaveSibling(ProfileView.this, currentuser, temp, progressbar,father,mother);
                        break;
                    case "childrenString":
                        fbh.SaveChildren(ProfileView.this, currentuser, temp, Gender,currentspouse,progressbar);
                        break;
                    case "editString":
                        fbh.EditUser(temp,ProfileView.this,progressbar);
                        break;
                    default:
                        break;
                }
                setResult(RESULT_OK);

                savenewUser.setClickable(false);

            }
        });
    }
    private void save_usersu(final User_toUpload temp, final Button savenewUser, final String gender, String ur, final String wuser){
        StorageReference storageref= mStorage.child("photos").child(temp.getId());
        Uri uri = Uri.parse(ur);
        storageref.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                temp.setGender(gender);
                temp.setPhotolink(taskSnapshot.getDownloadUrl().toString());

                setResult(RESULT_OK);

                switch(wuser){
                    case "spouseString":
                        fbh.SaveSpouse(ProfileView.this, currentuser, temp, progressbar);
                        break;
                    case "parentsString":
                        fbh.SaveParent(ProfileView.this, currentuser, temp, progressbar);
                        break;
                    case "siblingsString":
                        fbh.SaveSibling(ProfileView.this, currentuser, temp, progressbar,father,mother);
                        break;
                    case "childrenString":
                        fbh.SaveChildren(ProfileView.this, currentuser, temp, Gender,currentspouse,progressbar);
                        break;
                    case "editString":
                        fbh.EditUser(temp,ProfileView.this,progressbar);
                        break;
                    default:
                        break;
                }
            }
        });
    }



    private void tzbeterView(){

        setContentView(R.layout.activity_save_data);
        progressbar.setMessage(getResources().getString(R.string.str_error_loading));

        ifDeceasedLayout = (LinearLayout) findViewById(R.id.SaveData_ifDeceasedLayout);
        genderLayout = (RadioGroup) findViewById(R.id.SaveData_genders);
        isDeceased = (CheckBox) findViewById(R.id.SaveData_ifDeceasedCheckbox);

        isDeceased.setVisibility(View.VISIBLE);
        isDeceased.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    ifDeceasedLayout.setVisibility(View.VISIBLE);
                }
                else
                    ifDeceasedLayout.setVisibility(View.GONE);
            }
        });

        FDday = "";
        FDmonth = "";
        FDyear = "";

        profilepicture = (CircleImageView) findViewById(R.id.SDprofile_image);
        profilepicture.getLayoutParams().width=width/3;
        profilepicture.getLayoutParams().height=width/3;
        profilepicture.setBorderWidth(width/100);

        fullname = (EditText) findViewById(R.id.SDname);
        fullname.requestFocus();
        lastname = (EditText) findViewById(R.id.SDlastname);
        phone= (EditText) findViewById(R.id.SDphone);
        day= (EditText) findViewById(R.id.SDdateday);
        month = (EditText) findViewById(R.id.SDdatemonth);
        year= (EditText) findViewById(R.id.SDdateyear);

        profilepicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ifpicturepicked = true;
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                int gallery = 1;

                startActivityForResult(pickPhoto,gallery);
            }
        });

        kedomet = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, codes.countryCodes);

        kedomet.setAdapter(adapter);

        youtubeidvid = (EditText) findViewById(R.id.SDYoutubevideo);

        about = (EditText) findViewById(R.id.SDabout);

        fDday= (EditText) findViewById(R.id.SDDeathdateday);
        fDmonth = (EditText) findViewById(R.id.SDDeathdatemonth);
        fDyear= (EditText) findViewById(R.id.SDDeathdateyear);

        male = (RadioButton) findViewById(R.id.SDMale);
        female = (RadioButton) findViewById(R.id.SDFemale);

        save= (Button) findViewById(R.id.SDsave);
    }




}

