package com.E.B.SHORASHIM.AlaramManagerr;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.E.A.SHORASHIM.R;
import com.E.B.SHORASHIM.TreeLayoutTemp;

import java.util.Random;

public class AlarmReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent notificationIntent = new Intent(context, TreeLayoutTemp.class);

        String id = intent.getExtras().getString("id");

        notificationIntent.putExtra("user",id);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(TreeLayoutTemp.class);
        stackBuilder.addNextIntent(notificationIntent);

        Random r= new Random();

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

        String name = intent.getExtras().getString("fullname");
        String deathoraliving = intent.getExtras().getString("or");
        String date = intent.getExtras().getString("date");
        int daysTill = intent.getExtras().getInt("daystill");

        Notification notification;

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        String chosenForAlert;

        if(deathoraliving.equals("dateofdeath")) {
            chosenForAlert = context.getResources().getString(R.string.death_date_string);
            notification = notification_life_or_death(" "+chosenForAlert+" ", context, pendingIntent, name, date);
        }
        else{
            chosenForAlert = context.getResources().getString(R.string.birth_date_string);
            if(daysTill!=0)
                notification = notification_life_or_death(" "+chosenForAlert+" "+ context.getResources().getString(R.string.in)+" "+daysTill +context.getResources().getString(R.string.daysOn)+" ", context, pendingIntent, name, date);
            else
                notification = notification_life_or_death(" "+chosenForAlert +" " + context.getResources().getString(R.string.today)+ " ", context, pendingIntent, name, date);
        }

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(r.nextInt(10000000), notification);
    }
    private Notification notification_life_or_death(String content, Context context, PendingIntent pendingIntent, String name, String date)
    {
        Uri sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.who);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        Notification notification = builder.setContentTitle(context.getResources().getString(R.string.reminder))
                .setContentText(name + content + date)
                .setTicker("New Message Alert!")
                .setSmallIcon(R.drawable.rootss)
                .setContentIntent(pendingIntent)
                .setSound(sound)
                .setAutoCancel(true)
                .build();
        return notification;
    }
}