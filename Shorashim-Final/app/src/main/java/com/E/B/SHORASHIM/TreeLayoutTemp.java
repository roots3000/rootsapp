package com.E.B.SHORASHIM;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import java.util.Calendar;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.E.A.SHORASHIM.R;
import com.E.B.SHORASHIM.User_classes.User;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class TreeLayoutTemp extends AppCompatActivity {

    Button Home,SignOut;

    CircleImageView currentuser, father, mother, spouse;

    private static String language = "languagess";

    //Global array for Mother and Father of current user
    User[] popo123;

    //used to check if user is logged in or not
    SharedPreferences sharedpref;
    SharedPreferences.Editor editor;

    boolean swipeleftafterlongpressfix = true;

    // bitmap helper to load all images from firebase
    Bitmap biti;

    TextView currentusertext,fathertext,mothertext,spousetext;

    ImageButton childAdd, siblingadd;

    Button languages;

    int height,width;

    boolean ifCurrent;


    private float x1,x2;
    static final int MIN_DISTANCE = 150;

    LinearLayout siblingsLayout,childrenLayout;

    private FirebaseAuth firebaseauth;


    ProgressDialog progressBar;

    String Gender;

    DatabaseReference dataBaseUser;

    User currentspouse= null,CurrentUser;
    int spousecount=0;
    ArrayList<User> spouses;

    private static String ifloggedin = "iflogged";

    DataBaseHelper db;

    ArrayList<User>currentUserchildren;

    String userkey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_tree_layout_temp);

        sharedpref = this.getSharedPreferences("shared",MODE_PRIVATE);
        editor = sharedpref.edit();

        dataBaseUser = FirebaseDatabase.getInstance().getReference("userprofile");
        db = new DataBaseHelper(this);
        Cursor alldata= db.getalldata();
        StringBuffer stbuff = new StringBuffer();
        int ererte = alldata.getCount();
        if(alldata!=null)
            if(ererte == 0){
//            Toast.makeText(this,"no data", Toast.LENGTH_SHORT).show();
            }
            else{
                while (alldata.moveToNext()){
                    get_Data_Into_String_Buffer(stbuff , alldata);
                }
            }

        AlertDialog.Builder erios= new AlertDialog.Builder(this);
        erios.setCancelable(true);
        erios.setTitle("erios");
        erios.setMessage(stbuff.toString());


        languages = (Button) findViewById(R.id.TreeLanguageButton);

        LangButtonTextChange();

        languages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Tree = new Intent(TreeLayoutTemp.this, LangChoose.class);

                TreeLayoutTemp.this.startActivityForResult(Tree,0);
            }
        });

        Home = (Button) findViewById(R.id.TVhome);
        Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                classSetUp1(userkey);
            }
        });
        Home.setText(R.string.str_home);

        SharedPreferences sharedpref = this.getPreferences(MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedpref.edit();

        TextView tx5 = (TextView) findViewById(R.id.textView5);
        tx5.setText(getResources().getString(R.string.str_contactUs));

        SignOut = (Button) findViewById(R.id.TVlogout);
        SignOut.setText(R.string.str_signout);
        SignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start_main_activity();
            }
        });

        firebaseauth = FirebaseAuth.getInstance();

        popo123 = new User[2];

        userkey =  getIntent().getExtras().getString("user");

        currentuser = (CircleImageView) findViewById(R.id.TVcurrentuserpic);

        father = (CircleImageView) findViewById(R.id.TVfatherpic);

        mother = (CircleImageView) findViewById(R.id.TVmotherpic);

        spouse = (CircleImageView) findViewById(R.id.TVspousepic);

        currentusertext = (TextView) findViewById(R.id.TVcurrentuser);
        fathertext = (TextView) findViewById(R.id.TVfather);
        mothertext = (TextView) findViewById(R.id.TVmother);
        spousetext = (TextView) findViewById(R.id.TVspouse);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        childAdd = (ImageButton) findViewById(R.id.TVchildrenadd);

        childAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ifCurrent) {
                    if (currentspouse == null)
                    {
                        Toast.makeText(TreeLayoutTemp.this, R.string.str_toaddchildaddspouse, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if(!checkinternetconnectivity()){Toast t =Toast.makeText(TreeLayoutTemp.this,R.string.str_error_nointernetconnection,Toast.LENGTH_SHORT);
                        t.setGravity(Gravity.CENTER,0,0);
                        t.show();
                    }
                    else {
                        Intent i ;
                        i = init_abstract_user_intent(null);
                        i = add_relative_to_intent(i,"children",CurrentUser.getGender());

                        putExtra_CurrentSpouse(i,currentspouse);


                        TreeLayoutTemp.this.startActivityForResult(i, 1);
                    }
                }
            }
        });

        siblingadd = (ImageButton) findViewById(R.id.TVsiblingsadd);
        siblingadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(popo123[0] == null || popo123[1] == null)
                {
                    Toast.makeText(TreeLayoutTemp.this, R.string.str_error_pleaseaddparents, Toast.LENGTH_LONG).show();
                    return;
                }
                if(ifCurrent) {
                    if(!checkinternetconnectivity()){Toast t =Toast.makeText(TreeLayoutTemp.this,R.string.str_error_nointernetconnection,Toast.LENGTH_SHORT);
                        t.setGravity(Gravity.CENTER,0,0);
                        t.show();
                    }
                    else {
                        Intent i ;

                        User dad = popo123[0], mama = popo123[1];

                        Bitmap BitmapDad = null;
                        Bitmap BitmapMom = null;
                        if (dad != null)
                            BitmapDad = dad.getPhotolink();
                        if (mama != null)
                            BitmapMom = mama.getPhotolink();

                        if (dad != null)
                            dad.setPhotolink(null);
                        if (mama != null)
                            mama.setPhotolink(null);
                        i = init_abstract_user_intent(null);
                        i = add_relative_to_intent(i,"sibling",Gender);

                        // TODO: extract the i actions into methods of i - DONE

                        putExtra_Parents(i,  dad ,mama );


                        TreeLayoutTemp.this.startActivityForResult(i, 1);
                        if (dad != null)
                            dad.setPhotolink(BitmapDad);
                        if (mama != null)
                            mama.setPhotolink(BitmapMom);
                    }
                }
            }
        });

        siblingsLayout = (LinearLayout) findViewById(R.id.TVsiblingsView);

        childrenLayout = (LinearLayout) findViewById(R.id.TVchildrenView);

        progressBar = new ProgressDialog(this);

        siblingsLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);

                return false;
            }
        });

        //-----Changing size for circle images------------
        setButtonSizesMain(father);
        setButtonSizesMain(mother);
        setButtonSizesMain(currentuser);
        setButtonSizesMain(spouse);


        classSetUp(userkey);
    }

    protected Intent add_relative_to_intent(Intent i ,String relationship, String gender)
    {
        i.putExtra("who",relationship);
        i.putExtra("currentGender", gender);
        return i;
    }
    private void setupp(){
        languages = (Button) findViewById(R.id.TreeLanguageButton);

        LangButtonTextChange();

        languages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Tree = new Intent(TreeLayoutTemp.this, LangChoose.class);

                TreeLayoutTemp.this.startActivityForResult(Tree,0);
            }
        });


        Home = (Button) findViewById(R.id.TVhome);
        Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                classSetUp(userkey);
            }
        });
        Home.setText(R.string.str_home);

        SharedPreferences sharedpref = this.getPreferences(MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedpref.edit();

        TextView tx5 = (TextView) findViewById(R.id.textView5);
        tx5.setText(getResources().getString(R.string.str_contactUs));

        SignOut = (Button) findViewById(R.id.TVlogout);
        SignOut.setText(R.string.str_signout);
        SignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start_main_activity();
            }
        });

        firebaseauth = FirebaseAuth.getInstance();

        popo123 = new User[2];

        currentuser = (CircleImageView) findViewById(R.id.TVcurrentuserpic);

        father = (CircleImageView) findViewById(R.id.TVfatherpic);
        mother = (CircleImageView) findViewById(R.id.TVmotherpic);
        spouse = (CircleImageView) findViewById(R.id.TVspousepic);

        currentusertext = (TextView) findViewById(R.id.TVcurrentuser);
        fathertext = (TextView) findViewById(R.id.TVfather);
        mothertext = (TextView) findViewById(R.id.TVmother);
        spousetext = (TextView) findViewById(R.id.TVspouse);

        childAdd = (ImageButton) findViewById(R.id.TVchildrenadd);

        childAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ifCurrent) {
                    Intent i ;
                    i = init_abstract_user_intent(null);
                    i = add_relative_to_intent(i,"children",CurrentUser.getGender());

                    putExtra_CurrentSpouse(i,currentspouse);


                     TreeLayoutTemp.this.startActivityForResult(i, 1);
                }
            }
        });

        siblingadd = (ImageButton) findViewById(R.id.TVsiblingsadd);
        siblingadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ifCurrent) {
                    Intent i = new Intent(TreeLayoutTemp.this, ProfileView.class);

                    User dad =popo123[0],mama = popo123[1];

                    Bitmap BitmapDad = dad.getPhotolink();
                    Bitmap BitmapMom = mama.getPhotolink();
                    dad.setPhotolink(null);
                    mama.setPhotolink(null);
                    i = init_abstract_user_intent(null);
                    i = add_relative_to_intent(i,"sibling",Gender);

                    putExtra_Parents(i,  dad ,mama );
                    TreeLayoutTemp.this.startActivityForResult(i, 1);
                    dad.setPhotolink(BitmapDad);
                    mama.setPhotolink(BitmapMom);
                }
            }
        });

        siblingsLayout = (LinearLayout) findViewById(R.id.TVsiblingsView);

        childrenLayout = (LinearLayout) findViewById(R.id.TVchildrenView);

        progressBar = new ProgressDialog(this);

        classSetUp1(CurrentUser.getId());
    }

    protected Intent init_abstract_user_intent(User us)
    {
        Intent i = new Intent(TreeLayoutTemp.this, ProfileView.class);
        i.putExtra("user", (Serializable) us);
        i.putExtra("currentUser", userkey);
        return i;
    }
    protected void start_main_activity() {
        firebaseauth.signOut();
        editor.putInt(ifloggedin, 0);
        TreeLayoutTemp.this.startActivity(new Intent(TreeLayoutTemp.this, MainActivity.class));
        TreeLayoutTemp.this.finish();
    }
    //Once Tree Activity opens it calls this  function to check if the
    //current logged in user exists in the SQL DATABASE, and if it does it
    //just shows whats in the sql while in the background getting data from user
    //and his family tree from FIREBASE. if it doesnt exist then it will get data for first time
    private void classSetUp(final String key){
        currentspouse = null;
        spouses= new ArrayList<User>();
        childrenLayout.removeAllViews();
        siblingsLayout.removeAllViews();

        setImage(father , "male");
        setImage(mother , "female");
        setImage(spouse , "female");


        fathertext.setText(R.string.str_father);
        mothertext.setText(R.string.str_mother);
        spousetext.setText(R.string.str_spouse);

        if(key.equals(userkey)){
            ifCurrent = true;
        }
        else{
            ifCurrent = false;
        }

        progressBar.setMessage(getResources().getString(R.string.str_error_loading));
        progressBar.show();

        if(db.checkifexistsinsql(key))
            classSetupForSpouseAndParents11(key);
        else {
            gettingmainuserToSync1(key);
        }
    }
    //Once Tree Activity opens it calls this  function to check if the
    //current logged in user exists in the SQL DATABASE, and if it does it
    //just shows whats in the sql while in the background getting data from user
    //and his family tree from FIREBASE. if it doesnt exist then it will get data for first time

    //called after tree activity has been setup. in this function it only shows whats in sql
    //and it gets the data in firebase for user and his tree(in the background)
    private void classSetUp1(final String key) {
        currentspouse = null;
        spouses = new ArrayList<User>();
        childrenLayout.removeAllViews();
        siblingsLayout.removeAllViews();

        father.setImageResource(R.drawable.male);
        father.setBorderColor(Color.parseColor("#FFFFFF"));
        mother.setImageResource(R.drawable.female);
        mother.setBorderColor(Color.parseColor("#FFFFFF"));
        spouse.setImageResource(R.drawable.female);
        spouse.setBorderColor(Color.parseColor("#FFFFFF"));

        fathertext.setText(R.string.str_father);
        mothertext.setText(R.string.str_mother);
        spousetext.setText(R.string.str_spouse);

        if (key.equals(userkey)) {
            ifCurrent = true;
        } else {
            ifCurrent = false;
        }

        progressBar.setMessage(getResources().getString(R.string.str_error_loading));
        progressBar.show();


        classSetupForSpouseAndParents11(key);

        if(checkinternetconnectivity())
            gettingmainuserToSync(key);

    }
    //called after tree activity is setup in this function it only shows whats in sql
    //and it gets the data in firebase for user and his tree(in the background)


    //the functions that end with an extra 1 are connected to classSetUp1 and the other are connected with classSetUp

    private  void gettingmainuserToSync(final String key){
        dataBaseUser = FirebaseDatabase.getInstance().getReference("userprofile");
        dataBaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                loop1:
                for( DataSnapshot usersnapshot: dataSnapshot.getChildren()){
                    if(usersnapshot.getKey().equals(key)){
                        final User tempCurrent;
                        tempCurrent = new User(usersnapshot);


                        final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                        oo.setLayoutParams(lp);
                        Glide.with(getApplicationContext())
                                .load(tempCurrent.url)
                                .asBitmap()
                                .listener(new RequestListener<String, Bitmap>() {
                                    @Override
                                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                        biti= resource;
                                        tempCurrent.setPhotolink(biti);
                                        db.sync(tempCurrent,TreeLayoutTemp.this);
                                        ClassSync(tempCurrent);
                                        return false;
                                    }
                                })
                                .into(oo);
                        break loop1;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    private  void gettingmainuserToSync1(final String key){
        dataBaseUser = FirebaseDatabase.getInstance().getReference("userprofile");
        dataBaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                loop1:
                for( DataSnapshot usersnapshot: dataSnapshot.getChildren()){
                    if(usersnapshot.getKey().equals(key)){
                        final User tempCurrent;
                        tempCurrent = new User(usersnapshot);
                        final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                        oo.setLayoutParams(lp);
                        Glide.with(getApplicationContext())
                                .load(tempCurrent.url)
                                .asBitmap()
                                .listener(new RequestListener<String, Bitmap>() {
                                    @Override
                                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                        biti= resource;
                                        tempCurrent.setPhotolink(biti);
                                        db.sync(tempCurrent,TreeLayoutTemp.this);
                                        ClassSync1(tempCurrent);
                                        Cursor crs = db.getalldata();
                                        if(crs.getCount()>0){
                                            classSetupForSpouseAndParents111(key);
                                        }
                                        return false;
                                    }
                                })
                                .into(oo);
                        break loop1;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    //the ClassSync function gets data from firebase and stores it in sql (runs in the background)
    //it gets data from current user and his own tree
    private void ClassSync(final User main){
        dataBaseUser = FirebaseDatabase.getInstance().getReference("userprofile");
        dataBaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int i =0;
                loop1:
                for( DataSnapshot usersnapshot: dataSnapshot.getChildren()){
                    if(usersnapshot.getKey().equals(main.getParent()[0])||usersnapshot.getKey().equals(main.getParent()[1])){
                        final User tempCurrent;
                        tempCurrent = new User(usersnapshot);
                        final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                        oo.setLayoutParams(lp);
                        Glide.with(getApplicationContext())
                                .load(tempCurrent.url)
                                .asBitmap()
                                .listener(new RequestListener<String, Bitmap>() {
                                    @Override
                                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                        biti= resource;
                                        tempCurrent.setPhotolink(biti);

                                        ClassSyncforSecondCircle(tempCurrent);

                                        db.sync(tempCurrent,TreeLayoutTemp.this);
                                        return false;
                                    }
                                })
                                .into(oo);

                        continue loop1;
                    }

                    loop2:
                    for(String s: main.getSpouse()){
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            tempCurrent =  new User(usersnapshot);

                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(tempCurrent.url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            ClassSyncforSecondCircle(tempCurrent);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            return false;
                                        }
                                    })
                                    .into(oo);

                            continue loop1;
                        }
                    }

                    loop2:
                    for(int j =0; j< main.getSibling().size();j++){
                        String s = main.getSibling().get(j);
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            tempCurrent = new User(usersnapshot);

                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(tempCurrent.url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            ClassSyncforSecondCircle(tempCurrent);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            return false;
                                        }
                                    })
                                    .into(oo);

                            continue loop1;
                        }
                    }

                    loop2:
                    for(String s: main.getChildren()){
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            tempCurrent = new User(usersnapshot);

                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(tempCurrent.url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            ClassSyncforSecondCircle(tempCurrent);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            return false;
                                        }
                                    })
                                    .into(oo);

                            continue loop1;
                        }
                    }
                }
            }

            @Override

            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    private void ClassSync1(final User main){
        dataBaseUser = FirebaseDatabase.getInstance().getReference("userprofile");
        dataBaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int i =0;
                loop1:
                for( DataSnapshot usersnapshot: dataSnapshot.getChildren()){
                    if(usersnapshot.getKey().equals(main.getParent()[0])||usersnapshot.getKey().equals(main.getParent()[1])){
                        final User tempCurrent;
                        tempCurrent = new User(usersnapshot);
                        final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                        oo.setLayoutParams(lp);
                        Glide.with(getApplicationContext())
                                .load(tempCurrent.url)
                                .asBitmap()
                                .listener(new RequestListener<String, Bitmap>() {
                                    @Override
                                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                        biti= resource;
                                        tempCurrent.setPhotolink(biti);

                                        ClassSyncforSecondCircle1(tempCurrent);

                                        db.sync(tempCurrent,TreeLayoutTemp.this);
                                        Cursor crs = db.getalldata();
                                        if(crs.getCount()>0){
                                            classSetupForSpouseAndParents111(main.getId());
                                        }
                                        return false;
                                    }
                                })
                                .into(oo);

                        continue loop1;
                    }

                    loop2:
                    for(String s: main.getSpouse()){
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            tempCurrent = new User(usersnapshot);
                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(tempCurrent.url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            ClassSyncforSecondCircle1(tempCurrent);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            Cursor crs = db.getalldata();
                                            if(crs.getCount()>0){
                                                classSetupForSpouseAndParents111(main.getId());
                                            }
                                            return false;
                                        }
                                    })
                                    .into(oo);
                            continue loop1;
                        }
                    }

                    loop2:
                    for(int j =0; j< main.getSibling().size();j++){
                        String s = main.getSibling().get(j);
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                           tempCurrent = new User(usersnapshot);

                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(tempCurrent.url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            ClassSyncforSecondCircle1(tempCurrent);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            Cursor crs = db.getalldata();
                                            if(crs.getCount()>0){
                                                classSetupForSpouseAndParents111(main.getId());
                                            }
                                            return false;
                                        }
                                    })
                                    .into(oo);
                            continue loop1;
                        }
                    }

                    loop2:
                    for(String s: main.getChildren()){
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            tempCurrent = new User(usersnapshot);

                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(tempCurrent.url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            ClassSyncforSecondCircle1(tempCurrent);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            Cursor crs = db.getalldata();
                                            if(crs.getCount()>0){
                                                classSetupForSpouseAndParents111(main.getId());
                                            }
                                            return false;
                                        }
                                    })
                                    .into(oo);
                            continue loop1;
                        }
                    }
                }

            }

            @Override

            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void classSetupForSpouseAndParents111(final String main){
        User currenttt = null;
        User [] parents = new User[2];
        int i = 0;
        ArrayList<User> spousess = new ArrayList();
        ArrayList<User> siblingss = new ArrayList();
        ArrayList<User> childrenn = new ArrayList();


        Cursor alldata = db.getalldata();
        int zoborte = alldata.getCount();

        if(alldata!=null&& zoborte!=0) {
            loop1:
            while (alldata.moveToNext()) {
                String idd = alldata.getString(8);

                if (idd != null)
                    if (idd.equals(main)) {
                        final User tempCurrent = new User(alldata);
                        currenttt = tempCurrent;
                        CurrentUser = tempCurrent;
                        break loop1;
                    }

            }
            alldata= db.getalldata();
            String fafa,mama;
            if(currenttt.getParent()[0]==null)
                fafa = "";
            else
                fafa = currenttt.getParent()[0];

            if(currenttt.getParent()[1]==null)
                mama = "";
            else
                mama = currenttt.getParent()[1];

            loop2:
            while (alldata.moveToNext()) {
                if(alldata.getString(8)!=null)
                    if (alldata.getString(8).equals(fafa) || alldata.getString(8).equals(mama)) {
                        final User tempCurrent = new User(alldata);
                        String[] parentss = tempCurrent.getParent();
                        if(tempCurrent.getGender().equals("male"))
                            parents[0] = tempCurrent;
                        else if(tempCurrent.getGender().equals("female"))
                            parents[1] = tempCurrent;
                        tempCurrent.setParent(parentss);
                        continue loop2;
                    }

                for (String s : currenttt.getSpouse()) {
                    if (s.equals(alldata.getString(8))) {
                        final User tempCurrent = new User(alldata);
                        spousess.add(tempCurrent);
                        continue loop2;
                    }
                }

                for (String s : currenttt.getSibling()) {
                    if (s.equals(alldata.getString(8))) {
                        final User tempCurrent = new User(alldata);
                        siblingss.add(tempCurrent);
                        continue loop2;
                    }
                }

                for (String s : currenttt.getChildren()) {
                    if (s.equals(alldata.getString(8))) {
                        final User tempCurrent = new User(alldata);
                        childrenn.add(tempCurrent);
                        continue loop2;
                    }
                }
            }
            popo123[0]= parents[0];
            popo123[1]= parents[1];

            setUpButtons(parents[0],parents[1],currenttt,spousess,childrenn,siblingss);
        }
    }
    //the ClassSync function gets data from firebase and stores it in sql (runs in the background)
    //it gets data from tree's of users inside the current user's tree
    private void ClassSyncforSecondCircle(final User main){
        dataBaseUser = FirebaseDatabase.getInstance().getReference("userprofile");
        dataBaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                loop1:
                for( DataSnapshot usersnapshot: dataSnapshot.getChildren()){
                    if(usersnapshot.getKey().equals(main.getParent()[0])||usersnapshot.getKey().equals(main.getParent()[1])){
                        final User tempCurrent;
                        tempCurrent = new User(usersnapshot);

                        final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                        oo.setLayoutParams(lp);
                        Glide.with(getApplicationContext())
                                .load(tempCurrent.url)
                                .asBitmap()
                                .listener(new RequestListener<String, Bitmap>() {
                                    @Override
                                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {

                                        biti= resource;
                                        tempCurrent.setPhotolink(biti);

                                        db.sync(tempCurrent,TreeLayoutTemp.this);
                                        return false;
                                    }
                                })
                                .into(oo);


                        continue loop1;
                    }

                    loop2:
                    for(String s: main.getSpouse()){
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            tempCurrent = new User(usersnapshot);
                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(tempCurrent.url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {

                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            return false;
                                        }
                                    })
                                    .into(oo);


                            continue loop1;
                        }
                    }

                    loop2:
                    for(int j =0; j< main.getSibling().size();j++){
                        String s = main.getSibling().get(j);
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            tempCurrent = new User(usersnapshot);
                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(tempCurrent.url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {

                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);

                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            return false;
                                        }
                                    })
                                    .into(oo);
                            continue loop1;
                        }
                    }

                    loop2:
                    for(String s: main.getChildren()){
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                           tempCurrent = new User(usersnapshot);
                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(tempCurrent.url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);
                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            return false;
                                        }
                                    })
                                    .into(oo);


                            continue loop1;
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }
    private void ClassSyncforSecondCircle1(final User main){
        dataBaseUser = FirebaseDatabase.getInstance().getReference("userprofile");
        dataBaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                loop1:
                for( DataSnapshot usersnapshot: dataSnapshot.getChildren()){
                    if(usersnapshot.getKey().equals(main.getParent()[0])||usersnapshot.getKey().equals(main.getParent()[1])){
                        final User tempCurrent = new User(usersnapshot);
                        final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                        oo.setLayoutParams(lp);
                        Glide.with(getApplicationContext())
                                .load(tempCurrent.url)
                                .asBitmap()
                                .listener(new RequestListener<String, Bitmap>() {
                                    @Override
                                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {

                                        biti= resource;
                                        tempCurrent.setPhotolink(biti);
                                        db.sync(tempCurrent,TreeLayoutTemp.this);
                                        return false;
                                    }
                                })
                                .into(oo);
                        continue loop1;
                    }

                    loop2:
                    for(String s: main.getSpouse()){
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            tempCurrent = new User(usersnapshot);
                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(tempCurrent.url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);
                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            return false;
                                        }
                                    })
                                    .into(oo);
                            continue loop1;
                        }
                    }

                    loop2:
                    for(int j =0; j< main.getSibling().size();j++){
                        String s = main.getSibling().get(j);
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            tempCurrent = new User(usersnapshot);
                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(tempCurrent.url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);
                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            return false;
                                        }
                                    })
                                    .into(oo);
                            continue loop1;
                        }
                    }

                    loop2:
                    for(String s: main.getChildren()){
                        if(s.equals(usersnapshot.getKey())){
                            final User tempCurrent;
                            tempCurrent = new User(usersnapshot);
                            final CircleImageView oo = new CircleImageView(TreeLayoutTemp.this);
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(420,420);
                            oo.setLayoutParams(lp);
                            Glide.with(getApplicationContext())
                                    .load(tempCurrent.url)
                                    .asBitmap()
                                    .listener(new RequestListener<String, Bitmap>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                            return false;
                                        }
                                        @Override
                                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {

                                            biti= resource;
                                            tempCurrent.setPhotolink(biti);
                                            db.sync(tempCurrent,TreeLayoutTemp.this);
                                            return false;
                                        }
                                    })
                                    .into(oo);
                            continue loop1;
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    //here we retrieve data from SQL DataBase to set up the layout
    private void classSetupForSpouseAndParents11(final String main){
        User currenttt = null;
        User [] parents = new User[2];
        int i = 0;
        ArrayList<User> spousess = new ArrayList();
        ArrayList<User> siblingss = new ArrayList();
        ArrayList<User> childrenn = new ArrayList();

        Cursor alldata = db.getalldata();
        int zoborte = alldata.getCount();

        if(alldata!=null&& zoborte!=0) {
            loop1:
            while (alldata.moveToNext()) {
                String idd = alldata.getString(8);

                if (idd != null)
                    if (idd.equals(main)) {
                        final User tempCurrent = new User(alldata);
                        currenttt = tempCurrent;
                        CurrentUser = tempCurrent;
                        break loop1;
                    }
            }
            alldata= db.getalldata();
            String fafa="",mama="";
            if(currenttt.getParent()[0]==null)
                fafa = "";
            else
                fafa = currenttt.getParent()[0];

            if(currenttt.getParent()[1]==null)
                mama = "";
            else
                mama = currenttt.getParent()[1];

            loop2:
            while (alldata.moveToNext()) {
                if(alldata.getString(8)!=null)
                    if (alldata.getString(8).equals(fafa) || alldata.getString(8).equals(mama)) {
                        final User tempCurrent = new User(alldata);
                        String[] parentss = tempCurrent.getParent();
                        if(tempCurrent.getGender().equals("male"))
                            parents[0] = tempCurrent;
                        else if(tempCurrent.getGender().equals("female"))
                            parents[1] = tempCurrent;
                        tempCurrent.setParent(parentss);
                        continue loop2;
                    }

                for (String s : currenttt.getSpouse()) {
                    if (s.equals(alldata.getString(8))) {
                        final User tempCurrent = new User(alldata);
                        spousess.add(tempCurrent);
                        continue loop2;
                    }
                }

                for (String s : currenttt.getSibling()) {
                    if (s.equals(alldata.getString(8))) {
                        final User tempCurrent = new User(alldata);
                        siblingss.add(tempCurrent);
                        continue loop2;
                    }
                }

                for (String s : currenttt.getChildren()) {
                    if (s.equals(alldata.getString(8))) {
                        final User tempCurrent = new User(alldata);
                        childrenn.add(tempCurrent);
                        continue loop2;
                    }
                }
            }

            popo123[0]= parents[0];
            popo123[1]= parents[1];

            setUpButtons(parents[0],parents[1],currenttt,spousess,childrenn,siblingss);
        }
    }




    private void setUpButtons(final User Father, final User Mother, final User currentUser, final ArrayList<User> Spouse, ArrayList<User> children, ArrayList<User> siblings){
        if (progressBar != null)
            if(progressBar.isShowing())
                progressBar.dismiss();
        spouses= Spouse;
        spousecount = 0;
        currentspouse = getSpouse();

        currentUserchildren = children;

        //Checks if the user is dead or not/ heka bkol el buttons bf7s
        buttonset(currentUser,currentuser,currentusertext,currentUser.getFullname(),currentUser,"currrent");

        buttonset(Father,father,fathertext,this.getResources().getString(R.string.str_father),currentUser,"father");

        buttonset(Mother,mother,mothertext,this.getResources().getString(R.string.str_mother),currentUser,"mother");

        buttonset(currentspouse,spouse,spousetext,this.getResources().getString(R.string.str_spouse),currentUser,"spouse");

        setSiblingLayout(siblings,currentUser);

        setChildrenLayout(children,currentUser);


        setUpAllNotifications(Father,Mother,Spouse,children,siblings);

//        dAllM(children,siblings);
//        dAll(children,siblings);
//        dCurrentSpouse();
//        dAllSpouse(children);
//        dChildren(children);
//        dSiblings(siblings);
//        dParents(siblings);
    }



    //-----Gets first spouse from the global spouse array #spouses#------
    private User getSpouse() {
        User temp = null;
        if(spouses.size()>0) {
            temp = spouses.get(0);

        }
        else
            temp = null;
        return temp;
    }
    //-----Gets first spouse from the global spouse array #spouses#------

    private void setChildrenLayout(ArrayList<User> children, final User currentUser) {
        childrenLayout.removeAllViews();
        TextView newtext = new TextView(TreeLayoutTemp.this);
        newtext.setText(getResources().getString(R.string.str_children));
        newtext.setTextColor(Color.parseColor("#000000"));
        newtext.setTextSize(20);
        childrenLayout.addView(newtext);
        try {
            if (!children.isEmpty()&&children.size()>0) {
                childrenLayout.removeAllViews();
                for (final User user : children) {
                    if ((user.getParent()[0].equals(currentspouse.getId()) && user.getParent()[1].equals(CurrentUser.getId()) )|| (user.getParent()[1].equals(currentspouse.getId()) && user.getParent()[0].equals(CurrentUser.getId()))) {
                        childrensiblingsSet(user,currentUser,"children");

                    }
                }
            }
        }
        catch (Exception e){
            Toast.makeText(this,R.string.str_error_pleaseaddotherparenttoview,Toast.LENGTH_LONG).show();
        }
    }

    private void setSiblingLayout(ArrayList<User> siblings, final User currentUser) {
        siblingsLayout.removeAllViews();
        TextView newtext = new TextView(TreeLayoutTemp.this);
        newtext.setText(getResources().getString(R.string.str_siblings));
        newtext.setTextColor(Color.parseColor("#000000"));
        newtext.setTextSize(20);
        siblingsLayout.addView(newtext);
        if(siblings!=null&&siblings.size()>0) {
            siblingsLayout.removeAllViews();
            User user = null;
            for (int i = 0 ; i<siblings.size();i++) {
                user = siblings.get(i);
                childrensiblingsSet(user,currentUser,"sibling");
            }
        }

    }


    //---When we change language this is the code that take back what language we chose------------------
    //---also when we close an opened profile it enters this code also and the classSetUp is activated---
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            classSetUp1(userkey);
        }
        if(resultCode == RESULT_FIRST_USER){
            String lang = data.getExtras().getString(language);
            editor.putString(language,lang);
            editor.commit();
            changelang(lang);

            setupp();
        }
    }
    //---When we change language this is the code that take back what language we chose------------------
    //---also when we close an opened profile it enters this code also and the classSetUp is activated---





    //----------------On swipe switch to next/previous spouse------------------
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x2 - x1;
                if (Math.abs(deltaX) > MIN_DISTANCE)
                {
                    if(!swipeleftafterlongpressfix){
                        DisplayMetrics dm = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(dm);
                        x1 = dm.widthPixels;
                        swipeleftafterlongpressfix = true;
                    }
                    //left2right
                    if(x2>x1)
                    {

                        childrenLayout.removeAllViews();
                        boolean sex;
                        if(spousecount == spouses.size()-1){
                            spousecount++;
                            sex = true;
                        }
                        else
                            sex = false;
                        if(sex){
                            currentspouse = null;
                            if(Gender!=null)
                                if(Gender.equals("male"))
                                    spouse.setImageResource(R.drawable.female);
                                else
                                    spouse.setImageResource(R.drawable.male);

                            spouse.setBorderColor(Color.parseColor("#FFFFFF"));

                            spousetext.setText(R.string.str_spouse);

                            if(userkey.equals(CurrentUser.getId())) {
                                spouse.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                            if(!checkinternetconnectivity()){
                                                Toast t =Toast.makeText(TreeLayoutTemp.this,R.string.str_error_nointernetconnection,Toast.LENGTH_SHORT);
                                                t.setGravity(Gravity.CENTER,0,0);
                                                t.show();
                                            }
                                            else {
                                                profileviewNull(currentspouse,CurrentUser,"spouse");
                                            }
                                    }
                                });
                                try {
                                    spouse.setOnLongClickListener(new View.OnLongClickListener() {
                                        @Override
                                        public boolean onLongClick(View v) {
                                            classSetUp1(currentspouse.getId());
                                            return true;
                                        }
                                    });
                                } catch (Exception e) {

                                }
                            }
                            else{
                                spouse.setOnClickListener(null);
                                spouse.setOnLongClickListener(null);
                            }
                            if(currentspouse==null&&CurrentUser.getId().equals(userkey)) {
                                childAdd.setClickable(false);
                                Toast.makeText(TreeLayoutTemp.this,R.string.str_toaddchildaddspouse,Toast.LENGTH_LONG).show();
                            }
                        }
                        else if (spousecount<spouses.size()){
                            childAdd.setClickable(true);
                            currentspouse = spouses.get(++spousecount);
                            buttonset(currentspouse,spouse,spousetext,this.getResources().getString(R.string.str_spouse),CurrentUser,"spouse");

                            setChildrenLayout(currentUserchildren,CurrentUser);
                        }

                    }

                    //right2left
                    if(x1>x2)
                    {
                        childAdd.setClickable(true);
                        boolean sex;
                        if(spousecount > 0&&spouses.size()>0){
                            sex = true;
                        }
                        else
                            sex = false;
                        if(sex){
                            childrenLayout.removeAllViews();
                            currentspouse = spouses.get(--spousecount);

                            buttonset(currentspouse,spouse,spousetext,this.getResources().getString(R.string.str_spouse),CurrentUser,"spouse");

                            setChildrenLayout(currentUserchildren,CurrentUser);
                        }


                    }

                }
                else
                {
                    // consider as something else - a screen tap for example
                }
                break;
        }
        return super.onTouchEvent(event);
    }
    //----------------On swipe switch to next/previous spouse------------------



    private boolean checkinternetconnectivity(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;
        return  connected;
    }

    private void changelang(String lang){
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        this.setContentView(R.layout.activity_tree_layout_temp);
    }





    private void profileView(User us,User current,String who) {
        Intent i ;
        if (us != null) {
            Bitmap kokos = us.getPhotolink();
            ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
            kokos.compress(Bitmap.CompressFormat.PNG, 100, stream1);
            i = init_abstract_user_intent(us);

            us.setPhotolink(null);
            putExtra_ProfileView(i ,  who , current,stream1.toByteArray());

            TreeLayoutTemp.this.startActivityForResult(i, 1);
            us.setPhotolink(kokos);
        }
    }

    private void buttonset(final User us, CircleImageView b, TextView txt, String s, final User current,final String who) {
        Bitmap bitmap ;
        ByteArrayOutputStream stream;

        final boolean[] doubleClicked = new boolean[1];
        doubleClicked[0]=false;

        if (us != null) {
            if (us.getDateofdeath().equals(""))
                b.setBorderColor(Color.parseColor("#467777"));
            else {
                b.setBorderColor(Color.parseColor("#160d44"));

            }
            bitmap = us.getPhotolink();

            stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Glide.with(TreeLayoutTemp.this)
                    .load(stream.toByteArray())
                    .asBitmap()
                    .into(b);

        } else {
        }


        b.setOnClickListener(null);
        txt.setText(s);
        b.setOnLongClickListener(null);
        if (!ifCurrent && us == null) {
        } else {
            try {
                String name1 = us.getFullname() + " ";
                if (who.equals("spouse")&& !us.getId().equals(CurrentUser.getSpouse().get(0)))
                    name1 = "Ex Spouse: ".concat(name1);
                txt.setText(name1.split(" ")[0]);
            } catch (Exception e) {
            }
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Handler handler = new Handler();
                    Runnable runn = new Runnable() {
                        @Override
                        public void run() {
                            if (doubleClicked[0]){
                                if (us != null) {
                                    profileView(us,current,who);
                                } else {
                                    profileviewNull(us,current,who);
                                }
                            }
                            doubleClicked[0]=false;
                        }
                    };
                    if(!doubleClicked[0]){
                        handler.postDelayed(runn,400);
                        doubleClicked[0] = true;
                    }
                    else if(doubleClicked[0]){
                        swipeleftafterlongpressfix = false;
                        classSetUp1(us.getId());
                        doubleClicked[0]=false;
                    }
                }
            });
        }
    }

    private void profileviewNull(User us,User current,String who) {
        Intent i ;
        if(!checkinternetconnectivity()){Toast t =Toast.makeText(TreeLayoutTemp.this,R.string.str_error_nointernetconnection,Toast.LENGTH_SHORT);
            t.setGravity(Gravity.CENTER,0,0);
            t.show();
        }
        else
        {
            i = init_abstract_user_intent(us);
   putExtra_ProfileView(i ,  who , current , new byte[0]);
            i.putExtra("currentGender", current.getGender());


            TreeLayoutTemp.this.startActivityForResult(i, 1);
        }
    }

    private void childrensiblingsSet(final User us,final User current,final String who){
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT,1.0f);
        LinearLayout lolo = new LinearLayout(this);
        lolo.setOrientation(LinearLayout.VERTICAL);
        lolo.setLayoutParams(lp);

        lp = new LinearLayout.LayoutParams(width/6,width/6,10f);
        lp.setMargins(10,0,10,0);
        CircleImageView siblingtemp = new CircleImageView(this);
        siblingtemp.setBorderWidth(width/135);
        siblingtemp.setLayoutParams(lp);
        lolo.addView(siblingtemp);




        lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f);
        TextView siblingtemptext = new TextView(this);
        lp.gravity= Gravity.CENTER;
        siblingtemptext.setTextColor(Color.parseColor("#000000"));
        siblingtemptext.setLayoutParams(lp);

        lolo.addView(siblingtemptext);

        us.setFullname(us.getFullname() + " ");
        buttonset(us,siblingtemp,siblingtemptext,us.getFullname().split(" ")[0],current,who);
        if(who.equals("children"))
            childrenLayout.addView(lolo);
        else
            siblingsLayout.addView(lolo);

    }

    private void dCurrentSpouse(){
        if(currentspouse!=null) {
            db.deleteUser(currentspouse.getId());
            dataBaseUser.child(CurrentUser.getId()).removeValue();
        }
    }
    private void dAllSpouse(ArrayList<User> children){
        for (int i=0;i<spouses.size();i++){
            db.deleteUser(spouses.get(i).getId());
            dataBaseUser.child(spouses.get(i).getId()).removeValue();
        }
        dataBaseUser.child(userkey).child("spouse").removeValue();

        dChildren(children);
    }
    private void dParents(ArrayList<User> siblings){
        if(popo123[0]!=null) {
            db.deleteUser(popo123[0].getId());
            dataBaseUser.child(popo123[0].getId()).removeValue();
        }
        if(popo123[1]!=null) {
            db.deleteUser(popo123[1].getId());
            dataBaseUser.child(popo123[1].getId()).removeValue();
        }
        dataBaseUser.child(userkey).child("parent").removeValue();
        dSiblings(siblings);
    }
    private void dSiblings(ArrayList<User> siblings){
        for (int i=0;i<siblings.size();i++){
            db.deleteUser(siblings.get(i).getId());
            dataBaseUser.child(siblings.get(i).getId()).removeValue();
        }
        dataBaseUser.child(userkey).child("sibling").removeValue();

        //delete siblings from parents
        if(popo123[0]!=null) {
            dataBaseUser.child(popo123[0].getId()).child("children").removeValue();
        }
        if(popo123[1]!=null) {
            dataBaseUser.child(popo123[1].getId()).child("children").removeValue();
        }

    }
    private void dChildren(ArrayList<User> children){
        for (int i=0;i<children.size();i++){
            db.deleteUser(children.get(i).getId());
            dataBaseUser.child(children.get(i).getId()).removeValue();
        }
        dataBaseUser.child(userkey).child("children").removeValue();

        for (int i=0;i<spouses.size();i++){
            dataBaseUser.child(spouses.get(i).getId()).child("children").removeValue();
        }

    }
    private void dAll(ArrayList<User> children, ArrayList<User> siblings){
        dAllSpouse(children);
        dParents(siblings);
        dSiblings(siblings);
        dChildren(children);
    }
    private void dAllM(ArrayList<User> children, ArrayList<User> siblings){
        dAll(children,siblings);
        db.deleteUser(userkey);
        dataBaseUser.child(userkey).removeValue();

    }


    private void setImage(CircleImageView pro , String gender){
        int i;
        if (gender.equals("male"))
            i = R.drawable.male;
        else
            i = R.drawable.female;
        pro.setImageResource(i);
        pro.setBorderColor(Color.parseColor("#FFFFFF"));
    }

    private void get_Data_Into_String_Buffer(StringBuffer stbuff , Cursor alldata){
        for ( int i = 0 ; i <18 ; i++){
            if(i!= 13)
            stbuff.append(alldata.getString(i)+"\n");
        }
        stbuff.append("-----------------------------"+"\n");
    }

    private void putExtra_ProfileView(Intent i , String who , User current , byte[] arr){
        i.putExtra("phototo", arr);
        i.putExtra("who", who);
        i.putExtra("mainuser", current.getId());
    }

    private void putExtra_Parents(Intent i,  User dad ,User mama ){
        i.putExtra("father1", (Serializable) dad);
        i.putExtra("mother1", (Serializable) mama);
    }

    private void putExtra_CurrentSpouse(Intent i , User currentspouse){
        if (currentspouse != null)
            i.putExtra("currentspouse", currentspouse.getId());
        else
            i.putExtra("currentspouse", "");
        }



    private void setButtonSizesMain(CircleImageView crcl){
        crcl.getLayoutParams().width=width/4;
        crcl.getLayoutParams().height=width/4;
        crcl.setBorderWidth(width/100);
    }


    private void LangButtonTextChange(){
        sharedpref = this.getSharedPreferences("shared",MODE_PRIVATE);
        editor = sharedpref.edit();

        String languageToLoad="";

        String temporary1= sharedpref.getString(language,"");
        try {
            languageToLoad = sharedpref.getString(language, temporary1); // your language
        }
        catch(Exception e){}

        if(languageToLoad.equals("he")) languages.setText(R.string.hebrew);
        else if(languageToLoad.equals("en")) languages.setText(R.string.english);
    }


    private void setnotifications(String date, String name , String DateOf){

        int day = Integer.parseInt(date.split("/")[0]);
        int month = Integer.parseInt(date.split("/")[1]);
        int year = Integer.parseInt(date.split("/")[2]);


        Intent notificationIntent = notificationIntent(name,day,month,0,DateOf);

        Intent notificationIntent3 = notificationIntent(name,day,month,3,DateOf);

        Intent notificationIntent7 = notificationIntent(name,day,month,7,DateOf);

        int dateOfChecker =0; // this is to make a differenece in the unique number for date of death and date of birth

        if(DateOf.equals(DateOfDeath)) dateOfChecker = 3;

        int uniquenum= (((((month+day+year)*23)/4)+day+month+year)*29)/3+(name.length()*18)+69+dateOfChecker;


        PendingIntent broadcast = PendingIntent.getBroadcast(TreeLayoutTemp.this,uniquenum+0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        PendingIntent broadcast3 = PendingIntent.getBroadcast(TreeLayoutTemp.this,uniquenum+1, notificationIntent3, PendingIntent.FLAG_UPDATE_CURRENT);

        PendingIntent broadcast7 = PendingIntent.getBroadcast(TreeLayoutTemp.this,uniquenum+2, notificationIntent7, PendingIntent.FLAG_UPDATE_CURRENT);


        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH,month-1); // TODO: the set lines extracted into method
        cal.set(Calendar.DAY_OF_MONTH,day);
        cal.set(Calendar.HOUR_OF_DAY,8); // alarm is set at 8am exactly
        cal.set(Calendar.MINUTE,0);
        cal.set(Calendar.SECOND,0);
//        cal.set(Calendar.SECOND,cal.get(Calendar.SECOND)+2);



        long pika = cal.getTimeInMillis();

        Long pikachu = Calendar.getInstance().getTimeInMillis();


        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        AlarmManager alarmManager3 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        AlarmManager alarmManager7 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        if(pika < pikachu){ //TODO: no pikachu
            cal.set(Calendar.YEAR,cal.get(Calendar.YEAR)+1);
        }

        long pikas = cal.getTimeInMillis();


        Calendar threedays = Calendar.getInstance();
        threedays.setTimeInMillis(pikas-259200000); // three days in millis

        Calendar oneweek = Calendar.getInstance();
        oneweek.setTimeInMillis(pikas-604800000); // 7 days in millis


        calendarCheck(alarmManager, cal , broadcast);
        calendarCheck(alarmManager3 , threedays , broadcast3);
        calendarCheck(alarmManager7 , oneweek , broadcast7);
    }
    private Intent notificationIntent(String fullname, int day, int month , int daysTill ,String DateOf){
        Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
        notificationIntent.addCategory("android.intent.category.DEFAULT");
        notificationIntent.putExtra("fullname",fullname);
        notificationIntent.putExtra("or",DateOf);
        notificationIntent.putExtra("date",day+"/"+month);
        notificationIntent.putExtra("id",userkey);
        notificationIntent.putExtra("daystill",daysTill);
        return notificationIntent;
    }
    private void calendarCheck(AlarmManager alrm , Calendar cal , PendingIntent broadcast){
        if(Calendar.getInstance().getTimeInMillis() > cal.getTimeInMillis()) cal.set(Calendar.YEAR,cal.get(Calendar.YEAR)+1);
        alrm.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);

    }
    private void setUpAllNotifications(User father, User mother, ArrayList<User> spouse, ArrayList<User> children, ArrayList<User> siblings) {
        if(father!=null) {
            setnotifications(father.getDateofbirth(), father.getFullname(), DateOfBirth);
            if (!father.getDateofdeath().equals(""))
                setnotifications(father.getDateofdeath(), father.getFullname(), DateOfDeath);
        }

        if(mother!=null) {
            setnotifications(mother.getDateofbirth(), mother.getFullname(), DateOfBirth);
            if (!mother.getDateofdeath().equals(""))
                setnotifications(mother.getDateofdeath(), mother.getFullname(), DateOfDeath);
        }

        setUpNotificationArray(spouse);
        setUpNotificationArray(children);
        setUpNotificationArray(siblings);
    }
    private void setUpNotificationArray(ArrayList<User> users){
        for (User user : users){
            if(user!=null) {
                setnotifications(user.getDateofbirth(), user.getFullname(), DateOfBirth);
                if (!user.getDateofdeath().equals(""))
                    setnotifications(user.getDateofdeath(), user.getFullname(), DateOfDeath);
            }

        }
    }

    private static String DateOfBirth = "dateofbirth";
    private static String DateOfDeath = "dateofdeath";

}
